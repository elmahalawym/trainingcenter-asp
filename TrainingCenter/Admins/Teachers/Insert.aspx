﻿<%@ Page Title="TeacherViewModelInsert" Language="C#" MasterPageFile="~/Site.Master" CodeBehind="Insert.aspx.cs" Inherits="TrainingCenter.Admins.TeacherViewModels.Insert" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div>
        <p>&nbsp;</p>


        <p class="text-danger">
            <asp:Literal runat="server" ID="ErrorMessage" />
        </p>

        <div class="form-horizontal">
            <h4>إضافة مدرس</h4>
            <hr />
            <asp:ValidationSummary runat="server" CssClass="alert alert-danger" />
            <div class="form-group">
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="usernameTxt" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="usernameTxt"
                        CssClass="text-danger" ErrorMessage="يجب إدخال اسم المستخدم" />
                </div>
                <asp:Label runat="server" AssociatedControlID="usernameTxt" CssClass="col-md-2 control-label">اسم المستخدم</asp:Label>
            </div>

            <div class="form-group">
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="emailTxt" CssClass="form-control" TextMode="Email" />
                   <%-- <asp:RequiredFieldValidator runat="server" ControlToValidate="emailTxt"
                        CssClass="text-danger" ErrorMessage="يجب إدخال الإيميل" />--%>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" CssClass="text-danger" runat="server" ErrorMessage="ex: student@gmail.com" ControlToValidate="emailTxt" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                </div>
                <asp:Label runat="server" AssociatedControlID="emailTxt" CssClass="col-md-2 control-label">الإيميل</asp:Label>
            </div>


            <div class="form-group">
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="passwordTxt" CssClass="form-control" />
                    <span id="generateBtn" class="btn btn-default" data-target="<%= passwordTxt.ClientID %>">generate</span>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="passwordTxt"
                        CssClass="text-danger" ErrorMessage="يجب إدخال كلمة السر" />
                </div>
                <asp:Label runat="server" AssociatedControlID="passwordTxt" CssClass="col-md-2 control-label">كلمة السر</asp:Label>
            </div>

            <div class="form-group">
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="firstNameTxt" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="firstNameTxt"
                        CssClass="text-danger" ErrorMessage="يجب إدخال الإسم الأول" />
                </div>
                <asp:Label runat="server" AssociatedControlID="firstNameTxt" CssClass="col-md-2 control-label">الإسم الأول</asp:Label>
            </div>
            <div class="form-group">

                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="lastNameTxt" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="lastNameTxt"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="يجب إدخال اسم العائلة" />
                </div>
                <asp:Label runat="server" AssociatedControlID="lastNameTxt" CssClass="col-md-2 control-label">اسم العائلة</asp:Label>
            </div>
            <div class="form-group">

                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="phoneTxt" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="phoneTxt"
                        CssClass="text-danger" Display="Dynamic" ErrorMessage="يجب إدخال التليفون " />
                </div>
                <asp:Label runat="server" AssociatedControlID="phoneTxt" CssClass="col-md-2 control-label">رقم التليفون</asp:Label>
            </div>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <asp:Button runat="server" OnClick="CreateStudent_Click" Text="تسجيل" CssClass="btn btn-primary" />
                    <a href="Default.aspx" class="btn btn-default">إلغاء</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
