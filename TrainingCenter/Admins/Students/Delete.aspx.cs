﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using Microsoft.AspNet.FriendlyUrls.ModelBinding;
using TrainingCenter.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.FriendlyUrls;

namespace TrainingCenter.Admins.StudentViewModels
{
    public partial class Delete : System.Web.UI.Page
    {
        protected ApplicationDbContext _db = new ApplicationDbContext();
        ApplicationUser _user;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(FriendlyUrl.Segments.Count > 0)
            {
                string userId = FriendlyUrl.Segments[0];
                ApplicationUser user = _db.Users.Where(u => u.Id == userId).FirstOrDefault();
                if (user != null)
                {
                    _user = user;
                    userNameTxt.Text = user.FirstName + " " + user.LastName;
                }else
                {
                    userNameTxt.Text = "عفوا بيانات الطالب غير موجودة";
                }
            }
            else
            {
                Response.Redirect("~/Admins/Students/Default");
            }
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            if(_user != null)
            {
                _db.Users.Remove(_user);
                _db.SaveChanges();
            }
            Response.Redirect("~/Admins/Students/Default");
        }

    }
}

