﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrainingCenter.Models;

namespace TrainingCenter.Admins.SchoolYears
{
    public partial class setprices : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                Prices p = Helpers.GetPrices(Server.MapPath("~/App_Data/Prices.xml"));
                if(p != null)
                {
                    percentageTxt.Text = p.Percentage.ToString();
                    expensesTxt.Text = p.AdministrativeExpenses.ToString();
                }
            }
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            if(Page.IsValid)
            {
                // save new prices and redirect the user to the default page
                Prices p = new Prices();
                double expenses = double.Parse(expensesTxt.Text);
                double percentage = double.Parse(percentageTxt.Text);
                bool success = Helpers.UpdatePrices(Server.MapPath("~/App_Data/Prices.xml"), percentage, expenses);
                if (success)
                    Response.Redirect("~/Admins/Schoolyears/Default");
                else
                    errorMessage.Text = "error updating prices";
            }
        }
    }
}