﻿<%@ Page Title="TeacherDiscountEdit" Language="C#" MasterPageFile="~/Site.Master" CodeBehind="Edit.aspx.cs" Inherits="TrainingCenter.Admins.Discounts.Edit" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div>
        <p>&nbsp;</p>
        <asp:FormView runat="server"
            ItemType="TrainingCenter.Models.TeacherDiscount" DefaultMode="Edit" DataKeyNames="ID"
            UpdateMethod="UpdateItem" SelectMethod="GetItem"
            OnItemCommand="ItemCommand" RenderOuterTable="false">
            <EmptyDataTemplate>
                Cannot find the TeacherDiscount with ID <%: Request.QueryString["ID"] %>
            </EmptyDataTemplate>
            <EditItemTemplate>
                <fieldset class="form-horizontal">
                    <legend>تعديل الخصم</legend>
                    <asp:ValidationSummary runat="server" CssClass="alert alert-danger" />
<%--                    <asp:DynamicControl Mode="Edit"
                        DataField="TeacherID"
                        DataTypeName="TrainingCenter.Models.ApplicationUser"
                        DataTextField="FirstName"
                        DataValueField="Id"
                        UIHint="ForeignKey" runat="server" />--%>
                    <%--						    <asp:DynamicControl Mode="Edit" DataField="DiscountDate" runat="server" />
						    <asp:DynamicControl Mode="Edit" DataField="DiscountType" runat="server" />
						    <asp:DynamicControl Mode="Edit" DataField="Amount" runat="server" />--%>

                    <!-- Amount -->
                    <div class="form-group">
                        <div class="col-md-10">
                            <asp:TextBox ID="amountTxt" runat="server" TextMode="Number" CssClass="form-control"
                                Text='<%# Bind("Amount") %>' />
                            <asp:RequiredFieldValidator runat="server" ErrorMessage="يجب تحديد القيمة"
                                ControlToValidate="amountTxt" CssClass="text-danger" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="amountTxt" CssClass="col-md-2 control-label">قيمة الخصم</asp:Label>
                    </div>

                    <!-- Notes -->
                    <div class="form-group">
                        <div class="col-md-10">
                            <asp:TextBox ID="notesTxt" runat="server" CssClass="form-control" TextMode="MultiLine"
                                Text='<%# Bind("Notes") %>' />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="notesTxt" CssClass="col-md-2 control-label">ملاحظات</asp:Label>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button runat="server" ID="UpdateButton" CommandName="Update" Text="Update" CssClass="btn btn-primary" />
                            <asp:Button runat="server" ID="CancelButton" CommandName="Cancel" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" />
                        </div>
                    </div>
                </fieldset>
            </EditItemTemplate>
        </asp:FormView>
    </div>
</asp:Content>

