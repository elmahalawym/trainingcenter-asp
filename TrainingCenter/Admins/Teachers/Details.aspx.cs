﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using Microsoft.AspNet.FriendlyUrls.ModelBinding;
using TrainingCenter.Models;

namespace TrainingCenter.Admins.TeacherViewModels
{
    public partial class Details : System.Web.UI.Page
    {
        protected TrainingCenter.Models.ApplicationDbContext _db = new TrainingCenter.Models.ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        // This is the Select methd to selects a single TeacherViewModel item with the id
        // USAGE: <asp:FormView SelectMethod="GetItem">
        public TrainingCenter.Models.TeacherViewModel GetItem([FriendlyUrlSegmentsAttribute(0)]string ApplicationUserID)
        {
            if (ApplicationUserID == null)
            {
                return null;
            }

            using (_db)
            {
                return _db.Users.Where(m => m.Id == ApplicationUserID).Select(m => new
                    TeacherViewModel()
                    {
                        ApplicationUserID = m.Id,
                        UserName = m.UserName,
                        Email = m.Email,
                        FirstName = m.FirstName,
                        LastName = m.LastName,
                        Phone = m.Phone
                    }).FirstOrDefault();
            }
        }

        protected void ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Cancel", StringComparison.OrdinalIgnoreCase))
            {
                Response.Redirect("../Default");
            }
        }
    }
}

