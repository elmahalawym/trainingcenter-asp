﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="setprices.aspx.cs" Inherits="TrainingCenter.Admins.SchoolYears.setprices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <br />
    <br />

    <div class="form-horizontal">
        <!-- النسبة -->
        <div class="form-group">
            <div class="col-md-10">
                <asp:TextBox ID="percentageTxt" runat="server" CssClass="form-control" TextMode="Number" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="percentageTxt"
                    CssClass="text-danger" ErrorMessage="يجب إدخال النسبة" />
                <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="النسبة يجب أن تكون من 0 إلي 100"
                    ControlToValidate="percentageTxt" MinimumValue="0" Type="Integer" MaximumValue="100" CssClass="text-danger" />
                <asp:CompareValidator runat="server" ControlToValidate="percentageTxt" Type="Double"
                    Operator="DataTypeCheck" ErrorMessage="invalid value" />
            </div>
            <asp:Label runat="server" AssociatedControlID="percentageTxt"
                CssClass="col-md-2 control-label">النسبة</asp:Label>
        </div>

        <!-- المصاريف الإدارية -->
        <div class="form-group">
            <div class="col-md-10">
                <asp:TextBox ID="expensesTxt" runat="server" CssClass="form-control" TextMode="Number" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="expensesTxt"
                    CssClass="text-danger" ErrorMessage="يجب إدخال المصاريف الإدارية" />
                <asp:CompareValidator runat="server" ControlToValidate="expensesTxt" Type="Double"
                    Operator="DataTypeCheck" ErrorMessage="invalid value" />
            </div>
            <asp:Label runat="server" AssociatedControlID="expensesTxt"
                CssClass="col-md-2 control-label">المصاريف الإدارية</asp:Label>
        </div>

        <asp:Button ID="saveBtn" runat="server" CssClass="btn btn-primary" Text="حفظ"
            OnClick="saveBtn_Click" />
        <p class="text-danger">
            <asp:Literal ID="errorMessage" runat="server" />
        </p>

    </div>

</asp:Content>
