namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditedExamsModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StudentGrades", "ExamID", "dbo.Exams");
            DropForeignKey("dbo.StudentGrades", new[] { "Month", "Half" }, "dbo.Exams");
            DropIndex("dbo.StudentGrades", new[] { "ExamID" });
            RenameColumn(table: "dbo.StudentGrades", name: "ExamID", newName: "Month");
            DropPrimaryKey("dbo.Exams");
            DropPrimaryKey("dbo.StudentGrades");
            AddColumn("dbo.Courses", "NumberOfSessionsPerWeek", c => c.Int(nullable: false));
            AddColumn("dbo.Exams", "Month", c => c.Int(nullable: false));
            AddColumn("dbo.Exams", "Half", c => c.Int(nullable: false));
            AddColumn("dbo.StudentGrades", "Half", c => c.Int(nullable: false));
            AlterColumn("dbo.Exams", "ExamDate", c => c.DateTime());
            AddPrimaryKey("dbo.Exams", new[] { "Month", "Half" });
            AddPrimaryKey("dbo.StudentGrades", new[] { "StudentID", "Month", "Half" });
            CreateIndex("dbo.StudentGrades", new[] { "Month", "Half" });
            AddForeignKey("dbo.StudentGrades", new[] { "Month", "Half" }, "dbo.Exams", new[] { "Month", "Half" }, cascadeDelete: true);
            DropColumn("dbo.Exams", "ExamID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Exams", "ExamID", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.StudentGrades", new[] { "Month", "Half" }, "dbo.Exams");
            DropIndex("dbo.StudentGrades", new[] { "Month", "Half" });
            DropPrimaryKey("dbo.StudentGrades");
            DropPrimaryKey("dbo.Exams");
            AlterColumn("dbo.Exams", "ExamDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.StudentGrades", "Half");
            DropColumn("dbo.Exams", "Half");
            DropColumn("dbo.Exams", "Month");
            DropColumn("dbo.Courses", "NumberOfSessionsPerWeek");
            AddPrimaryKey("dbo.StudentGrades", new[] { "StudentID", "ExamID" });
            AddPrimaryKey("dbo.Exams", "ExamID");
            RenameColumn(table: "dbo.StudentGrades", name: "Month", newName: "ExamID");
            CreateIndex("dbo.StudentGrades", "ExamID");
            AddForeignKey("dbo.StudentGrades", new[] { "Month", "Half" }, "dbo.Exams", new[] { "Month", "Half" }, cascadeDelete: true);
            AddForeignKey("dbo.StudentGrades", "ExamID", "dbo.Exams", "ExamID", cascadeDelete: true);
        }
    }
}
