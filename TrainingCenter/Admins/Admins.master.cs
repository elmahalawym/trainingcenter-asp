﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TrainingCenter.Admins
{
    public partial class Admins : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string SelectedMenuItem { get; set; }

        protected void Page_Prerender(object sender, EventArgs e)
        {
            switch(SelectedMenuItem)
            {
                case "courses":
                    coursesLi.Attributes.Add("class", "active");
                    break;
                case "default":
                    mainLi.Attributes.Add("class", "active");
                    break;
                case "prices":
                    pricesLi.Attributes.Add("class", "active");
                    break;
                case "expenses":
                    expensesLi.Attributes.Add("class", "active");
                    break;
                case "salaries":
                    salariesLi.Attributes.Add("class", "active");
                    break;
                case "teachers":
                    teachersLi.Attributes.Add("class", "active");
                    break;
                case "students":
                    studentsLi.Attributes.Add("class", "active");
                    break;
                case "exams":
                    examsLi.Attributes.Add("class", "active");
                    break;
                case "discounts":
                    discountsLi.Attributes.Add("class", "active");
                    break;
                default:
                    break;
            }
        }
    }
}