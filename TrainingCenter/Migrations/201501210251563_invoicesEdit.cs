namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invoicesEdit : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Invoices", "PaidAmount", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Invoices", "PaidAmount");
        }
    }
}
