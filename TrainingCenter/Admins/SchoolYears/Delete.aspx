﻿<%@ Page Title="SchoolYearDelete" Language="C#" MasterPageFile="~/Site.Master" CodeBehind="Delete.aspx.cs" Inherits="TrainingCenter.SchoolYears.Delete" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div>
        <p>&nbsp;</p>
        <h3>هل أنت متأكد من حذف السنة الدراسية؟</h3>
        <asp:FormView runat="server"
            ItemType="TrainingCenter.Models.SchoolYear" DataKeyNames="SchoolYearID"
            DeleteMethod="DeleteItem" SelectMethod="GetItem"
            OnItemCommand="ItemCommand">
            <EmptyDataTemplate>
                Cannot find the SchoolYear with SchoolYearID <%: Request.QueryString["SchoolYearID"] %>
            </EmptyDataTemplate>

            <ItemTemplate>
                <fieldset class="form-horizontal">
                    <legend>حذف السنة الدراسية</legend>
                    <%--							<div class="row">
								<div class="col-sm-2 text-right">
									<strong>SchoolYearID</strong>
								</div>
								<div class="col-sm-4">
									<asp:DynamicControl runat="server" DataField="SchoolYearID" ID="SchoolYearID" Mode="ReadOnly" />
								</div>
							</div>--%>


                    <div class="row">

                        <div class="col-sm-4">
                            <asp:DynamicControl runat="server" DataField="Name" ID="Name" Mode="ReadOnly" />
                        </div>
                        <div class="col-sm-2">
                            <strong>إسم السنة الدراسية</strong>
                        </div>  
                    </div>
                    <br />
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="حذف" CssClass="btn btn-danger" />
                            <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="إلغاء" CssClass="btn btn-default" />
                        </div>
                    </div>
                </fieldset>
            </ItemTemplate>
        </asp:FormView>
    </div>
</asp:Content>

