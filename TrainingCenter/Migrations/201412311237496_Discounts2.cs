namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Discounts2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TeacherDiscounts", "Notes", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TeacherDiscounts", "Notes");
        }
    }
}
