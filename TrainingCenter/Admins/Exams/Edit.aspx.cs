﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using Microsoft.AspNet.FriendlyUrls.ModelBinding;
using TrainingCenter.Models;
using Microsoft.AspNet.FriendlyUrls;

namespace TrainingCenter.Admins.Exams
{
    public partial class Edit : System.Web.UI.Page
    {
		protected TrainingCenter.Models.ApplicationDbContext _db = new TrainingCenter.Models.ApplicationDbContext();
        int month, year, half;
        int courseId;
        Exam exam;

        protected void Page_Load(object sender, EventArgs e)
        {
            // redirect to default page if the required parameters are not provided
            if(FriendlyUrl.Segments.Count != 4)
            {
                Response.Redirect("~/Admins/Exams/Default");
                return;
            }

            if (!int.TryParse(FriendlyUrl.Segments[0], out year) || !int.TryParse(FriendlyUrl.Segments[1], out month)
                 || !int.TryParse(FriendlyUrl.Segments[2], out half) || !int.TryParse(FriendlyUrl.Segments[3], out courseId))
            {
                Response.Redirect("~/Admins/Exams/Default");
                return;
            }

            // redirect to default page if exam is not found
            exam = _db.Exams.Where(ee => ee.Year == year && ee.Month == month &&
                ee.Half == half && ee.CourseID == courseId).Include(ee => ee.Course)
                .Include(ee => ee.Course.SchoolYear).FirstOrDefault();
            if(exam == null)
            {
                Response.Redirect("~/Admins/Exams/Default");
                return;
            }

            // fill form controls with exam data
            if(!IsPostBack)
            {
                yearLabel.Text = exam.Year.ToString();
                monthLabel.Text = exam.GetMonth();
                halfLabel.Text = exam.GetHalf();
                courseLabel.Text = exam.Course.CourseName + " - " + exam.Course.SchoolYear.Name;
                fullMarkTxt.Text = exam.FullMark.ToString();
                if (exam.ExamDate != null) examDateCalendar.SelectedDate = (DateTime)exam.ExamDate;
            }

        }


        protected void UpdateButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    // update exam information
                    exam.FullMark = int.Parse(fullMarkTxt.Text);
                    if(examDateCalendar.SelectedDate.Date != DateTime.MinValue)
                        exam.ExamDate = examDateCalendar.SelectedDate.Date;
                    else
                        exam.ExamDate = null;

                    // update database
                    _db.SaveChanges();
                    Response.Redirect("~/Admins/Exams/Default");

                }
                catch (Exception) { errorMessage.Text = "an error occured"; }
            }
        }


        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                _db.Exams.Remove(exam);
                _db.SaveChanges();
            }
            catch (Exception) { errorMessage.Text = "an error occured white deleting the selected exam"; }
            Response.Redirect("~/Admins/Exams/Default");
        }


        protected void CancelButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admins/Exams/Default");
        }



        /*
        // This is the Update methd to update the selected Exam item
        // USAGE: <asp:FormView UpdateMethod="UpdateItem">
        public void UpdateItem(int  ExamID)
        {
            using (_db)
            {
                var item = _db.Exams.Find(ExamID);

                if (item == null)
                {
                    // The item wasn't found
                    ModelState.AddModelError("", String.Format("Item with id {0} was not found", ExamID));
                    return;
                }

                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    // Save changes here
                    _db.SaveChanges();
                    Response.Redirect("../Default");
                }
            }
        }

        // This is the Select method to selects a single Exam item with the id
        // USAGE: <asp:FormView SelectMethod="GetItem">
        public TrainingCenter.Models.Exam GetItem([FriendlyUrlSegmentsAttribute(0)]int? ExamID)
        {
            if (ExamID == null)
            {
                return null;
            }

            using (_db)
            {
                return _db.Exams.Find(ExamID);
            }
        }

        protected void ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Cancel", StringComparison.OrdinalIgnoreCase))
            {
                Response.Redirect("../Default");
            }
        }*/
    }
}
