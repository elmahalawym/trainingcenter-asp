﻿<%@ Page Title="Salary Details" Language="C#" MasterPageFile="~/Site.Master" CodeBehind="Details.aspx.cs" Inherits="TrainingCenter.Admins.Salaries.Details" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div>
        <p>&nbsp;</p>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>تفاصيل المرتب</h3>
            </div>
            <div class="panel-body">
                <div class="form-horizontal">

                    <div class="form-group">
                        <div class="col-md-10">
                            <asp:Label runat="server" CssClass="form-control" ID="teacherNameLabel" />
                        </div>
                        <asp:Label runat="server" CssClass="col-md-2 control-label">اسم المدرس</asp:Label>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10">
                            <asp:Label runat="server" CssClass="form-control" ID="totalLabel" />
                        </div>
                        <asp:Label runat="server" CssClass="col-md-2 control-label">المجموع</asp:Label>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10">
                            <asp:Label runat="server" CssClass="form-control" ID="factorLabel" />
                        </div>
                        <asp:Label runat="server" CssClass="col-md-2 control-label">النسبة</asp:Label>
                    </div>

                    <asp:ListView ID="sUnitsListView" runat="server" ItemType="TrainingCenter.Models.SalaryUnit">
                        <LayoutTemplate>
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>المادة</th>
                                        <th>عدد الطلاب - عام</th>
                                        <th>سعر الطالب الواحد - عام</th>
                                        <th>عدد الطلاب - خاص</th>
                                        <th>سعر الطالب الواحد - خاص</th>
                                        <th>المجموع</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <div id="itemPlaceHolder" runat="server"></div>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%# Item.CourseName %></td>
                                <td><%# Item.NStudentsGeneral %></td>
                                <td><%# Item.GeneralStudentPrice %></td>
                                <td><%# Item.NStudentsSpecial %></td>
                                <td><%# Item.SpecialStudentPrice %></td>
                                <td><%# (Item.NStudentsGeneral * Item.GeneralStudentPrice) + 
                                    (Item.NStudentsSpecial * Item.SpecialStudentPrice) %></td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </div>

            <div class="form-horizontal">

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <asp:Button runat="server" Text="العودة" CssClass="btn btn-default"
                            OnClick="Unnamed_Click" />
                    </div>
                </div>
            </div>

        </div>



        <%--  <asp:FormView runat="server"
            ItemType="TrainingCenter.Models.Salary" DataKeyNames="SalaryID"
            SelectMethod="GetItem"
            OnItemCommand="ItemCommand" RenderOuterTable="false">
            <EmptyDataTemplate>
                Cannot find the Salary with SalaryID <%: Request.QueryString["SalaryID"] %>
            </EmptyDataTemplate>
            <ItemTemplate>
                <fieldset class="form-horizontal">
                    <legend>Salary Details</legend>
							<div class="row">
								<div class="col-sm-2 text-right">
									<strong>SalaryID</strong>
								</div>
								<div class="col-sm-4">
									<asp:DynamicControl runat="server" DataField="SalaryID" ID="SalaryID" Mode="ReadOnly" />
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2 text-right">
									<strong>TeacherID</strong>
								</div>
								<div class="col-sm-4">
									<%#: Item.ApplicationUser != null ? Item.ApplicationUser.FirstName : "" %>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2 text-right">
									<strong>Price</strong>
								</div>
								<div class="col-sm-4">
									<asp:DynamicControl runat="server" DataField="Price" ID="Price" Mode="ReadOnly" />
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2 text-right">
									<strong>IsPaid</strong>
								</div>
								<div class="col-sm-4">
									<asp:DynamicControl runat="server" DataField="IsPaid" ID="IsPaid" Mode="ReadOnly" />
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2 text-right">
									<strong>PaidAmount</strong>
								</div>
								<div class="col-sm-4">
									<asp:DynamicControl runat="server" DataField="PaidAmount" ID="PaidAmount" Mode="ReadOnly" />
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2 text-right">
									<strong>Year</strong>
								</div>
								<div class="col-sm-4">
									<asp:DynamicControl runat="server" DataField="Year" ID="Year" Mode="ReadOnly" />
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2 text-right">
									<strong>Month</strong>
								</div>
								<div class="col-sm-4">
									<asp:DynamicControl runat="server" DataField="Month" ID="Month" Mode="ReadOnly" />
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2 text-right">
									<strong>PaymentDate</strong>
								</div>
								<div class="col-sm-4">
									<asp:DynamicControl runat="server" DataField="PaymentDate" ID="PaymentDate" Mode="ReadOnly" />
								</div>
							</div>
                 	<div class="row">
					  &nbsp;
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Back" CssClass="btn btn-default" />
						</div>
					</div>
                </fieldset>
            </ItemTemplate>
        </asp:FormView>--%>
    </div>
</asp:Content>

