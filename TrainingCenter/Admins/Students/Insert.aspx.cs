﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using TrainingCenter.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace TrainingCenter.Admins.StudentViewModels
{
    public partial class Insert : System.Web.UI.Page
    {
        protected ApplicationDbContext _db = new ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var schoolYears = _db.SchoolYears.ToList();
                schoolYearDropDown.DataSource = schoolYears;
                schoolYearDropDown.DataBind();
            }
        }


        // change courses in coursesList when the user changes school year
        protected void schoolYearDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedSchoolyearId = -1;
            if (!int.TryParse(schoolYearDropDown.SelectedValue, out selectedSchoolyearId))
                return;

            var courses = _db.Courses.Where(c => c.SchoolYearID == selectedSchoolyearId).ToList();
            coursesList.DataSource = courses;
            coursesList.DataBind();
        }

        protected void CreateStudent_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                var userManager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

                DateTime birthDate;
                DateTime? birthDateEntry = null;
                if (!string.IsNullOrEmpty(birthDateTxt.Text) && DateTime.TryParse(birthDateTxt.Text, out birthDate))
                    birthDateEntry = birthDate;

                string email = (string.IsNullOrEmpty(emailTxt.Text) ? "user@example.com" : emailTxt.Text);

                bool generalType = (int.Parse(generalTypeDropDown.SelectedValue) == 1) ? true : false;
                var user = new ApplicationUser()
                {
                    UserName = usernameTxt.Text,
                    FirstName = firstNameTxt.Text,
                    LastName = lastNameTxt.Text,
                    Phone = phoneTxt.Text,
                    Email = email,
                    SchoolYearID = int.Parse(schoolYearDropDown.SelectedValue),
                    SchoolName = schoolNameTxt.Text,
                    BirthDate = birthDateEntry,
                    FatherPhoneNumber = fatherPhoneTxt.Text,
                    MotherPhoneNumber = motherPhoneTxt.Text,
                    Mobile1 = mobile1Txt.Text,
                    Mobile2 = mobile2Txt.Text,
                    Notes = notesTxt.Text,
                    IsStudentActive = isActiveCB.Checked,

                    GeneralType = generalType
                };

                IdentityResult result = userManager.Create(user, passwordTxt.Text);
                if (result.Succeeded)
                {
                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    //string code = manager.GenerateEmailConfirmationToken(user.Id);
                    //string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                    //manager.SendEmail(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");

                    //IdentityHelper.SignIn(manager, user, isPersistent: false);
                    //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);

                    // add user to student role
                    userManager.AddToRole(user.Id, "student");

                    // add student subscriptions
                    try
                    {
                        var selectedItems = coursesList.Items.Cast<ListItem>().Where
                                    (li => li.Selected).ToList();
                        foreach (ListItem item in selectedItems)
                        {
                            _db.StudentSubscriptions.Add(new StudentSubscription()
                            {
                                CourseID = int.Parse(item.Value),
                                StudentID = user.Id
                            });
                        }
                        _db.SaveChanges();
                    }
                    catch (Exception) { }

                    if (int.Parse(curMonthSubType.SelectedValue) == 2)
                    {
                        Helpers.AddIdToHMonthList(Server.MapPath("~/App_Data/" + Helpers.GetHMonthFileName(DateTime.Now.Year, DateTime.Now.Month)), user.Id);
                    }

                    // update number of students
                    string hMonthFilePath = Server.MapPath(string.Format("~/App_Data/{0}", Helpers.GetHMonthFileName(DateTime.Now.Year, DateTime.Now.Month)));
                    string absNumbersFilePath = Server.MapPath(string.Format("~/App_Data/{0}", Helpers.AbsoluteNumbersFileName));
                    Helpers.UpdateNumberOfStudents(Server.MapPath(string.Format("~/App_Data/{0}", Helpers.NumberOfStudentsFileName)), absNumbersFilePath, hMonthFilePath);

                    // redirect to default page
                    Response.Redirect("Default");
                }
                else
                {
                    ErrorMessage.Text = result.Errors.FirstOrDefault();
                }
            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default");
        }



        #region FormView

        // This is the Insert method to insert the entered StudentViewModel item
        // USAGE: <asp:FormView InsertMethod="InsertItem">
        //public void InsertItem()
        //{
        //    using (_db)
        //    {
        //        var item = new TrainingCenter.Models.StudentViewModel();

        //        TryUpdateModel(item);

        //        if (ModelState.IsValid)
        //        {
        //            // Register new student
        //            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
        //            var user = new ApplicationUser() { UserName = item., Email = Email.Text };
        //            IdentityResult result = manager.Create(user, Password.Text);
        //            if (result.Succeeded)
        //            {
        //                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
        //                //string code = manager.GenerateEmailConfirmationToken(user.Id);
        //                //string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
        //                //manager.SendEmail(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");

        //                IdentityHelper.SignIn(manager, user, isPersistent: false);
        //                IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
        //            }
        //            else
        //            {
        //                //ErrorMessage.Text = result.Errors.FirstOrDefault();
        //            }


        //            Response.Redirect("Default");
        //        }
        //    }
        //}

        //protected void ItemCommand(object sender, FormViewCommandEventArgs e)
        //{
        //    if (e.CommandName.Equals("Cancel", StringComparison.OrdinalIgnoreCase))
        //    {
        //        Response.Redirect("Default");
        //    }
        //}

        #endregion

    }
}
