﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrainingCenter.Models
{
    public class Exam
    {
        //[Key]
        //public int ExamID { get; set; }

        [Range(1, 12)]
        [Key, Column(Order=0)]
        [ScaffoldColumn(true)]
        public int Month { get; set; }

        [Range(1, 2)]
        [Key, Column(Order=1)]
        [ScaffoldColumn(true)]
        public int Half { get; set; }

        [Key, Column(Order=2)]
        [ScaffoldColumn(true)]
        public int Year { get; set; }

        [ForeignKey("Course")]
        [Key, Column(Order=3)]
        public int CourseID { get; set; }

        [Display(Name="اسم الكورس")]
        public virtual Course Course { get; set; }

        [Display(Name="موعد الإمتحان")]
        public DateTime? ExamDate { get; set; }

        [Display(Name="الدرجة النهائية")]
        public int FullMark { get; set; }

        public string GetMonth()
        {
            switch(Month)
            {
                case 1:
                    return "يناير";
                case 2:
                    return "فبراير";
                case 3:
                    return "مارس";
                case 4:
                    return "إبريل";
                case 5:
                    return "مايو";
                case 6:
                    return "يونيو";
                case 7:
                    return "يوليو";
                case 8:
                    return "أغسطس";
                case 9:
                    return "سبتمبر";
                case 10:
                    return "أكتوبر";
                case 11:
                    return "نوفمبر";
                case 12:
                    return "ديسمبر";
                default:
                    return "";
            }
        }

        public string GetHalf()
        {
            switch(Half)
            {
                case 1:
                    return "الأول";
                case 2:
                    return "الثاني";
                default:
                    return "";
            }
        }
    }
}
