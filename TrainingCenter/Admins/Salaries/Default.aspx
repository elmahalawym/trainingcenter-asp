﻿<%@ Page Title="SalaryList" Language="C#" MasterPageFile="~/Admins/Admins.master" CodeBehind="Default.aspx.cs" Inherits="TrainingCenter.Admins.Salaries.Default" %>

<%@ Register TagPrefix="FriendlyUrls" Namespace="Microsoft.AspNet.FriendlyUrls" %>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">

    <br />

    <asp:HyperLink runat="server" NavigateUrl="/Admins/Salaries/Calculate"
        Text="حساب المرتبات" />

    <br />
    <br />

    <div class="form-inline">
        <asp:Label runat="server" AssociatedControlID="yearTxt" Text="السنة" CssClass="control-label" />
        <asp:TextBox runat="server" ID="yearTxt" TextMode="Number" CssClass="form-control" AutoPostBack="true" />
        <asp:Label runat="server" AssociatedControlID="monthDropDown" Text="الشهر" CssClass="control-label" />
        <asp:DropDownList runat="server" ID="monthDropDown" CssClass="form-control" AutoPostBack="true">
            <asp:ListItem Text="اختر الشهر" Value="-1" />
            <asp:ListItem Text="يناير" Value="1" />
            <asp:ListItem Text="فبراير" Value="2" />
            <asp:ListItem Text="مارس" Value="3" />
            <asp:ListItem Text="إبريل" Value="4" />
            <asp:ListItem Text="مايو" Value="5" />
            <asp:ListItem Text="يونيو" Value="6" />
            <asp:ListItem Text="يوليو" Value="7" />
            <asp:ListItem Text="أغسطس" Value="8" />
            <asp:ListItem Text="سبتمبر" Value="9" />
            <asp:ListItem Text="أكتوبر" Value="10" />
            <asp:ListItem Text="نوفمبر" Value="11" />
            <asp:ListItem Text="ديسمبر" Value="12" />
        </asp:DropDownList>
        <asp:Label runat="server" AssociatedControlID="stateDropDown" Text="الحالة" CssClass="control-label" />
        <asp:DropDownList runat="server" ID="stateDropDown" CssClass="form-control" AutoPostBack="true">
            <asp:ListItem Text="الكل" Value="-1" />
            <asp:ListItem Text="مدفوع" Value="1" />
            <asp:ListItem Text="غير مدفوع" Value="2" />
        </asp:DropDownList>
    </div>
    <br />
    <br />

    <%--    <div class="panel panel-default">
        <div class="panel-heading">المرتبات</div>
        <div class="panel-body">

            <asp:ListView ID="salariesListView" runat="server"
                ItemType="TrainingCenter.Models.SalaryDetails"
                SelectMethod="salariesListView_GetData">
                                <EmptyDataTemplate>
                    لا توجد بيانات
                </EmptyDataTemplate>
                <LayoutTemplate>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>اسم المدرس</th>
                                <th>الراتب المستحق</th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr runat="server" id="itemPlaceholder" />
                        </tbody>
                    </table>
                    <asp:DataPager PageSize="5" runat="server">
                        <Fields>
                            <asp:NextPreviousPagerField ShowLastPageButton="False" ShowNextPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                            <asp:NumericPagerField ButtonType="Button" NumericButtonCssClass="btn" CurrentPageLabelCssClass="btn disabled" NextPreviousButtonCssClass="btn" />
                            <asp:NextPreviousPagerField ShowFirstPageButton="False" ShowPreviousPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                        </Fields>
                    </asp:DataPager>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <%# Item.TeacherFullName %>
                        </td>
                        <td>
                            <%# Item.Total %>
                        </td>
                        <td>
                        <asp:HyperLink runat="server" NavigateUrl='<%# FriendlyUrl.Href("~/Admins/Salaries/Details", Item.TeacherId, yearTxt.Text, monthDropDown.SelectedIndex) %>' Text="التفاصيل" />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>

        </div>
    </div>--%>
    <%--    <p>
        <asp:HyperLink runat="server" NavigateUrl="Insert" Text="Create new" />
    </p>--%>


    <div class="panel panel-default">
        <div class="panel-heading">المرتبات</div>
        <div class="panel-body">
            <asp:ListView ID="ListView1" runat="server"
                ItemType="TrainingCenter.Models.Salary"
                DataKeyNames="TeacherID,Year,Month"
                SelectMethod="GetData">
                <EmptyDataTemplate>
                    لا توجد بيانات
                </EmptyDataTemplate>
                <LayoutTemplate>
                    <table class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <%--                                <th>
                                    <asp:LinkButton Text="SalaryID" CommandName="Sort" CommandArgument="SalaryID" runat="Server" />
                                </th>--%>
                                <th>المدرس
                                </th>
                                <th>القيمة
                                </th>
                                <th>مدفوع
                                </th>
                                <th>القيمة المدفوعة
                                </th>
                                <th>تاريخ الدفع
                                </th>
                                <th>ملاحظات
                                </th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr runat="server" id="itemPlaceholder" />
                        </tbody>
                    </table>
<%--                    <asp:DataPager PageSize="5" runat="server">
                        <Fields>
                            <asp:NextPreviousPagerField ShowLastPageButton="False" ShowNextPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                            <asp:NumericPagerField ButtonType="Button" NumericButtonCssClass="btn" CurrentPageLabelCssClass="btn disabled" NextPreviousButtonCssClass="btn" />
                            <asp:NextPreviousPagerField ShowFirstPageButton="False" ShowPreviousPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                        </Fields>
                    </asp:DataPager>--%>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <%--                        <td>
                            <asp:DynamicControl runat="server" DataField="SalaryID" ID="SalaryID" Mode="ReadOnly" />
                        </td>--%>
                        <td>
                            <%#: Item.ApplicationUser.FirstName + " " + Item.ApplicationUser.LastName %>
                        </td>
                        <td>
                            <asp:DynamicControl runat="server" DataField="Price" ID="Price" Mode="ReadOnly" />
                        </td>
                        <td>
                            <%# Item.IsPaid ? "نعم" : "لا" %>
                        </td>
                        <td>
                            <asp:DynamicControl runat="server" DataField="PaidAmount" ID="PaidAmount" Mode="ReadOnly" />
                        </td>
                        <td>
                            <asp:DynamicControl runat="server" DataField="PaymentDate" ID="PaymentDate" Mode="ReadOnly" />
                        </td>
                        <td>
                            <asp:DynamicControl runat="server" DataField="Notes" ID="Notes" Mode="ReadOnly" />
                        </td>
                        <td>
                            <asp:HyperLink runat="server" NavigateUrl='<%# FriendlyUrl.Href("~/Admins/Salaries/Details", Item.TeacherID, yearTxt.Text, monthDropDown.SelectedIndex) %>' Text="التفاصيل" />

                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
    <div>
    </div>
</asp:Content>

