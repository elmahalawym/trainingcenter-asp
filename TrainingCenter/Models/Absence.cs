﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TrainingCenter.Models
{
    public class Absence
    {
        [Key, Column(Order=0)]
        [ForeignKey("ApplicationUser")]
        public string StudentID { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Session")]
        public int SessionID { get; set; }

        public virtual Session Session { get; set; }
    }
}