﻿<%@ Page Title="TeacherDiscountInsert" Language="C#" MasterPageFile="~/Site.Master" CodeBehind="Insert.aspx.cs" Inherits="TrainingCenter.Admins.Discounts.Insert" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div>
        <p>&nbsp;</p>
        <asp:ValidationSummary runat="server" CssClass="alert alert-danger" />
        <p class="text-danger">
            <asp:Literal ID="errorMessage" runat="server" />
        </p>

        <div class="form-horizontal">


            <!-- teacher -->
            <div class="form-group">
                <div class="col-md-10">
                    <asp:DropDownList runat="server" ID="teachersDropDown" CssClass="form-control"
                        AppendDataBoundItems="true" DataValueField="Id" DataTextField="username">
                        <asp:ListItem Text="اختر المدرس" Value="" />
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="يجب إختيار المدرس"
                        ControlToValidate="teachersDropDown" CssClass="text-danger" />
                </div>
                <asp:Label runat="server" AssociatedControlID="teachersDropDown" CssClass="col-md-2 control-label">المدرس</asp:Label>
            </div>

            <!-- Discount Date -->
            <div class="form-group">
                <div class="col-md-10">
                    <asp:Calendar ID="discountCalendar" runat="server" SelectedDate="<%# DateTime.Today %>" />
                </div>
                <asp:Label runat="server" AssociatedControlID="discountCalendar" CssClass="col-md-2 control-label">تاريخ الخصم</asp:Label>
            </div>

            <!-- Discount Type -->
            <div class="form-group">
                <div class="col-md-10">
                    <asp:DropDownList ID="discountTypeDropDown" runat="server" AppendDataBoundItems="true"
                        CssClass="form-control">
                        <asp:ListItem Text="اختر نوع الخصم" Value="" />
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="يجب تحديد نوع الخصم"
                        ControlToValidate="discountTypeDropDown" CssClass="text-danger" />
                </div>
                <asp:Label runat="server" AssociatedControlID="discountTypeDropDown" CssClass="col-md-2 control-label">نوع الخصم</asp:Label>
            </div>


            <!-- Amount -->
            <div class="form-group">
                <div class="col-md-10">
                    <asp:TextBox ID="amountTxt" runat="server" TextMode="Number" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ErrorMessage="يجب تحديد القيمة"
                        ControlToValidate="amountTxt" CssClass="text-danger" />
                </div>
                <asp:Label runat="server" AssociatedControlID="amountTxt" CssClass="col-md-2 control-label">قيمة الخصم</asp:Label>
            </div>

            <!-- Notes -->
            <div class="form-group">
                <div class="col-md-10">
                    <asp:TextBox ID="notesTxt" runat="server" CssClass="form-control" TextMode="MultiLine" />
                </div>
                <asp:Label runat="server" AssociatedControlID="notesTxt" CssClass="col-md-2 control-label">ملاحظات</asp:Label>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <asp:Button runat="server" ID="InsertButton" OnClick="InsertButton_Click" Text="إضافة"
                        CssClass="btn btn-primary" />
                    <asp:Button runat="server" ID="CancelButton" OnClick="CancelButton_Click"
                        Text="إلغاء" CausesValidation="false" CssClass="btn btn-default" />
                </div>
            </div>


        </div>



        <%--               <asp:FormView runat="server"
            ItemType="TrainingCenter.Models.TeacherDiscount" DefaultMode="Insert"
            InsertItemPosition="FirstItem" InsertMethod="InsertItem"
            OnItemCommand="ItemCommand" RenderOuterTable="false">
            <InsertItemTemplate>
                <fieldset class="form-horizontal">
				<legend>Insert TeacherDiscount</legend>
		        <asp:ValidationSummary runat="server" CssClass="alert alert-danger" />
							<asp:DynamicControl Mode="Insert" 
								DataField="TeacherID" 
								DataTypeName="TrainingCenter.Models.ApplicationUser" 
								DataTextField="FirstName" 
								DataValueField="Id" 
								UIHint="ForeignKey" runat="server" />
						    <asp:DynamicControl Mode="Insert" DataField="DiscountDate" runat="server" />
						    <asp:DynamicControl Mode="Insert" DataField="DiscountType" runat="server" />
						    <asp:DynamicControl Mode="Insert" DataField="Amount" runat="server" />
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button runat="server" ID="InsertButton" CommandName="Insert" Text="Insert" CssClass="btn btn-primary" />
                            <asp:Button runat="server" ID="CancelButton" CommandName="Cancel" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" />
                        </div>
					</div>
                </fieldset>
            </InsertItemTemplate>
        </asp:FormView>--%>
    </div>
</asp:Content>
