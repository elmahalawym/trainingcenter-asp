﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using TrainingCenter.Models;

namespace TrainingCenter.Admins.Exams
{
    public partial class Default : System.Web.UI.Page
    {
		protected TrainingCenter.Models.ApplicationDbContext _db = new TrainingCenter.Models.ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            (Page.Master as Admins).SelectedMenuItem = "exams";

            if(!IsPostBack)
            {
                // set year default value to current year
                yearTxt.Text = DateTime.Now.Year.ToString();

                // bind school years
                schoolYearsDropDown.DataSource = _db.SchoolYears.ToList();
                schoolYearsDropDown.DataBind();
            }
        }

        // Model binding method to get List of Exam entries
        // USAGE: <asp:ListView SelectMethod="GetData">
        public IQueryable<TrainingCenter.Models.Exam> GetData([System.Web.ModelBinding.Control]string yearTxt,
            [System.Web.ModelBinding.Control]int? monthDropDown, [System.Web.ModelBinding.Control] int? halfDropDown,
            [System.Web.ModelBinding.Control]int? schoolYearsDropDown)
        {
            var query = _db.Exams.Include(m => m.Course).Include(m => m.Course.SchoolYear);

            // filter using year
            int year = -1;
            if (!string.IsNullOrEmpty(yearTxt) && int.TryParse(yearTxt, out year))
                query = query.Where(e => e.Year == year);

            // filter using month
            if (monthDropDown != null && monthDropDown != -1)
                query = query.Where(e => e.Month == monthDropDown);

            // filter using half (first/second)
            if (halfDropDown != null && halfDropDown != -1)
                query = query.Where(e => e.Half == halfDropDown);

            // filter by school year
            if (schoolYearsDropDown != null && schoolYearsDropDown != -1)
                query = query.Where(e => e.Course.SchoolYearID == schoolYearsDropDown);

            return query;
        }

        protected void examCollectionBtn_Click(object sender, EventArgs e)
        {

        }
    }
}

