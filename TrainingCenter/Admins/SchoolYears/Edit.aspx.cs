﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using Microsoft.AspNet.FriendlyUrls.ModelBinding;
using TrainingCenter.Models;
namespace TrainingCenter.SchoolYears
{
    public partial class Edit : System.Web.UI.Page
    {
		protected TrainingCenter.Models.ApplicationDbContext _db = new TrainingCenter.Models.ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        // This is the Update methd to update the selected SchoolYear item
        // USAGE: <asp:FormView UpdateMethod="UpdateItem">
        public void UpdateItem(int  SchoolYearID)
        {
            using (_db)
            {
                var item = _db.SchoolYears.Find(SchoolYearID);

                if (item == null)
                {
                    // The item wasn't found
                    ModelState.AddModelError("", String.Format("Item with id {0} was not found", SchoolYearID));
                    return;
                }

                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    // Save changes here
                    _db.SaveChanges();

                    // update prices
                    Helpers.UpdatePrices(Server.MapPath("~/App_Data/Prices.xml"));

                    Response.Redirect("../Default");
                }
            }
        }

        // This is the Select method to selects a single SchoolYear item with the id
        // USAGE: <asp:FormView SelectMethod="GetItem">
        public TrainingCenter.Models.SchoolYear GetItem([FriendlyUrlSegmentsAttribute(0)]int? SchoolYearID)
        {
            if (SchoolYearID == null)
            {
                return null;
            }

            using (_db)
            {
                return _db.SchoolYears.Find(SchoolYearID);
            }
        }

        protected void ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Cancel", StringComparison.OrdinalIgnoreCase))
            {
                Response.Redirect("../Default");
            }
        }
    }
}
