﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using TrainingCenter.Models;

namespace TrainingCenter.Admins.Teachers
{
    public partial class PasswordReset : System.Web.UI.Page
    {
        protected ApplicationDbContext _db = new ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            // navigate if no userid is provided
            if (FriendlyUrl.Segments.Count == 0)
                Response.Redirect("~/Admins/Teachers/Default");
        }

        protected void ResetPassword_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                // get student role id
                var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_db));
                string teacherRoleId = RoleManager.FindByName("teacher").Id;

                // get user id
                string userId = FriendlyUrl.Segments[0];

                // get selected user
                var user = _db.Users.Where(u => u.Id == userId && u.Roles.Any(r => r.RoleId == teacherRoleId)).FirstOrDefault();

                if (user == null) // user is not found
                {
                    errorMessage.Text = "Invalid User Id";
                    return;
                }

                // change user password
                var userManager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                userManager.RemovePassword(userId);
                userManager.AddPassword(userId, newPasswordTxt.Text);

                // navigate back to default page
                Response.Redirect("~/Admins/Teachers/Default");

            }
        }
    }
}