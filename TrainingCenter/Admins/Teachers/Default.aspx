﻿<%@ Page Title="TeacherViewModelList" Language="C#" MasterPageFile="~/Admins/Admins.master" CodeBehind="Default.aspx.cs" Inherits="TrainingCenter.Admins.TeacherViewModels.Default" %>

<%@ Register TagPrefix="FriendlyUrls" Namespace="Microsoft.AspNet.FriendlyUrls" %>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">

    <h2>المدرسين</h2>
    <p>
        <asp:HyperLink runat="server" NavigateUrl="Insert" Text="إضافة مدرس" />
    </p>

    <div class="form-inline">
        <asp:Label runat="server" Text="الإسم الأول" AssociatedControlID="firstNameTxt"></asp:Label>
        <asp:TextBox runat="server" ID="firstNameTxt" CssClass="form-control" AutoPostBack="true"></asp:TextBox>

        <asp:Label runat="server" Text="الإسم العائلة" AssociatedControlID="lastNameTxt"></asp:Label>
        <asp:TextBox runat="server" ID="lastNameTxt" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
    </div>
    <br />

    <div>
        <asp:ListView ID="ListView1" runat="server"
            DataKeyNames="ApplicationUserID"
            ItemType="TrainingCenter.Models.TeacherViewModel"
            SelectMethod="GetData">
            <EmptyDataTemplate>
            لا يوجد مدرسين
            </EmptyDataTemplate>
            <LayoutTemplate>
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>
                                <asp:LinkButton Text="الإسم" CommandName="Sort" CommandArgument="FirstName" runat="Server" />
                            </th>
<%--                            <th>
                                <asp:LinkButton Text="إسم العائلة" CommandName="Sort" CommandArgument="LastName" runat="Server" />
                            </th>--%>
                            <th>
                                <asp:LinkButton Text="رقم التليفون" CommandName="Sort" CommandArgument="Phone" runat="Server" />
                            </th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr runat="server" id="itemPlaceholder" />
                    </tbody>
                </table>
<%--                <asp:DataPager PageSize="5" runat="server">
                    <Fields>
                        <asp:NextPreviousPagerField FirstPageText="الأولي" LastPageText="الأخيرة" NextPageText="التالي" PreviousPageText="السابق" ShowLastPageButton="False" ShowNextPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                        <asp:NumericPagerField ButtonType="Button" NumericButtonCssClass="btn" CurrentPageLabelCssClass="btn disabled" NextPreviousButtonCssClass="btn" />
                        <asp:NextPreviousPagerField FirstPageText="الأولي" LastPageText="الأخيرة" NextPageText="التالي" PreviousPageText="السابق"  ShowFirstPageButton="False" ShowPreviousPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                    </Fields>
                </asp:DataPager>--%>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <%--<asp:DynamicControl runat="server" DataField="FirstName" ID="FirstName" Mode="ReadOnly" />--%>
                        <%# string.Format("{0} {1}", Item.FirstName, Item.LastName) %>
                    </td>
<%--                    <td>
                        <asp:DynamicControl runat="server" DataField="LastName" ID="LastName" Mode="ReadOnly" />
                    </td>--%>
                    <td>
                        <asp:DynamicControl runat="server" DataField="Phone" ID="Phone" Mode="ReadOnly" />
                    </td>
                    <td>
                        <asp:HyperLink runat="server" NavigateUrl='<%# FriendlyUrl.Href("~/Admins/Teachers/Details", Item.ApplicationUserID) %>' Text="التفاصيل" />
                        | 
					    <asp:HyperLink runat="server" NavigateUrl='<%# FriendlyUrl.Href("~/Admins/Teachers/Edit", Item.ApplicationUserID) %>' Text="تعديل" />
                        | 
                        <asp:HyperLink runat="server" NavigateUrl='<%# FriendlyUrl.Href("~/Admins/Teachers/Delete", Item.ApplicationUserID) %>' Text="حذف" />
                        |
                        <a href="<%#: FriendlyUrl.Href("~/Admins/Teachers/PasswordReset/" + Item.ApplicationUserID) %>">إعادة كلمة السر</a>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
</asp:Content>

