namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditedSalaryModel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Salaries", "TeacherID", "dbo.AspNetUsers");
            DropIndex("dbo.Salaries", new[] { "TeacherID" });
            DropPrimaryKey("dbo.Salaries");
            AddColumn("dbo.Salaries", "Notes", c => c.String());
            AlterColumn("dbo.Salaries", "TeacherID", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Salaries", new[] { "TeacherID", "Year", "Month" });
            CreateIndex("dbo.Salaries", "TeacherID");
            AddForeignKey("dbo.Salaries", "TeacherID", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            DropColumn("dbo.Salaries", "SalaryID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Salaries", "SalaryID", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.Salaries", "TeacherID", "dbo.AspNetUsers");
            DropIndex("dbo.Salaries", new[] { "TeacherID" });
            DropPrimaryKey("dbo.Salaries");
            AlterColumn("dbo.Salaries", "TeacherID", c => c.String(maxLength: 128));
            DropColumn("dbo.Salaries", "Notes");
            AddPrimaryKey("dbo.Salaries", "SalaryID");
            CreateIndex("dbo.Salaries", "TeacherID");
            AddForeignKey("dbo.Salaries", "TeacherID", "dbo.AspNetUsers", "Id");
        }
    }
}
