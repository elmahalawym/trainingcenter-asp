﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using TrainingCenter.Models;
using System.Web.ModelBinding;

namespace TrainingCenter.Admins.Salaries
{
    public partial class Default : System.Web.UI.Page
    {
		protected TrainingCenter.Models.ApplicationDbContext _db = new TrainingCenter.Models.ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            (Page.Master as Admins).SelectedMenuItem = "salaries";

            if(!IsPostBack)
            {
                // set default year, month
                yearTxt.Text = DateTime.Now.Year.ToString();
                monthDropDown.SelectedIndex = DateTime.Now.Month;
            }
        }

        // Model binding method to get List of Salary entries
        // USAGE: <asp:ListView SelectMethod="GetData">
        public IQueryable<TrainingCenter.Models.Salary> GetData([Control("yearTxt")]int? year, [Control("monthDropDown")]int? month, [Control("stateDropDown")]int? stateDropDown)
        {
            if (year == null || month == null || month == -1)
                return null;

            var query = _db.Salaries.Include(m => m.ApplicationUser).Where(s => s.Year == year && s.Month == month);

            if(stateDropDown != null && stateDropDown != -1)
            {
                bool isPaid = stateDropDown == 1;
                query = query.Where(s => s.IsPaid == isPaid);
            }

            return query;
        }

        // The return type can be changed to IEnumerable, however to support
        // paging and sorting, the following parameters must be added:
        //     int maximumRows
        //     int startRowIndex
        //     out int totalRowCount
        //     string sortByExpression
        //public IQueryable<TrainingCenter.Models.SalaryDetails> salariesListView_GetData([Control("yearTxt")]int? year, [Control("monthDropDown")]int? month)
        //{
        //    if(year != null && month != null && month != -1)
        //    {
        //        List<SalaryDetails> salaryDetails = Helpers.GetSalaries(Server.MapPath("~/App_Data/" + Helpers.GetSalariesFileName
        //            ((int)year, (int)month)));
        //        return (salaryDetails != null) ? salaryDetails.AsQueryable() : null;
        //    }

        //    return null;
        //}

    }
}

