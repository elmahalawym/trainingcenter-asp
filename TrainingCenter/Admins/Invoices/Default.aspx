﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admins/Admins.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TrainingCenter.Admins.Invoices.Default" %>

<%@ Register TagPrefix="FriendlyUrls" Namespace="Microsoft.AspNet.FriendlyUrls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <br />

    <asp:HyperLink runat="server" NavigateUrl="/Admins/Invoices/Calculate"
        Text="حساب المصروفات" />

    <br />
    <br />

    <div class="form-inline">
        <asp:Label runat="server" AssociatedControlID="yearTxt" Text="السنة" CssClass="control-label" />
        <asp:TextBox runat="server" ID="yearTxt" TextMode="Number" CssClass="form-control" AutoPostBack="true" />
        <asp:Label runat="server" AssociatedControlID="monthDropDown" Text="الشهر" CssClass="control-label" />
        <asp:DropDownList runat="server" ID="monthDropDown" CssClass="form-control" AutoPostBack="true">
            <asp:ListItem Text="اختر الشهر" Value="-1" />
            <asp:ListItem Text="يناير" Value="1" />
            <asp:ListItem Text="فبراير" Value="2" />
            <asp:ListItem Text="مارس" Value="3" />
            <asp:ListItem Text="إبريل" Value="4" />
            <asp:ListItem Text="مايو" Value="5" />
            <asp:ListItem Text="يونيو" Value="6" />
            <asp:ListItem Text="يوليو" Value="7" />
            <asp:ListItem Text="أغسطس" Value="8" />
            <asp:ListItem Text="سبتمبر" Value="9" />
            <asp:ListItem Text="أكتوبر" Value="10" />
            <asp:ListItem Text="نوفمبر" Value="11" />
            <asp:ListItem Text="ديسمبر" Value="12" />
        </asp:DropDownList>

        <asp:Label runat="server" AssociatedControlID="monthDropDown" Text="الشهر" CssClass="control-label" />
        <asp:DropDownList runat="server" ID="schoolYearsDropDown" CssClass="form-control" AutoPostBack="true" AppendDataBoundItems="true" DataTextField="Text" DataValueField="Value">
            <asp:ListItem Text="كل السنوات" Value="-1" />
        </asp:DropDownList>
        <asp:Label runat="server" AssociatedControlID="stateDropDown" Text="الحالة" CssClass="control-label" />
        <asp:DropDownList runat="server" ID="stateDropDown" CssClass="form-control" AutoPostBack="true">
            <asp:ListItem Text="الكل" Value="-1" />
            <asp:ListItem Text="مدفوع" Value="1" />
            <asp:ListItem Text="غير مدفوع" Value="2" />
        </asp:DropDownList>

    </div>
    <br />
    <br />

    <asp:ListView ID="invoicesListView" runat="server"
        ItemType="TrainingCenter.Models.Invoice"
        SelectMethod="GetData">
        <EmptyDataTemplate>
            لا توجد بيانات
        </EmptyDataTemplate>
        <LayoutTemplate>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>اسم الطالب</th>
                        <th>المصروفات</th>
                        <th>مدفوع</th>
                        <th>القيمة المدفوعة</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr runat="server" id="itemPlaceholder" />
                </tbody>
            </table>
            <%--            <asp:DataPager PageSize="5" runat="server">
                <Fields>
                    <asp:NextPreviousPagerField ShowLastPageButton="False" ShowNextPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                    <asp:NumericPagerField ButtonType="Button" NumericButtonCssClass="btn" CurrentPageLabelCssClass="btn disabled" NextPreviousButtonCssClass="btn" />
                    <asp:NextPreviousPagerField ShowFirstPageButton="False" ShowPreviousPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                </Fields>
            </asp:DataPager>--%>
<%--            <asp:DataPager PageSize="5" runat="server">
                <Fields>
                    <asp:NextPreviousPagerField FirstPageText="الأولي" LastPageText="الأخيرة" NextPageText="التالي" PreviousPageText="السابق" ShowLastPageButton="False" ShowNextPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                    <asp:NumericPagerField ButtonType="Button" NumericButtonCssClass="btn" CurrentPageLabelCssClass="btn disabled" NextPreviousButtonCssClass="btn" />
                    <asp:NextPreviousPagerField FirstPageText="الأولي" LastPageText="الأخيرة" NextPageText="التالي" PreviousPageText="السابق" ShowFirstPageButton="False" ShowPreviousPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                </Fields>
            </asp:DataPager>--%>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <%# Item.ApplicationUser.FirstName + " " + Item.ApplicationUser.LastName %>
                </td>
                <td>
                    <%# Item.Price %>
                </td>
                <td>
                    <%# Item.Paid ? "نعم" : "لا" %>
                </td>
                <td>
                    <%# Item.PaidAmount %>
                </td>
                <td>
                    <a href="<%#: FriendlyUrl.Href("~/Admins/Invoices/Edit/", yearTxt.Text, monthDropDown.SelectedIndex, Item.StudentID) %>">تعديل</a>
                </td>
            </tr>
        </ItemTemplate>
    </asp:ListView>
</asp:Content>
