﻿<%@ Page Title="StudentViewModelList" Language="C#" MasterPageFile="~/Admins/Admins.master" CodeBehind="Default.aspx.cs" Inherits="TrainingCenter.Admins.StudentViewModels.Default" %>

<%@ Register TagPrefix="FriendlyUrls" Namespace="Microsoft.AspNet.FriendlyUrls" %>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">

    <h2>الطلاب</h2>
    <p>
        <asp:HyperLink runat="server" NavigateUrl="Insert" Text="إضافة طالب جديد" />
    </p>

    <div class="form-inline">
        <asp:Label runat="server" Text="الإسم الأول" AssociatedControlID="firstNameTxt"></asp:Label>
        <asp:TextBox runat="server" ID="firstNameTxt" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
        <asp:Label runat="server" Text="إسم العائلة" AssociatedControlID="lastNameTxt"></asp:Label>
        <asp:TextBox runat="server" ID="lastNameTxt" CssClass="form-control" AutoPostBack="true"></asp:TextBox>
        <asp:Label runat="server" Text="السنة الدراسية" AssociatedControlID="schoolYearDropDown"></asp:Label>
        <asp:DropDownList runat="server" CssClass="form-control" ID="schoolYearDropDown" AppendDataBoundItems="true" DataValueField="SchoolYearID" DataTextField="Name"
            ItemType="TrainingCenter.Models.SchoolYear" SelectMethod="schoolYearDropDown_GetData" AutoPostBack="true">
            <asp:ListItem Value="-1" Text="كل السنوات الدراسية" />
        </asp:DropDownList>

        <%--<asp:Button runat="server" Text="بحث" CssClass="btn btn-default"/>--%>
    </div>

    <br />

    <div>
        <asp:ListView ID="ListView1" runat="server"
            DataKeyNames="ApplicationUserID"
            ItemType="TrainingCenter.Models.StudentViewModel"
            SelectMethod="GetData">
            <EmptyDataTemplate>
                 لا يوجد طلاب
            </EmptyDataTemplate>
           
            <LayoutTemplate>
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <%--                            <th>
                                <asp:LinkButton Text="اسم المستخدم" CommandName="Sort" CommandArgument="UserName" runat="Server" />
                            </th>--%>
                            <th>
                                <asp:LinkButton Text="إسم الطالب" CommandName="Sort" CommandArgument="FirstName" runat="Server" />
                            </th>
                            <th>السنة الدراسية
                            </th>
                            <%--                            <th>
                                <asp:LinkButton Text="إسم العائلة" CommandName="Sort" CommandArgument="LastName" runat="Server" />
                            </th>--%>

                            <th>
                                <asp:LinkButton Text="نوع الإشتراك" CommandName="Sort" CommandArgument="Phone" runat="Server" />
                            </th>

                            <th>اشتراك الشهر</th>

                            <th>
                                <asp:LinkButton Text="رقم التليفون" CommandName="Sort" CommandArgument="Phone" runat="Server" />
                            </th>

                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr runat="server" id="itemPlaceholder" />
                    </tbody>
                </table>
                <asp:DataPager PageSize="5" runat="server">
                    <Fields>
                        <asp:NextPreviousPagerField FirstPageText="الأولي" LastPageText="الأخيرة" NextPageText="التالي" PreviousPageText="السابق" ShowLastPageButton="False" ShowNextPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                        <asp:NumericPagerField ButtonType="Button" NumericButtonCssClass="btn" CurrentPageLabelCssClass="btn disabled" NextPreviousButtonCssClass="btn" />
                        <asp:NextPreviousPagerField FirstPageText="الأولي" LastPageText="الأخيرة" NextPageText="التالي" PreviousPageText="السابق" ShowFirstPageButton="False" ShowPreviousPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                    </Fields>
                </asp:DataPager>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <%--                    <td>
                        <asp:DynamicControl runat="server" DataField="UserName" ID="UserName" Mode="ReadOnly" />
                    </td>--%>
                    <td>
                        <asp:Label runat="server"><%#: Item.FirstName + " " + Item.LastName %></asp:Label>
                    </td>
                    <td>
                        <asp:DynamicControl runat="server" DataField="SchoolYear" ID="SchoolYear" Mode="ReadOnly" />
                    </td>
                    <%--                    <td>
                        <asp:DynamicControl runat="server" DataField="LastName" ID="LastName" Mode="ReadOnly" />
                    </td>--%>

                    <td>
                        <asp:Label runat="server"><%#: Item.GeneralType ? "كل المواد" : "بعض المواد" %></asp:Label>
                        <%--<asp:DynamicControl runat="server" DataField="GeneralType" ID="GeneralType" Mode="ReadOnly" />--%>
                    </td>
                    <td>
                        <%# Item.IsHalfMonth ? "نصف شهر" : "شهر كامل" %>
                    </td>
                    <td>
                        <asp:DynamicControl runat="server" DataField="Phone" ID="Phone" Mode="ReadOnly" />
                    </td>

                    <td>
                        <asp:HyperLink runat="server" NavigateUrl='<%#: FriendlyUrl.Href("~/Admins/Students/Details", Item.ApplicationUserID) %>' Text="التفاصيل" />
                        | 
					    <asp:HyperLink runat="server" NavigateUrl='<%#: FriendlyUrl.Href("~/Admins/Students/Edit", Item.ApplicationUserID) %>' Text="تعديل" />
                        | 
                        <asp:HyperLink runat="server" NavigateUrl='<%#: FriendlyUrl.Href("~/Admins/Students/Delete", Item.ApplicationUserID) %>' Text="حذف" />

                        <br />
                        <asp:HyperLink runat="server" NavigateUrl='<%#: FriendlyUrl.Href("~/Admins/Students/Grades", Item.ApplicationUserID) %>' Text="الدرجات" />
                        |
                        <a href="<%#: FriendlyUrl.Href("~/Admins/Students/PasswordReset/" + Item.ApplicationUserID) %>">إعادة كلمة السر</a>

                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>

        <br />
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>الأعداد</h3>
            </div>
            <div class="panel-body">
                <asp:ListView runat="server" ItemType="TrainingCenter.Models.NumberOfStudents"
                    SelectMethod="NumberOfStudents_GetData">
                    <LayoutTemplate>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>المادة</th>
                                    <th>العام (كل المواد)</th>
                                    <th>الخاص )بعض المواد(</th>
                                </tr>
                            </thead>
                            <tbody>
                                <div id="itemPlaceHolder" runat="server"></div>
                            </tbody>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%# Item.CourseName + " " + Item.SchoolYearName %></td>
                            <td><%# Item.NumberOfStudents_General %></td>
                            <td><%# Item.NumberOfStudents_Special %></td>
                        </tr>

                    </ItemTemplate>
                </asp:ListView>
                <br />
                <asp:Button ID="updateBtn" runat="server" CssClass="btn btn-primary" Text="تحديث" OnClick="updateBtn_Click" />
            </div>
        </div>
    </div>
</asp:Content>

