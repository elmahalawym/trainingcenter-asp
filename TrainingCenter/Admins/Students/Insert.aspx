﻿<%@ Page Title="StudentViewModelInsert" Language="C#" MasterPageFile="~/Site.Master"
    CodeBehind="Insert.aspx.cs" Inherits="TrainingCenter.Admins.StudentViewModels.Insert" MaintainScrollPositionOnPostback="true" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div>
        <p>&nbsp;</p>


        <p class="text-danger">
            <asp:Literal runat="server" ID="ErrorMessage" />
        </p>

        <div class="form-horizontal">
            <h4>إضافة طالب</h4>
            <hr />
            <asp:ValidationSummary runat="server" CssClass="alert alert-danger" />

            <div class="panel panel-default">
                <div class="panel-heading">
                    بيانات الطالب
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="usernameTxt" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="usernameTxt"
                                CssClass="text-danger" ErrorMessage="يجب إدخال اسم المستخدم" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="usernameTxt" CssClass="col-md-2 control-label">اسم المستخدم</asp:Label>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="emailTxt" CssClass="form-control" TextMode="Email" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" CssClass="text-danger" runat="server" ErrorMessage="ex: student@gmail.com" ControlToValidate="emailTxt" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                        </div>
                        <asp:Label runat="server" AssociatedControlID="emailTxt" CssClass="col-md-2 control-label">الإيميل</asp:Label>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10">

                            <asp:DropDownList runat="server" ID="schoolYearDropDown"
                                DataValueField="SchoolYearID" DataTextField="Name"
                                ItemType="TrainingCenter.Models.SchoolYear"
                                CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="schoolYearDropDown_SelectedIndexChanged"
                                AppendDataBoundItems="true">
                                <asp:ListItem Text="اختر السنة الدراسية" Value="" />
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="schoolYearDropDown"
                                CssClass="text-danger" ErrorMessage="يجب إدخال السنة الدراسية" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="schoolYearDropDown" CssClass="col-md-2 control-label">السنة الدراسية</asp:Label>
                    </div>

                    <!-- Courses -->
                    <div class="form-group">
                        <div class="col-md-10">
                            <asp:CheckBoxList ID="coursesList" runat="server"
                                DataTextField="CourseName" DataValueField="CourseID">
                            </asp:CheckBoxList>
                            <span class="btn btn-default checkAllBtn" data-target="<%= coursesList.ClientID %>">تحديد الكل</span>
                        </div>
                        <asp:Label runat="server" AssociatedControlID="coursesList" CssClass="col-md-2 control-label">المواد</asp:Label>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10">
                            <asp:DropDownList ID="curMonthSubType" runat="server" CssClass="form-control">
                                <asp:ListItem Text="شهر كامل" Value="1" />
                                <asp:ListItem Text="نصف شهر" Value="2" />
                            </asp:DropDownList>
                        </div>
                        <asp:Label runat="server" AssociatedControlID="curMonthSubType" CssClass="col-md-2 control-label">اشتراك الشهر الحالي</asp:Label>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="passwordTxt" CssClass="form-control" />
                            <span id="generateBtn" class="btn btn-default" data-target="<%= passwordTxt.ClientID %>">generate</span>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="passwordTxt"
                                CssClass="text-danger" ErrorMessage="يجب إدخال كلمة السر" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="passwordTxt" CssClass="col-md-2 control-label">كلمة السر</asp:Label>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="firstNameTxt" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="firstNameTxt"
                                CssClass="text-danger" ErrorMessage="يجب إدخال الإسم الأول" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="firstNameTxt" CssClass="col-md-2 control-label">الإسم الأول</asp:Label>
                    </div>
                    <div class="form-group">

                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="lastNameTxt" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="lastNameTxt"
                                CssClass="text-danger" Display="Dynamic" ErrorMessage="يجب إدخال اسم العائلة" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="lastNameTxt" CssClass="col-md-2 control-label">اسم العائلة</asp:Label>
                    </div>
                    <div class="form-group">

                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="phoneTxt" CssClass="form-control" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="phoneTxt"
                                CssClass="text-danger" Display="Dynamic" ErrorMessage="يجب إدخال رقم التليفون" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="phoneTxt" CssClass="col-md-2 control-label">رقم التليفون</asp:Label>
                    </div>


                    <!-- Mobile1 -->
                    <div class="form-group">

                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="mobile1Txt" CssClass="form-control" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="mobile1Txt" CssClass="col-md-2 control-label">موبايل 1</asp:Label>
                    </div>


                    <!-- Mobile2 -->
                    <div class="form-group">

                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="mobile2Txt" CssClass="form-control" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="mobile2Txt" CssClass="col-md-2 control-label">موبايل 2</asp:Label>
                    </div>


                    <div class="form-group">

                        <div class="col-md-10">
                            <%--<asp:CheckBox runat="server" ID="generalTypeCB" />--%>
                            <asp:DropDownList ID="generalTypeDropDown" runat="server" CssClass="form-control">
                                <asp:ListItem Text="كل المواد" Value="1" />
                                <asp:ListItem Text="بعض المواد" Value="0" />
                            </asp:DropDownList>
                        </div>
                        <asp:Label runat="server" AssociatedControlID="generalTypeDropDown" CssClass="col-md-2 control-label">نوع الإشتراك</asp:Label>
                    </div>

                    <!-- اسم المدرسة -->
                    <div class="form-group">

                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="schoolNameTxt" CssClass="form-control" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="schoolNameTxt" CssClass="col-md-2 control-label">اسم المدرسة</asp:Label>
                    </div>

                    <!-- Birth Date -->
                    <div class="form-group">

                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="birthDateTxt" CssClass="form-control" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="birthDateTxt" CssClass="col-md-2 control-label">تاريخ الميلاد</asp:Label>
                    </div>



                    <!-- Is Active -->
                    <div class="form-group">

                        <div class="col-md-10">
                            <asp:CheckBox runat="server" ID="isActiveCB" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="isActiveCB" CssClass="col-md-2 control-label">Is Active</asp:Label>
                    </div>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    بيانات ولي الأمر
                </div>

                <div class="panel-body">

                    <!-- Father Mobile -->
                    <div class="form-group">

                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="fatherPhoneTxt" CssClass="form-control" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="fatherPhoneTxt" CssClass="col-md-2 control-label">رقم تليفون الوالد</asp:Label>
                    </div>


                    <!-- Mother Mobile -->
                    <div class="form-group">

                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="motherPhoneTxt" CssClass="form-control" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="motherPhoneTxt" CssClass="col-md-2 control-label">رقم تليفون الوالدة</asp:Label>
                    </div>



                    <!-- Notes -->
                    <div class="form-group">

                        <div class="col-md-10">
                            <asp:TextBox runat="server" ID="notesTxt" TextMode="MultiLine" CssClass="form-control" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="notesTxt" CssClass="col-md-2 control-label">ملاحظات</asp:Label>
                    </div>

                </div>


            </div>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <asp:Button runat="server" OnClick="CreateStudent_Click" Text="تسجيل" CssClass="btn btn-primary" />
                    <a href="Default.aspx" class="btn btn-default">إلغاء</a>
                </div>
            </div>



        </div>

        <%--  <asp:FormView runat="server"
            ItemType="TrainingCenter.Models.StudentViewModel" DefaultMode="Insert"
            InsertItemPosition="FirstItem" InsertMethod="InsertItem"
            OnItemCommand="ItemCommand" RenderOuterTable="false">
            <InsertItemTemplate>
                <fieldset class="form-horizontal">
                    <legend>Insert StudentViewModel</legend>
                    <asp:ValidationSummary runat="server" CssClass="alert alert-danger" />
                    <asp:DynamicControl Mode="Insert" DataField="UserName" runat="server" />
                    <asp:DynamicControl Mode="Insert" DataField="FirstName" runat="server" />
                    <asp:DynamicControl Mode="Insert" DataField="LastName" runat="server" />
                    <asp:DynamicControl Mode="Insert" DataField="Phone" runat="server" />
                    <asp:TextBox runat="server" ID="passwordTxt" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                    <asp:TextBox runat="server" ID="repeatPasswordTxt" TextMode="Password"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="CompareValidator"></asp:CompareValidator>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button runat="server" ID="InsertButton" CommandName="Insert" Text="Insert" CssClass="btn btn-primary" />
                            <asp:Button runat="server" ID="CancelButton" CommandName="Cancel" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" />
                        </div>
                    </div>
                </fieldset>
            </InsertItemTemplate>
        </asp:FormView>--%>
    </div>
</asp:Content>
