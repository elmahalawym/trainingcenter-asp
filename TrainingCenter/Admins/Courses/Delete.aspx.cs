﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using Microsoft.AspNet.FriendlyUrls.ModelBinding;
using TrainingCenter.Models;

namespace TrainingCenter.Admins.Courses
{
    public partial class Delete : System.Web.UI.Page
    {
		protected TrainingCenter.Models.ApplicationDbContext _db = new TrainingCenter.Models.ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        // This is the Delete methd to delete the selected Course item
        // USAGE: <asp:FormView DeleteMethod="DeleteItem">
        public void DeleteItem(int CourseID)
        {
            using (_db)
            {
                var item = _db.Courses.Find(CourseID);

                if (item != null)
                {
                    _db.Courses.Remove(item);
                    _db.SaveChanges();

                    // update prices
                    Helpers.UpdatePrices(Server.MapPath("~/App_Data/Prices.xml"));
                }
            }
            Response.Redirect("../Default");
        }

        // This is the Select methd to selects a single Course item with the id
        // USAGE: <asp:FormView SelectMethod="GetItem">
        public TrainingCenter.Models.Course GetItem([FriendlyUrlSegmentsAttribute(0)]int? CourseID)
        {
            if (CourseID == null)
            {
                return null;
            }

            using (_db)
            {
	            return _db.Courses.Where(m => m.CourseID == CourseID).Include(m => m.ApplicationUser).Include(m => m.SchoolYear).FirstOrDefault();
            }
        }

        protected void ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Cancel", StringComparison.OrdinalIgnoreCase))
            {
                Response.Redirect("../Default");
            }
        }
    }
}

