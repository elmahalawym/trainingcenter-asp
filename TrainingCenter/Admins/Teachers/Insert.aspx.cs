﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using TrainingCenter.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace TrainingCenter.Admins.TeacherViewModels
{
    public partial class Insert : System.Web.UI.Page
    {
        protected ApplicationDbContext _db = new ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void CreateStudent_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                var userManager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();

                var user = new ApplicationUser()
                {
                    UserName = usernameTxt.Text,
                    FirstName = firstNameTxt.Text,
                    LastName = lastNameTxt.Text,
                    Phone = phoneTxt.Text,
                    Email = emailTxt.Text,
                };
                IdentityResult result = userManager.Create(user, passwordTxt.Text);
                if (result.Succeeded)
                {
                    // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                    //string code = manager.GenerateEmailConfirmationToken(user.Id);
                    //string callbackUrl = IdentityHelper.GetUserConfirmationRedirectUrl(code, user.Id, Request);
                    //manager.SendEmail(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>.");

                    //IdentityHelper.SignIn(manager, user, isPersistent: false);
                    //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);

                    // add user to student role
                    userManager.AddToRole(user.Id, "teacher");

                    Response.Redirect("Default");
                }
                else
                {
                    ErrorMessage.Text = result.Errors.FirstOrDefault();
                }
            }
        }
    }
}
