namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoreStudentProperties : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "SchoolName", c => c.String());
            AddColumn("dbo.AspNetUsers", "BirthDate", c => c.DateTime());
            AddColumn("dbo.AspNetUsers", "FatherPhoneNumber", c => c.String());
            AddColumn("dbo.AspNetUsers", "MotherPhoneNumber", c => c.String());
            AddColumn("dbo.AspNetUsers", "Mobile1", c => c.String());
            AddColumn("dbo.AspNetUsers", "Mobile2", c => c.String());
            AddColumn("dbo.AspNetUsers", "Notes", c => c.String());
            AddColumn("dbo.AspNetUsers", "IsStudentActive", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "IsStudentActive");
            DropColumn("dbo.AspNetUsers", "Notes");
            DropColumn("dbo.AspNetUsers", "Mobile2");
            DropColumn("dbo.AspNetUsers", "Mobile1");
            DropColumn("dbo.AspNetUsers", "MotherPhoneNumber");
            DropColumn("dbo.AspNetUsers", "FatherPhoneNumber");
            DropColumn("dbo.AspNetUsers", "BirthDate");
            DropColumn("dbo.AspNetUsers", "SchoolName");
        }
    }
}
