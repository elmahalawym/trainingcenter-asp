﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="TrainingCenter.Admins.Invoices.Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <br />
    <br />
    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-md-10">
                <asp:Label runat="server" ID="studentNameLabel" CssClass="control-label" />
            </div>
            <asp:Label runat="server" CssClass="col-md-2 control-label">اسم الطالب</asp:Label>
        </div>

        <div class="form-group">
            <div class="col-md-10">
                <asp:Label runat="server" ID="expensesLabel" CssClass="control-label" />
            </div>
            <asp:Label runat="server" CssClass="col-md-2 control-label">المصروفات</asp:Label>
        </div>
        <div class="form-group">
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="paidAmountTxt" CssClass="form-control" TextMode="Number" />
            </div>
            <asp:Label runat="server" AssociatedControlID="paidAmountTxt" CssClass="col-md-2 control-label">القيمة المدفوعة</asp:Label>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <asp:Button runat="server" ID="UpdateButton" OnClick="UpdateButton_Click" Text="تحديث" CssClass="btn btn-primary" />
                <asp:Button runat="server" ID="CancelButton" OnClick="CancelButton_Click" Text="إلغاء" CausesValidation="false" CssClass="btn btn-default" />
            </div>
        </div>

        <p class="text-danger">
            <asp:Literal ID="ErrorMessage" runat="server" Text="" />
        </p>
    </div>



</asp:Content>
