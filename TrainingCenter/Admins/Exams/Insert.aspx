﻿<%@ Page Title="ExamInsert" Language="C#" MasterPageFile="~/Site.Master" CodeBehind="Insert.aspx.cs" Inherits="TrainingCenter.Admins.Exams.Insert" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div>
        <p>&nbsp;</p>
        <asp:ValidationSummary runat="server" CssClass="alert alert-danger" />
        <p class="text-danger">
            <asp:Literal runat="server" ID="errorMessage" />
        </p>
        <div class="form-horizontal">
            <h4>إضافة إمتحان</h4>

            <!-- Month -->
            <div class="form-group">
                <div class="col-md-10">
                    <asp:DropDownList runat="server" ID="monthDropDown" CssClass="form-control">
                        <asp:ListItem Text="كل الشهور" Value="-1" />
                        <asp:ListItem Text="يناير" Value="1" />
                        <asp:ListItem Text="فبراير" Value="2" />
                        <asp:ListItem Text="مارس" Value="3" />
                        <asp:ListItem Text="إبريل" Value="4" />
                        <asp:ListItem Text="مايو" Value="5" />
                        <asp:ListItem Text="يونيو" Value="6" />
                        <asp:ListItem Text="يوليو" Value="7" />
                        <asp:ListItem Text="أغسطس" Value="8" />
                        <asp:ListItem Text="سبتمبر" Value="9" />
                        <asp:ListItem Text="أكتوبر" Value="10" />
                        <asp:ListItem Text="نوفمبر" Value="11" />
                        <asp:ListItem Text="ديسمبر" Value="12" />
                    </asp:DropDownList>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="يجب إختيار الشهر"
                        ControlToValidate="monthDropDown" MinimumValue="1" Type="Integer" MaximumValue="12" CssClass="text-danger" />
                </div>
                <asp:Label runat="server" AssociatedControlID="monthDropDown" CssClass="col-md-2 control-label">الشهر</asp:Label>
            </div>
        

        <!-- Half -->
        <div class="form-group">
            <div class="col-md-10">
                <asp:DropDownList runat="server" ID="halfDropDown" CssClass="form-control">
                    <asp:ListItem Text="اختر النصف" Value="-1" />
                    <asp:ListItem Text="أول" Value="1" />
                    <asp:ListItem Text="ثاني" Value="2" />
                </asp:DropDownList>
                <asp:RangeValidator ID="RangeValidator2" runat="server" ErrorMessage="يجب إختيار النصف"
                    ControlToValidate="halfDropDown" MinimumValue="1" Type="Integer" MaximumValue="12" CssClass="text-danger" />
            </div>
            <asp:Label runat="server" AssociatedControlID="halfDropDown"
                CssClass="col-md-2 control-label">النصف</asp:Label>
        </div>

        <!-- Year -->
        <div class="form-group">
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="yearTxt" TextMode="Number" CssClass="form-control" />
            </div>
            <asp:Label runat="server" AssociatedControlID="yearTxt"
                CssClass="col-md-2 control-label">السنة</asp:Label>
        </div>

        <!-- school year -->
        <div class="form-group">
            <div class="col-md-10">
                <asp:DropDownList runat="server" ID="SchoolyearDropDown" DataTextField="Name"
                    CssClass="form-control" DataValueField="SchoolYearID" AppendDataBoundItems="true"
                    AutoPostBack="true">
                    <asp:ListItem Text="اختر السنة الدراسية" Value="-1" />
                </asp:DropDownList>
                <asp:RangeValidator ID="RangeValidator4" runat="server" ErrorMessage="يجب إختيار الكورس"
                    ControlToValidate="SchoolyearDropDown" MinimumValue="1" MaximumValue="999999999" Type="Integer" CssClass="text-danger" />
            </div>
            <asp:Label runat="server" AssociatedControlID="coursesDropDown"
                CssClass="col-md-2 control-label">السنة الدراسية</asp:Label>
        </div>

        <!-- Coruse -->
        <div class="form-group">
            <div class="col-md-10">
                <asp:DropDownList runat="server" ID="coursesDropDown" DataTextField="CourseName"
                    CssClass="form-control" DataValueField="CourseID" ItemType="TrainingCenter.Models.Course"
                    SelectMethod="GetCourses">
                </asp:DropDownList>
                <%--                    <asp:RangeValidator ID="RangeValidator3" runat="server" ErrorMessage="يجب إختيار الكورس"
                    ControlToValidate="coursesDropDown" MinimumValue="1" MaximumValue="999999999" Type="Integer"
                         CssClass="text-danger" />--%>
                <asp:CompareValidator ID="CompareValidator" runat="server" ErrorMessage="" ControlToValidate="coursesDropDown"
                    ValueToCompare="1" Operator="GreaterThanEqual" />
            </div>
            <asp:Label runat="server" AssociatedControlID="coursesDropDown"
                CssClass="col-md-2 control-label">الكورس</asp:Label>
        </div>

        <!-- Full Mark -->
        <div class="form-group">
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="fullMarkTxt" CssClass="form-control" TextMode="Number" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="text-danger"
                    ErrorMessage="يجب إدخال الدرجة النهائية" ControlToValidate="fullMarkTxt" />
            </div>
            <asp:Label runat="server" AssociatedControlID="fullMarkTxt"
                CssClass="col-md-2 control-label">الدرجة النهائية</asp:Label>
        </div>

        <!-- Exam Date -->
        <div class="form-group">
            <div class="col-md-10">
                <asp:Calendar runat="server" ID="calender" />
            </div>
            <asp:Label runat="server" AssociatedControlID="calender"
                CssClass="col-md-2 control-label">موعد الإمتحان</asp:Label>
        </div>

        <%--<asp:DynamicControl Mode="Insert"
                        DataField="CourseID"
                        DataTypeName="TrainingCenter.Models.Course"
                        DataTextField="TeacherID"
                        DataValueField="CourseID" 
                        UIHint="ForeignKey" runat="server" />
                    <asp:DynamicControl Mode="Insert" DataField="ExamDate" runat="server" />
                    <asp:DynamicControl Mode="Insert" DataField="FullMark" runat="server" />--%>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <asp:Button runat="server" ID="InsertButton" OnClick="InsertButton_Click" Text="إضافة"
                    CssClass="btn btn-primary" />
                <asp:Button runat="server" ID="CancelButton" OnClick="CancelButton_Click"
                    Text="إلغاء" CausesValidation="false" CssClass="btn btn-default" />
            </div>
        </div>
    </div>

    </div>
</asp:Content>
