﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrainingCenter.Models
{
    public class SchoolYear
    {
        public SchoolYear()
        {
            Courses = new List<Course>();
        }

        [Key]
        public int SchoolYearID { get; set; }

        [Required]
        [Display(Name="السنة الدراسية")]
        public string Name { get; set; }

        [Display(Name="السعر العام")]
        public double GeneralPrice { get; set; }

        [Display(Name = "السعر الخاص")]
        public double SpecialPrice { get; set; }

        public virtual ICollection<Course> Courses { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}