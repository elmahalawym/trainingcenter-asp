﻿<%@ Page Title="ExamEdit" Language="C#" MasterPageFile="~/Site.Master" CodeBehind="Edit.aspx.cs" Inherits="TrainingCenter.Admins.Exams.Edit" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div>
        <p>&nbsp;</p>
        <asp:ValidationSummary runat="server" CssClass="alert alert-danger" />
        <p class="text-danger">
            <asp:Literal runat="server" ID="errorMessage" />
        </p>

        <div class="form-horizontal">
            <h4>تعديل بيانات الإمتحان</h4>

            <!-- Exam Details -->
            <!-- Year -->
            <div class="form-group">
                <div class="col-md-10">
                    <asp:Label runat="server" ID="yearLabel" CssClass="control-label" />
                </div>
                <asp:Label runat="server" AssociatedControlID="yearLabel" CssClass="col-md-2 control-label">السنة</asp:Label>
            </div>

            <!-- Month -->
            <div class="form-group">
                <div class="col-md-10">
                    <asp:Label runat="server" ID="monthLabel" CssClass="control-label" />
                </div>
                <asp:Label runat="server" AssociatedControlID="monthLabel" CssClass="col-md-2 control-label">الشهر</asp:Label>
            </div>

            <!-- Half -->
            <div class="form-group">
                <div class="col-md-10">
                    <asp:Label runat="server" ID="halfLabel" CssClass="control-label" />
                </div>
                <asp:Label runat="server" AssociatedControlID="monthLabel" CssClass="col-md-2 control-label">النصف</asp:Label>
            </div>

            <!-- Course -->
            <div class="form-group">
                <div class="col-md-10">
                    <asp:Label runat="server" ID="courseLabel" CssClass="control-label" />
                </div>
                <asp:Label runat="server" AssociatedControlID="courseLabel" CssClass="col-md-2 control-label">الكورس</asp:Label>
            </div>

            <!-- Editable Controls -->
            <!-- Full Mark -->
            <div class="form-group">
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="fullMarkTxt" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="fullMarkTxt"
                        CssClass="text-danger" ErrorMessage="يجب إدخال الدرجة النهائية " />
                </div>
                <asp:Label runat="server" AssociatedControlID="fullMarkTxt" CssClass="col-md-2 control-label">الدرجة النهائية</asp:Label>
            </div>

            <!-- Exam Date -->
            <div class="form-group">
                <div class="col-md-10">
                    <asp:Calendar runat="server" ID="examDateCalendar" />
                </div>
                <asp:Label runat="server" AssociatedControlID="examDateCalendar" CssClass="col-md-2 control-label">تاريخ الإمتحان</asp:Label>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <asp:Button runat="server" ID="UpdateButton" OnClick="UpdateButton_Click" Text="تحديث" CssClass="btn btn-primary" />
                    <asp:Button runat="server" ID="DeleteButton" OnClick="DeleteButton_Click" Text="حذف" CssClass="btn btn-danger" />
                    <asp:Button runat="server" ID="CancelButton" OnClick="CancelButton_Click" Text="إلغاء" CausesValidation="false" CssClass="btn btn-default" />
                </div>
            </div>


        </div>

        <%--<asp:FormView runat="server"
            ItemType="TrainingCenter.Models.Exam" DefaultMode="Edit" DataKeyNames="ExamID"
            UpdateMethod="UpdateItem" SelectMethod="GetItem"
            OnItemCommand="ItemCommand" RenderOuterTable="false">
            <EmptyDataTemplate>
                Cannot find the Exam with ExamID <%: Request.QueryString["ExamID"] %>
            </EmptyDataTemplate>
            <EditItemTemplate>
                <fieldset class="form-horizontal">
                    <legend>Edit Exam</legend>
					<asp:ValidationSummary runat="server" CssClass="alert alert-danger"  />                 
							<asp:DynamicControl Mode="Edit" 
								DataField="CourseID" 
								DataTypeName="TrainingCenter.Models.Course" 
								DataTextField="TeacherID" 
								DataValueField="CourseID" 
								UIHint="ForeignKey" runat="server" />
						    <asp:DynamicControl Mode="Edit" DataField="ExamDate" runat="server" />
						    <asp:DynamicControl Mode="Edit" DataField="FullMark" runat="server" />
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
							<asp:Button runat="server" ID="UpdateButton" CommandName="Update" Text="Update" CssClass="btn btn-primary" />
							<asp:Button runat="server" ID="CancelButton" CommandName="Cancel" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" />
						</div>
					</div>
                </fieldset>
            </EditItemTemplate>
        </asp:FormView>--%>
    </div>
</asp:Content>

