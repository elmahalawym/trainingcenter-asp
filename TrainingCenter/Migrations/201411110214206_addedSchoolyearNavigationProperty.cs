namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedSchoolyearNavigationProperty : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "FirstName", c => c.String(nullable: false));
            AlterColumn("dbo.AspNetUsers", "LastName", c => c.String(nullable: false));
            AlterColumn("dbo.AspNetUsers", "Phone", c => c.String(nullable: false));
            CreateIndex("dbo.AspNetUsers", "SchoolYearID");
            AddForeignKey("dbo.AspNetUsers", "SchoolYearID", "dbo.SchoolYears", "SchoolYearID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "SchoolYearID", "dbo.SchoolYears");
            DropIndex("dbo.AspNetUsers", new[] { "SchoolYearID" });
            AlterColumn("dbo.AspNetUsers", "Phone", c => c.String());
            AlterColumn("dbo.AspNetUsers", "LastName", c => c.String());
            AlterColumn("dbo.AspNetUsers", "FirstName", c => c.String());
        }
    }
}
