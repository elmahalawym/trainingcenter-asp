﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using Microsoft.AspNet.FriendlyUrls.ModelBinding;
using TrainingCenter.Models;
using Microsoft.AspNet.FriendlyUrls;
using System.Text;

namespace TrainingCenter.Admins.StudentViewModels
{
    public partial class Details : System.Web.UI.Page
    {
        ApplicationDbContext _db = new ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (FriendlyUrl.Segments.Count == 0)
            {
                Response.Redirect("~/Admins/Students/Default");
                return;
            }

            string userId = FriendlyUrl.Segments[0];
            var subscriptions = _db.StudentSubscriptions.Where(s => s.StudentID == userId).Include(s => s.Course);
            StringBuilder sb = new StringBuilder();
            foreach (var item in subscriptions)
            {
                sb.Append(item.Course.CourseName + ", ");
            }
            ((Label)formView.FindControl("coursesLabel")).Text = sb.ToString();
        }

        // This is the Select methd to selects a single StudentViewModel item with the id
        // USAGE: <asp:FormView SelectMethod="GetItem">
        public TrainingCenter.Models.ApplicationUser GetItem([FriendlyUrlSegmentsAttribute(0)]string ApplicationUserID)
        {
            if (ApplicationUserID == null)
            {
                return null;
            }

            using (_db)
            {
                return _db.Users.Where(m => m.Id == ApplicationUserID).Include(u => u.SchoolYear).FirstOrDefault();
            }
        }

        protected void ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Cancel", StringComparison.OrdinalIgnoreCase))
            {
                Response.Redirect("../Default");
            }
        }
    }
}

