﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TrainingCenter.Models
{
    public class StudentViewModel
    {
        [ScaffoldColumn(false)]
        [Key]
        public string ApplicationUserID { get; set; }

        [Required]
        [Display(Name="اسم المستخدم")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "الإسم الأول")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "إسم العائلة")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "رقم التليفون")]
        public string Phone { get; set; }

        [ScaffoldColumn(false)]
        public int? SchoolYearID { get; set; }   // this property is for students only.

        public SchoolYear SchoolYear { get; set; }

        public bool GeneralType { get; set; }

        public string Courses { get; set; }

        public bool IsHalfMonth { get; set; }


    }
}