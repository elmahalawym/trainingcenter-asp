﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Calculate.aspx.cs" Inherits="TrainingCenter.Admins.Invoices.Calculate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <div class="panel">
        <div class="panel-body">
            <div class="form-inline">
                <asp:Label runat="server" AssociatedControlID="yearTxt" Text="السنة" CssClass="control-label" />
                <asp:TextBox runat="server" ID="yearTxt" TextMode="Number" CssClass="form-control" AutoPostBack="true" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="text-danger"
                    ErrorMessage="يجب إدخال السنة" ControlToValidate="yearTxt" />

                <asp:Label runat="server" AssociatedControlID="monthDropDown" Text="الشهر" CssClass="control-label" />
                <asp:DropDownList runat="server" ID="monthDropDown" CssClass="form-control" AutoPostBack="true">
                    <asp:ListItem Text="اختر الشهر" Value="-1" />
                    <asp:ListItem Text="يناير" Value="1" />
                    <asp:ListItem Text="فبراير" Value="2" />
                    <asp:ListItem Text="مارس" Value="3" />
                    <asp:ListItem Text="إبريل" Value="4" />
                    <asp:ListItem Text="مايو" Value="5" />
                    <asp:ListItem Text="يونيو" Value="6" />
                    <asp:ListItem Text="يوليو" Value="7" />
                    <asp:ListItem Text="أغسطس" Value="8" />
                    <asp:ListItem Text="سبتمبر" Value="9" />
                    <asp:ListItem Text="أكتوبر" Value="10" />
                    <asp:ListItem Text="نوفمبر" Value="11" />
                    <asp:ListItem Text="ديسمبر" Value="12" />
                </asp:DropDownList>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="يجب إختيار الشهر"
                    ControlToValidate="monthDropDown" MinimumValue="1" Type="Integer" MaximumValue="12" CssClass="text-danger" />

                <%--                <asp:Label runat="server" Text="النسبة" CssClass="control-label" />
                <asp:TextBox runat="server" ID="factorTxt" Text="1.0" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="factorTxt" ErrorMessage="Required" CssClass="text-danger" />
                <p class="text-danger">
                    <asp:Literal ID="factorError" runat="server" />
                </p>--%>
                <div class="form-group">
                    <div class="col-md-10">
                        <asp:DropDownList ID="curMonthSubType" runat="server" CssClass="form-control">
                            <asp:ListItem Text="شهر كامل" Value="1" />
                            <asp:ListItem Text="نصف شهر" Value="2" />
                        </asp:DropDownList>
                    </div>
                    <asp:Label runat="server" AssociatedControlID="curMonthSubType" CssClass="col-md-2 control-label">نظام المرتب</asp:Label>
                </div>
            </div>

            <br />
            <asp:Button ID="calculateBtn" runat="server" CssClass="btn btn-primary" Text="حساب"
                OnClick="calculateBtn_Click" />
            <br />
            <p class="text-danger">
                <asp:Literal runat="server" ID="ErrorMessage" />
            </p>
        </div>
    </div>
</asp:Content>
