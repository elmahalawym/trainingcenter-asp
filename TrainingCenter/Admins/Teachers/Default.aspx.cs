﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using TrainingCenter.Models;
using System.Web.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TrainingCenter.Admins.TeacherViewModels
{
    public partial class Default : System.Web.UI.Page
    {
        protected ApplicationDbContext _db = new ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            (Page.Master as Admins).SelectedMenuItem = "teachers";
        }

        // Model binding method to get List of TeacherViewModel entries
        // USAGE: <asp:ListView SelectMethod="GetData">
        public IQueryable<TrainingCenter.Models.TeacherViewModel> GetData([Control] string firstNameTxt,
            [Control] string lastNameTxt)
        {
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_db));
            string teachersRoleId = RoleManager.FindByName("teacher").Id;

            var query = _db.Users.Where(u => u.Roles.Any(r => r.RoleId == teachersRoleId));

            // filter            
            if (!string.IsNullOrEmpty(firstNameTxt))
                query = query.Where(u => u.FirstName.ToLower().Contains(firstNameTxt.ToLower()));

            if (!string.IsNullOrEmpty(lastNameTxt))
                query = query.Where(u => u.LastName.ToLower().Contains(lastNameTxt.ToLower()));

            return query.Select(u => new TeacherViewModel()
            {
                FirstName = u.FirstName,
                LastName = u.LastName,
                ApplicationUserID = u.Id,
                Phone = u.Phone,
                UserName = u.UserName,
                Email = u.Email
            });
        }
    }
}

