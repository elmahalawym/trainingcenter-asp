﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using TrainingCenter.Models;
using Microsoft.AspNet.FriendlyUrls;
using Microsoft.AspNet.FriendlyUrls.ModelBinding;
using System.Web.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TrainingCenter.Admins.StudentViewModels
{
    public partial class Default : System.Web.UI.Page
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            (Page.Master as Admins).SelectedMenuItem = "students";
            //if(!IsPostBack)
            //{
            //    // fill school years drop down
            //    var schoolYears = _db.SchoolYears.ToList();
            //    schoolYearDropDown.DataSource = schoolYears;
            //    schoolYearDropDown.DataBind();
            //}

        }

        public IQueryable<SchoolYear> schoolYearDropDown_GetData()
        {
            return _db.SchoolYears;
        }

        // Model binding method to get List of StudentViewModel entries
        // USAGE: <asp:ListView SelectMethod="GetData">
        public IQueryable<TrainingCenter.Models.StudentViewModel> GetData([Control] string firstNameTxt,
            [Control] string lastNameTxt, [Control("schoolYearDropDown")] int? schoolYearDropDown)
        {
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_db));
            string studentsRoleId = RoleManager.FindByName("student").Id;

            var query = _db.Users.Where(u => u.Roles.Any(r => r.RoleId == studentsRoleId)).Include(s => s.SchoolYear);

            // filter            
            if(!string.IsNullOrEmpty(firstNameTxt))
                query = query.Where(u => u.FirstName.ToLower().Contains(firstNameTxt.ToLower()));

            if (!string.IsNullOrEmpty(lastNameTxt))
                query = query.Where(u => u.LastName.ToLower().Contains(lastNameTxt.ToLower()));

            if (schoolYearDropDown != null && schoolYearDropDown != -1)
                query = query.Where(u => u.SchoolYearID == schoolYearDropDown);

            List<string> hMonthIds = Helpers.GetHMonthList(Server.MapPath(string.Format("~/App_Data/{0}", Helpers.GetHMonthFileName(DateTime.Now.Year, DateTime.Now.Month))));
            
            return query.Select(u => new StudentViewModel()
                 {
                     ApplicationUserID = u.Id,
                     UserName = u.UserName,
                     FirstName = u.FirstName,
                     LastName = u.LastName,
                     SchoolYear = u.SchoolYear,
                     Phone = u.Phone,
                     GeneralType = (bool)u.GeneralType,
                     IsHalfMonth = hMonthIds.Contains(u.Id)
                 });
        }

        // The return type can be changed to IEnumerable, however to support
        // paging and sorting, the following parameters must be added:
        //     int maximumRows
        //     int startRowIndex
        //     out int totalRowCount
        //     string sortByExpression
        public IQueryable<TrainingCenter.Models.NumberOfStudents> NumberOfStudents_GetData()
        {
            List<NumberOfStudents> res = Helpers.GetNumberOfStudents(Server.MapPath("~/App_Data/NumberOfStudents.xml"));
            if (res != null)
                return res.AsQueryable();
            return null;
        }

        protected void updateBtn_Click(object sender, EventArgs e)
        {
            string hMonthFilePath = Server.MapPath(string.Format("~/App_Data/{0}", Helpers.GetHMonthFileName(DateTime.Now.Year, DateTime.Now.Month)));
            string absNumbersFilePath = Server.MapPath(string.Format("~/App_Data/{0}", Helpers.AbsoluteNumbersFileName));
            Helpers.UpdateNumberOfStudents(Server.MapPath("~/App_Data/NumberOfStudents.xml"), absNumbersFilePath, hMonthFilePath);
        }
    }
}

