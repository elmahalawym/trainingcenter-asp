﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using TrainingCenter.Models;
using System.IO;
using System.Xml.Serialization;

namespace TrainingCenter.SchoolYears
{
    public partial class Default : System.Web.UI.Page
    {
		protected TrainingCenter.Models.ApplicationDbContext _db = new TrainingCenter.Models.ApplicationDbContext();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            (Page.Master as Admins.Admins).SelectedMenuItem = "prices";

            if (!IsPostBack)
            {
                Prices p = Helpers.GetPrices(Server.MapPath("~/App_Data/Prices.xml"));
                if (p != null)
                {
                    percentageTxt.Text = p.Percentage.ToString();
                    expensesTxt.Text = p.AdministrativeExpenses.ToString();
                }
            }
        }

        // Model binding method to get List of SchoolYear entries
        // USAGE: <asp:ListView SelectMethod="GetData">
        public IQueryable<TrainingCenter.Models.SchoolYear> GetData()
        {
            return _db.SchoolYears;
        }

        protected void editPrices_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admins/Schoolyears/setprices.aspx");
        }

        // The return type can be changed to IEnumerable, however to support
        // paging and sorting, the following parameters must be added:
        //     int maximumRows
        //     int startRowIndex
        //     out int totalRowCount
        //     string sortByExpression
        public IEnumerable<TrainingCenter.Models.SchoolYearStudentValue> Unnamed_GetData()
        {
            Prices p = Helpers.GetPrices(Server.MapPath("~/App_Data/Prices.xml"));
            if(p != null)
                return Helpers.GetPrices(Server.MapPath("~/App_Data/Prices.xml")).SchoolyearValues;
            return null;
        }
    }
}

