﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrainingCenter.Models
{
    public class Prices
    {
        public Prices()
        {
            SchoolyearValues = new List<SchoolYearStudentValue>();
        }

        public double Percentage { get; set; }

        public double AdministrativeExpenses { get; set; }

        public List<SchoolYearStudentValue> SchoolyearValues { get; set; }
    }

    public class SchoolYearStudentValue
    {
        public int SchoolYearId { get; set; }
        public string SchoolYearName { get; set; }
        public double StudentGeneralPrice { get; set; } // كل المواد
        public double StudentSpecialPrice { get; set; } // بعض المود
    }
}