﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrainingCenter.Models;
using System.Web.ModelBinding;
using System.Data.Entity;

namespace TrainingCenter.Admins.Invoices
{
    public partial class Default : System.Web.UI.Page
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            (Page.Master as Admins).SelectedMenuItem = "expenses";

            if(!IsPostBack)
            {
                yearTxt.Text = DateTime.Now.Year.ToString();
                monthDropDown.SelectedIndex = DateTime.Now.Month;

                schoolYearsDropDown.DataSource = _db.SchoolYears.Select(s => new { Text = s.Name, Value = s.SchoolYearID }).ToList();
                schoolYearsDropDown.DataBind();
            }
        }

        public IQueryable<Invoice> GetData([Control("yearTxt")]int? year, [Control("monthDropDown")]int? month, [Control("schoolYearsDropDown")]int? schoolYearId, [Control]int? stateDropDown)
        {
            if (year == null || month == null)
                return null;

            var query = _db.Invoices.Where(i => i.Year == year && i.Month == month).Include(i => i.ApplicationUser);

            if (schoolYearId != null && schoolYearId != -1)
                query = query.Where(i => i.ApplicationUser.SchoolYearID == schoolYearId);

            if(stateDropDown != null && stateDropDown != -1)
            {
                bool isPaid = stateDropDown == 1;
                query = query.Where(i => i.Paid == isPaid);
            }

            return query.OrderBy(i => i.ApplicationUser.FirstName);
        }

    }
}