﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using Microsoft.AspNet.FriendlyUrls.ModelBinding;
using TrainingCenter.Models;

namespace TrainingCenter.Admins.Exams
{
    public partial class Delete : System.Web.UI.Page
    {
		protected TrainingCenter.Models.ApplicationDbContext _db = new TrainingCenter.Models.ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        // This is the Delete methd to delete the selected Exam item
        // USAGE: <asp:FormView DeleteMethod="DeleteItem">
        public void DeleteItem(int ExamID)
        {
            using (_db)
            {
                var item = _db.Exams.Find(ExamID);

                if (item != null)
                {
                    _db.Exams.Remove(item);
                    _db.SaveChanges();
                }
            }
            Response.Redirect("../Default");
        }

        // This is the Select methd to selects a single Exam item with the id
        // USAGE: <asp:FormView SelectMethod="GetItem">
        public TrainingCenter.Models.Exam GetItem([FriendlyUrlSegmentsAttribute(0)]int? ExamID)
        {
            throw new NotImplementedException();
            /*
            if (ExamID == null)
            {
                return null;
            }

            using (_db)
            {
	            return _db.Exams.Where(m => m.ExamID == ExamID).Include(m => m.Course).FirstOrDefault();
            }*/
        }

        protected void ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Cancel", StringComparison.OrdinalIgnoreCase))
            {
                Response.Redirect("../Default");
            }
        }
    }
}

