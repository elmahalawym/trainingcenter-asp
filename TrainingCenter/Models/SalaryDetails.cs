﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrainingCenter.Models
{
    public class SalaryDetails
    {
        public SalaryDetails()
        {
            SalaryUnits = new List<SalaryUnit>();
        }
        public string TeacherId { get; set; }
        public string TeacherFullName { get; set; }
        public string TeacherUsername { get; set; }
        public List<SalaryUnit> SalaryUnits { get; set; }
        public double Total { get; set; }

        public double Factor { get; set; }
    }

    public class SalaryUnit
    {
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public double NStudentsGeneral { get; set; }

        public double NStudentsSpecial { get; set; }
        public double GeneralStudentPrice { get; set; }
        public double SpecialStudentPrice { get; set; }

        public double Total { get; set; }
    }
}