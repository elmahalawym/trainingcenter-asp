﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using TrainingCenter.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TrainingCenter.Admins.Students
{
    public partial class PasswordReset : System.Web.UI.Page
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            // navigate if no userid is provided
            if (FriendlyUrl.Segments.Count == 0)
                Response.Redirect("~/Admins/Students/Default");
        }

        protected void ResetPassword_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                // get student role id
                var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_db));
                string studentsRoleId = RoleManager.FindByName("student").Id;

                string userId = FriendlyUrl.Segments[0];

                var user = _db.Users.Where(u => u.Id == userId && u.Roles.Any(r => r.RoleId == studentsRoleId)).FirstOrDefault();

                if (user == null) // user is not found
                {
                    errorMessage.Text = "Invalid User Id";
                    return;
                }

                // change user password
                var userManager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                userManager.RemovePassword(userId);
                userManager.AddPassword(userId, newPasswordTxt.Text);

                // navigate back to default page
                Response.Redirect("~/Admins/Students/Default");

            }
        }
    }
}