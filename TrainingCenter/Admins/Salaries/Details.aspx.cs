﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using Microsoft.AspNet.FriendlyUrls.ModelBinding;
using TrainingCenter.Models;
using Microsoft.AspNet.FriendlyUrls;

namespace TrainingCenter.Admins.Salaries
{
    public partial class Details : System.Web.UI.Page
    {
		protected TrainingCenter.Models.ApplicationDbContext _db = new TrainingCenter.Models.ApplicationDbContext();
        int year, month;
        string teacherId;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(FriendlyUrl.Segments.Count != 3 || !int.TryParse(FriendlyUrl.Segments[1], out year) ||
                !int.TryParse(FriendlyUrl.Segments[2], out month))
            {
                Response.Redirect("~/Admins/Salaries/Default");
                return;
            }

            teacherId = FriendlyUrl.Segments[0];

            // find teacher
            ApplicationUser teacher = _db.Users.Where(u => u.Id == teacherId).FirstOrDefault();

            if(teacher == null) // teacher not found
            {
                Response.Redirect("~/Admins/Salaries/Default");
                return;
            }

            // get salary report
            List<SalaryDetails> salaries = Helpers.GetSalaries(Server.MapPath(string.Format("~/App_Data/{0}",
                Helpers.GetSalariesFileName(year, month))));

            SalaryDetails sDetails = salaries.Where(s => s.TeacherId == teacherId).FirstOrDefault();

            if(sDetails == null) // details not found
            {
                Response.Redirect("~/Admins/Salaries/Default");
                return;
            }

            teacherNameLabel.Text = sDetails.TeacherFullName;
            totalLabel.Text = string.Format("{0:0.00}", sDetails.Total);
            factorLabel.Text = string.Format("{0:0.0}", sDetails.Factor);

            sUnitsListView.DataSource = sDetails.SalaryUnits;
            sUnitsListView.DataBind();

        }

        // This is the Select methd to selects a single Salary item with the id
        // USAGE: <asp:FormView SelectMethod="GetItem">
        //public TrainingCenter.Models.Salary GetItem([FriendlyUrlSegmentsAttribute(0)]int? SalaryID)
        //{
        //    if (SalaryID == null)
        //    {
        //        return null;
        //    }

        //    using (_db)
        //    {
        //        return _db.Salaries.Where(m => m.SalaryID == SalaryID).Include(m => m.ApplicationUser).FirstOrDefault();
        //    }
        //}

        //protected void ItemCommand(object sender, FormViewCommandEventArgs e)
        //{
        //    if (e.CommandName.Equals("Cancel", StringComparison.OrdinalIgnoreCase))
        //    {
        //        Response.Redirect("../Default");
        //    }
        //}

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admins/Salaries/Default");
        }
    }
}

