namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedGeneralTypeToApplicationUser2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AspNetUsers", "GeneralType", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AspNetUsers", "GeneralType", c => c.Boolean(nullable: false));
        }
    }
}
