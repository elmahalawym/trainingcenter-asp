﻿<%@ Page Title="SchoolYearEdit" Language="C#" MasterPageFile="~/Site.Master" CodeBehind="Edit.aspx.cs" Inherits="TrainingCenter.SchoolYears.Edit" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div class="right-align">
        <p>&nbsp;</p>
        <asp:FormView runat="server"
            ItemType="TrainingCenter.Models.SchoolYear" DefaultMode="Edit" DataKeyNames="SchoolYearID"
            UpdateMethod="UpdateItem" SelectMethod="GetItem"
            OnItemCommand="ItemCommand" RenderOuterTable="false">
            <EmptyDataTemplate>
                Cannot find the SchoolYear with SchoolYearID <%: Request.QueryString["SchoolYearID"] %>
            </EmptyDataTemplate>
            <EditItemTemplate>
                <fieldset class="form-horizontal">
                    <legend>تعديل السنة الدراسية</legend>

                    <asp:ValidationSummary runat="server" CssClass="alert alert-danger" />
                    <asp:DynamicControl Mode="Edit" DataField="Name" runat="server" />
                    <asp:DynamicControl Mode="Edit" DataField="GeneralPrice" runat="server" />
                    <asp:DynamicControl Mode="Edit" DataField="SpecialPrice" runat="server" />

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button runat="server" ID="UpdateButton" CommandName="Update" Text="تحديث" CssClass="btn btn-primary" />
                            <asp:Button runat="server" ID="CancelButton" CommandName="Cancel" Text="إلغاء" CausesValidation="false" CssClass="btn btn-default" />
                        </div>
                    </div>
                </fieldset>
            </EditItemTemplate>
        </asp:FormView>
    </div>
</asp:Content>

