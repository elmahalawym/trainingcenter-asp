﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using TrainingCenter.Models;
using System.Web.ModelBinding;

namespace TrainingCenter.Admins.Courses
{
    public partial class Default : System.Web.UI.Page
    {
		protected TrainingCenter.Models.ApplicationDbContext _db = new TrainingCenter.Models.ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            (Page.Master as Admins).SelectedMenuItem = "courses";

            if(!IsPostBack)
            {
                schoolYearDropdown.DataSource = _db.SchoolYears.ToList();
                schoolYearDropdown.DataBind();
            }
        }

        // Model binding method to get List of Course entries
        // USAGE: <asp:ListView SelectMethod="GetData">
        public IQueryable<TrainingCenter.Models.Course> GetData([Control]int? schoolYearDropdown)
        {
            if (schoolYearDropdown == null || schoolYearDropdown == -1)
                return null;
            var query = _db.Courses.Include(m => m.ApplicationUser).Include(m => m.SchoolYear);
            if (schoolYearDropdown != 0)
                query = query.Where(c => c.SchoolYearID == schoolYearDropdown);
            return query;    
        }
    }
}

