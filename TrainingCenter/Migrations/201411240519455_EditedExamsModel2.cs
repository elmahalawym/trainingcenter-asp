namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditedExamsModel2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StudentGrades", new[] { "Month", "Half" }, "dbo.Exams");
            DropForeignKey("dbo.StudentGrades", new[] { "Month", "Half", "Year" }, "dbo.Exams");
            DropIndex("dbo.StudentGrades", new[] { "Month", "Half" });
            DropPrimaryKey("dbo.Exams");
            DropPrimaryKey("dbo.StudentGrades");
            AddColumn("dbo.Exams", "Year", c => c.Int(nullable: false));
            AddColumn("dbo.StudentGrades", "Year", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Exams", new[] { "Month", "Half", "Year" });
            AddPrimaryKey("dbo.StudentGrades", new[] { "StudentID", "Month", "Half", "Year" });
            CreateIndex("dbo.StudentGrades", new[] { "Month", "Half", "Year" });
            AddForeignKey("dbo.StudentGrades", new[] { "Month", "Half", "Year" }, "dbo.Exams", new[] { "Month", "Half", "Year" }, cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StudentGrades", new[] { "Month", "Half", "Year" }, "dbo.Exams");
            DropIndex("dbo.StudentGrades", new[] { "Month", "Half", "Year" });
            DropPrimaryKey("dbo.StudentGrades");
            DropPrimaryKey("dbo.Exams");
            DropColumn("dbo.StudentGrades", "Year");
            DropColumn("dbo.Exams", "Year");
            AddPrimaryKey("dbo.StudentGrades", new[] { "StudentID", "Month", "Half" });
            AddPrimaryKey("dbo.Exams", new[] { "Month", "Half" });
            CreateIndex("dbo.StudentGrades", new[] { "Month", "Half" });
            AddForeignKey("dbo.StudentGrades", new[] { "Month", "Half", "Year" }, "dbo.Exams", new[] { "Month", "Half", "Year" }, cascadeDelete: true);
            AddForeignKey("dbo.StudentGrades", new[] { "Month", "Half" }, "dbo.Exams", new[] { "Month", "Half" }, cascadeDelete: true);
        }
    }
}
