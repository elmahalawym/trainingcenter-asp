﻿<%@ Page Title="TeacherDiscountList" Language="C#" MasterPageFile="~/Admins/Admins.master" CodeBehind="Default.aspx.cs" Inherits="TrainingCenter.Admins.Discounts.Default" %>

<%@ Register TagPrefix="FriendlyUrls" Namespace="Microsoft.AspNet.FriendlyUrls" %>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">

    <h2>الخصومات</h2>
    <p>
        <asp:HyperLink runat="server" NavigateUrl="Insert" Text="إضافة" />
    </p>

    <div class="form-inline">

        <!-- Filter by year -->
        <div class="form-group">
            <asp:Label runat="server" Text="السنة" AssociatedControlID="yearTxt" />
            <asp:TextBox ID="yearTxt" runat="server" AutoPostBack="true" TextMode="Number" CssClass="form-control" />
        </div>

        <!-- Filter by month -->
        <div class="form-group">
            <asp:Label runat="server" Text="الشهر" AssociatedControlID="monthDropDown"></asp:Label>
            <asp:DropDownList runat="server" ID="monthDropDown" CssClass="form-control" AutoPostBack="true">
                <asp:ListItem Text="كل الشهور" Value="-1" />
                <asp:ListItem Text="يناير" Value="1" />
                <asp:ListItem Text="فبراير" Value="2" />
                <asp:ListItem Text="مارس" Value="3" />
                <asp:ListItem Text="إبريل" Value="4" />
                <asp:ListItem Text="مايو" Value="5" />
                <asp:ListItem Text="يونيو" Value="6" />
                <asp:ListItem Text="يوليو" Value="7" />
                <asp:ListItem Text="أغسطس" Value="8" />
                <asp:ListItem Text="سبتمبر" Value="9" />
                <asp:ListItem Text="أكتوبر" Value="10" />
                <asp:ListItem Text="نوفمبر" Value="11" />
                <asp:ListItem Text="ديسمبر" Value="12" />
            </asp:DropDownList>
        </div>


        <!-- Filter by teacher -->
        <div class="form-group">
            <asp:Label runat="server" Text="الشهر" AssociatedControlID="teacherDropDown"></asp:Label>
            <asp:DropDownList runat="server" ID="teacherDropDown" CssClass="form-control" AutoPostBack="true"
                AppendDataBoundItems="true" DataValueField="Id" DataTextField="UserName">
                <asp:ListItem Text="اختر المدرس" Value="-1" />
            </asp:DropDownList>
        </div>
    </div>
    <div>
        <asp:ListView ID="ListView1" runat="server"
            DataKeyNames="ID"
            ItemType="TrainingCenter.Models.TeacherDiscount"
            SelectMethod="GetData">
            <EmptyDataTemplate>
                لا توجد خصومات مسجلة
            </EmptyDataTemplate>
            <LayoutTemplate>
                <table class="table table-bordered table-hover table-striped margin-top-20">
                    <thead>
                        <tr>
                            <th>
                                <asp:LinkButton Text="المدرس" CommandName="Sort" CommandArgument="TeacherID" runat="Server" />
                            </th>
                            <th>
                                <asp:LinkButton Text="تاريخ الخصم" CommandName="Sort" CommandArgument="DiscountDate" runat="Server" />
                            </th>
                            <th>
                                <asp:LinkButton Text="نوع الخصم" CommandName="Sort" CommandArgument="DiscountType" runat="Server" />
                            </th>
                            <th>
                                <asp:LinkButton Text="القيمة" CommandName="Sort" CommandArgument="Amount" runat="Server" />
                            </th>
                            <th>
                                <asp:Label runat="server" Text="ملاحظات" />
                            </th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr runat="server" id="itemPlaceholder" />
                    </tbody>
                </table>
                <asp:DataPager PageSize="5" runat="server">
                    <Fields>
                        <asp:NextPreviousPagerField ShowLastPageButton="False" ShowNextPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                        <asp:NumericPagerField ButtonType="Button" NumericButtonCssClass="btn" CurrentPageLabelCssClass="btn disabled" NextPreviousButtonCssClass="btn" />
                        <asp:NextPreviousPagerField ShowFirstPageButton="False" ShowPreviousPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                    </Fields>
                </asp:DataPager>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <%#: Item.ApplicationUser != null ? Item.ApplicationUser.FirstName + " " + Item.ApplicationUser.LastName : "" %>
                    </td>
                    <td>
                        <%# Item.DiscountDate.ToLongDateString() %>
                    </td>
                    <td>
                        <%# Item.GetDiscountTypeName() %>
                    </td>
                    <td>
                        <asp:DynamicControl runat="server" DataField="Amount" ID="Amount" Mode="ReadOnly" />
                    </td>
                    <td>
                        <%# Item.Notes %>
                    </td>
                    <td>
					    <asp:HyperLink runat="server" NavigateUrl='<%# FriendlyUrl.Href("~/Admins/Discounts/Edit", Item.ID) %>' Text="تعديل" />
                        | 
                        <asp:HyperLink runat="server" NavigateUrl='<%# FriendlyUrl.Href("~/Admins/Discounts/Delete", Item.ID) %>' Text="حذف" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
</asp:Content>

