﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StudentGrades.aspx.cs" Inherits="TrainingCenter.Admins.Exams.StudentGrades" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2>درجات الإمتحان</h2>

    <div class="form-horizontal">

        <br />
        <p class="text-danger">
            <asp:Literal runat="server" ID="errorMessage" />
        </p>
        <!-- Exam Details -->
        <div class="panel panel-default">
            <div class="panel-heading">بيانات الإمتحان</div>
            <div class="panel-body">
                <!-- Year -->
                <div class="form-group">
                    <div class="col-md-10">
                        <asp:Label runat="server" ID="yearLabel" CssClass="control-label" />
                    </div>
                    <asp:Label runat="server" AssociatedControlID="yearLabel" CssClass="col-md-2 control-label">السنة</asp:Label>
                </div>

                <!-- Month -->
                <div class="form-group">
                    <div class="col-md-10">
                        <asp:Label runat="server" ID="monthLabel" CssClass="control-label" />
                    </div>
                    <asp:Label runat="server" AssociatedControlID="monthLabel" CssClass="col-md-2 control-label">الشهر</asp:Label>
                </div>

                <!-- Half -->
                <div class="form-group">
                    <div class="col-md-10">
                        <asp:Label runat="server" ID="halfLabel" CssClass="control-label" />
                    </div>
                    <asp:Label runat="server" AssociatedControlID="monthLabel" CssClass="col-md-2 control-label">النصف</asp:Label>
                </div>

                <!-- Course -->
                <div class="form-group">
                    <div class="col-md-10">
                        <asp:Label runat="server" ID="courseLabel" CssClass="control-label" />
                    </div>
                    <asp:Label runat="server" AssociatedControlID="courseLabel" CssClass="col-md-2 control-label">الكورس</asp:Label>
                </div>

                <!-- Full Mark -->
                <div class="form-group">
                    <div class="col-md-10">
                        <asp:Label runat="server" ID="fullMarkLabel" CssClass="control-label" />
                    </div>
                    <asp:Label runat="server" AssociatedControlID="fullMarkLabel" CssClass="col-md-2 control-label">الدرجة النهائية</asp:Label>
                </div>


                <asp:Button ID="updateStudentsListBtn" runat="server" CssClass="btn btn-lg btn-primary" OnClick="updateStudentsListBtn_Click" Text="تحديث قائمة الطلاب" />
                <br />
            </div>
        </div>


        <div class="panel panel-default">
            <div class="panel-heading">الدرجات</div>
            <div class="panel-body">
                <div class="form-horizontal">


                    <asp:GridView ID="GradesList" runat="server" ItemType="TrainingCenter.Models.StudentGrade"
                        SelectMethod="gradesRepeater_GetData" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover">
                        <Columns>
                            <asp:TemplateField HeaderText="الطالب">
                                <ItemTemplate>
                                    <asp:Label runat="server"><%# Item.ApplicationUser.FirstName + " " + Item.ApplicationUser.LastName %></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="الدرجة">
                                <ItemTemplate>
                                    <asp:TextBox runat="server" Text="<%#: Item.Grade %>" CssClass="form-control"
                                        TextMode="Number" ID="gradeTxt" />
                                    <asp:HiddenField ID="studentId" runat="server" Value="<%#: Item.StudentID %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
                <%--<asp:ListView runat="server" ID="GradesListView"
                    ItemType="TrainingCenter.Models.StudentGrade" SelectMethod="GradesListView_GetData"
                    >
                    <LayoutTemplate>
                        <div class="form-horizontal">
                            <div runat="server" id="itemPlaceholder"></div>
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div class="form-group right-align">
                            <div class="col-md-10">
                                <asp:Label runat="server" ID="Grade" Text='<%# Eval("Grade") %>'
                                    CssClass="control-label" />
                            </div>
                            <asp:Label runat="server" AssociatedControlID="Grade"
                                CssClass="col-md-2 control-label"><%# Item.ApplicationUser.FirstName + " " + Item.ApplicationUser.LastName %></asp:Label>
                        </div>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <div class="form-group right-align">
                            <div class="col-md-10">
                                <asp:Label runat="server" ID="Grade" Text='<%# Eval("Grade") %>'
                                    CssClass="form-control" TextMode="Number" />
                            </div>
                            <asp:Label runat="server" AssociatedControlID="Grade"
                                CssClass="col-md-2 control-label"><%# Item.ApplicationUser.FirstName + " " + Item.ApplicationUser.LastName %></asp:Label>
                        </div>
                    </EditItemTemplate>
                </asp:ListView>--%>

                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                        <asp:Button runat="server" OnClick="Save_Click" Text="حفظ" CssClass="btn btn-primary" />
                        <a href="<%= Microsoft.AspNet.FriendlyUrls.FriendlyUrl.Href("~/Admins/Exams/Default") %>" class="btn btn-default">إلغاء</a>
                    </div>
                </div>

            </div>
        </div>

    </div>

</asp:Content>
