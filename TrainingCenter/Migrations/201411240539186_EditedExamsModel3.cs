namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditedExamsModel3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StudentGrades", new[] { "Month", "Half", "Year" }, "dbo.Exams");
            DropForeignKey("dbo.StudentGrades", new[] { "Month", "Half", "Year", "CourseID" }, "dbo.Exams");
            DropIndex("dbo.StudentGrades", new[] { "Month", "Half", "Year" });
            DropPrimaryKey("dbo.Exams");
            DropPrimaryKey("dbo.StudentGrades");
            AddColumn("dbo.StudentGrades", "CourseID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Exams", new[] { "Month", "Half", "Year", "CourseID" });
            AddPrimaryKey("dbo.StudentGrades", new[] { "StudentID", "Month", "Half", "Year", "CourseID" });
            CreateIndex("dbo.StudentGrades", new[] { "Month", "Half", "Year", "CourseID" });
            AddForeignKey("dbo.StudentGrades", new[] { "Month", "Half", "Year", "CourseID" }, "dbo.Exams", new[] { "Month", "Half", "Year", "CourseID" }, cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StudentGrades", new[] { "Month", "Half", "Year", "CourseID" }, "dbo.Exams");
            DropIndex("dbo.StudentGrades", new[] { "Month", "Half", "Year", "CourseID" });
            DropPrimaryKey("dbo.StudentGrades");
            DropPrimaryKey("dbo.Exams");
            DropColumn("dbo.StudentGrades", "CourseID");
            AddPrimaryKey("dbo.StudentGrades", new[] { "StudentID", "Month", "Half", "Year" });
            AddPrimaryKey("dbo.Exams", new[] { "Month", "Half", "Year" });
            CreateIndex("dbo.StudentGrades", new[] { "Month", "Half", "Year" });
            AddForeignKey("dbo.StudentGrades", new[] { "Month", "Half", "Year", "CourseID" }, "dbo.Exams", new[] { "Month", "Half", "Year", "CourseID" }, cascadeDelete: true);
            AddForeignKey("dbo.StudentGrades", new[] { "Month", "Half", "Year" }, "dbo.Exams", new[] { "Month", "Half", "Year" }, cascadeDelete: true);
        }
    }
}
