﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PasswordReset.aspx.cs" Inherits="TrainingCenter.Admins.Teachers.PasswordReset" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div>
        <p>&nbsp;</p>

        <div class="form-horizontal">

            <h4>تعيين كلمة سر جديدة</h4>
            <hr />
            <p class="text-danger">
                <asp:Literal runat="server" ID="errorMessage"></asp:Literal>
            </p>

            <div class="form-group">
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="newPasswordTxt" CssClass="form-control" />
                    <span id="generateBtn" class="btn btn-default" data-target="<%= newPasswordTxt.ClientID %>">generate</span>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="newPasswordTxt"
                        CssClass="text-danger" ErrorMessage="يجب إدخال كلمة السر" />
                </div>
                <asp:Label runat="server" AssociatedControlID="newPasswordTxt" CssClass="col-md-2 control-label">كلمة السر الجديدة</asp:Label>
            </div>


            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <asp:Button runat="server" OnClick="ResetPassword_Click" Text="تغيير" CssClass="btn btn-primary" />
                    <a href="<%= Microsoft.AspNet.FriendlyUrls.FriendlyUrl.Href("~/Admins/Teachers/Default") %>" class="btn btn-default">إلغاء</a>
                </div>
            </div>
        </div>

    </div>
</asp:Content>
