﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TrainingCenter.Startup))]
namespace TrainingCenter
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
