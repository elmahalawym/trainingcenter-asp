namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Discounts : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TeacherAbsences", "TeacherID", "dbo.AspNetUsers");
            DropIndex("dbo.TeacherAbsences", new[] { "TeacherID" });
            CreateTable(
                "dbo.TeacherDiscounts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TeacherID = c.String(maxLength: 128),
                        DiscountDate = c.DateTime(nullable: false),
                        DiscountType = c.Int(nullable: false),
                        Amount = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.TeacherID)
                .Index(t => t.TeacherID);
            
            DropTable("dbo.TeacherAbsences");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TeacherAbsences",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TeacherID = c.String(maxLength: 128),
                        AbsenceDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            DropForeignKey("dbo.TeacherDiscounts", "TeacherID", "dbo.AspNetUsers");
            DropIndex("dbo.TeacherDiscounts", new[] { "TeacherID" });
            DropTable("dbo.TeacherDiscounts");
            CreateIndex("dbo.TeacherAbsences", "TeacherID");
            AddForeignKey("dbo.TeacherAbsences", "TeacherID", "dbo.AspNetUsers", "Id");
        }
    }
}
