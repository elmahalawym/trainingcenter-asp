﻿<%@ Page Title="StudentViewModel Details" Language="C#" MasterPageFile="~/Site.Master" CodeBehind="Details.aspx.cs" Inherits="TrainingCenter.Admins.StudentViewModels.Details" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div>
        <p>&nbsp;</p>

        <asp:FormView runat="server" ID="formView"
            ItemType="TrainingCenter.Models.ApplicationUser" DataKeyNames="Id"
            SelectMethod="GetItem"
            OnItemCommand="ItemCommand" RenderOuterTable="false">
            <EmptyDataTemplate>
                Cannot find the StudentViewModel with ApplicationUserID <%: Request.QueryString["ApplicationUserID"] %>
            </EmptyDataTemplate>
            <ItemTemplate>
                <fieldset class="form-horizontal">
                    <legend>بيانات الطالب</legend>
                    <div class="row">

                        <div class="col-sm-10">
                            <asp:DynamicControl runat="server" DataField="UserName" ID="UserName" Mode="ReadOnly" />
                        </div>
                        <div class="col-sm-2 text-right">
                            <strong>اسم المستخدم</strong>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <asp:DynamicControl runat="server" DataField="FirstName" ID="FirstName" Mode="ReadOnly" />
                        </div>
                        <div class="col-sm-2 text-right">
                            <strong>الإسم الأول</strong>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-sm-10">
                            <asp:DynamicControl runat="server" DataField="LastName" ID="LastName" Mode="ReadOnly" />
                        </div>

                        <div class="col-sm-2 text-right">
                            <strong>إسم العائلة</strong>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <asp:DynamicControl runat="server" DataField="Phone" ID="Phone" Mode="ReadOnly" />
                        </div>

                        <div class="col-sm-2 text-right">
                            <strong>رقم التليفون</strong>
                        </div>
                    </div>

                    <!-- Email -->
                    <div class="row">
                        <div class="col-sm-10">
                            <asp:DynamicControl runat="server" DataField="Email" ID="Email" Mode="ReadOnly" />
                        </div>

                        <div class="col-sm-2 text-right">
                            <strong>الإيميل</strong>
                        </div>
                    </div>

                    <!-- SchoolYear -->
                    <div class="row">
                        <div class="col-sm-10">
                            <asp:DynamicControl runat="server" DataField="SchoolYear" ID="SchoolYear" Mode="ReadOnly" />
                        </div>

                        <div class="col-sm-2 text-right">
                            <strong>السنة الدراسية</strong>
                        </div>
                    </div>

                    <!-- Courses -->
                    <div class="row">
                        <div class="col-sm-10">
                            <asp:Label runat="server" ID="coursesLabel" />
                        </div>

                        <div class="col-sm-2 text-right">
                            <strong>الكورسات</strong>
                        </div>
                    </div>


                    <!-- Mobile 1 -->
                    <div class="row">
                        <div class="col-sm-10">
                            <asp:DynamicControl runat="server" DataField="Mobile1" ID="Mobile1" Mode="ReadOnly" />
                        </div>

                        <div class="col-sm-2 text-right">
                            <strong>موبايل 1</strong>
                        </div>
                    </div>

                    <!-- Mobile 2 -->
                    <div class="row">
                        <div class="col-sm-10">
                            <asp:DynamicControl runat="server" DataField="Mobile2" ID="Mobile2" Mode="ReadOnly" />
                        </div>

                        <div class="col-sm-2 text-right">
                            <strong>موبايل 2</strong>
                        </div>
                    </div>

                    <!-- Subscription Type -->
                    <div class="row">
                        <div class="col-sm-10">
                            <asp:DynamicControl runat="server" DataField="GeneralType" ID="GeneralType" Mode="ReadOnly" />
                        </div>

                        <div class="col-sm-2 text-right">
                            <strong>اشتراك عام؟ </strong>
                        </div>
                    </div>

                    <!-- School Name -->
                    <div class="row">
                        <div class="col-sm-10">
                            <asp:DynamicControl runat="server" DataField="SchoolName" ID="SchoolName" Mode="ReadOnly" />
                        </div>

                        <div class="col-sm-2 text-right">
                            <strong>اسم المدرسة </strong>
                        </div>
                    </div>

                    <!-- Birth Date -->
                    <div class="row">
                        <div class="col-sm-10">
                            <asp:DynamicControl runat="server" DataField="BirthDate" ID="BirthDate" Mode="ReadOnly" />
                        </div>

                        <div class="col-sm-2 text-right">
                            <strong>تاريخ الميلاد</strong>
                        </div>
                    </div>

                    <!-- IsStudentActive -->
                    <div class="row">
                        <div class="col-sm-10">
                            <asp:DynamicControl runat="server" DataField="IsStudentActive" ID="IsStudentActive" Mode="ReadOnly" />
                        </div>

                        <div class="col-sm-2 text-right">
                            <strong>Active?</strong>
                        </div>
                    </div>

                    <!-- Father Phone -->
                    <div class="row">
                        <div class="col-sm-10">
                            <asp:DynamicControl runat="server" DataField="FatherPhoneNumber" ID="FatherPhoneNumber" Mode="ReadOnly" />
                        </div>

                        <div class="col-sm-2 text-right">
                            <strong>رقم تليفون الوالد</strong>
                        </div>
                    </div>

                    <!-- Mother Phone -->
                    <div class="row">
                        <div class="col-sm-10">
                            <asp:DynamicControl runat="server" DataField="MotherPhoneNumber" ID="MotherPhoneNumber" Mode="ReadOnly" />
                        </div>

                        <div class="col-sm-2 text-right">
                            <strong>رقم تليفون الوالدة</strong>
                        </div>
                    </div>

                    <!-- Notes -->
                    <div class="row">
                        <div class="col-sm-10">
                            <asp:DynamicControl runat="server" DataField="Notes" ID="Notes" Mode="ReadOnly" />
                        </div>

                        <div class="col-sm-2 text-right">
                            <strong>رقم التليفون</strong>
                        </div>
                    </div>
                    <div class="row">
                        &nbsp;
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Back" CssClass="btn btn-default" />
                        </div>
                    </div>
                </fieldset>
            </ItemTemplate>
        </asp:FormView>

    </div>
</asp:Content>

