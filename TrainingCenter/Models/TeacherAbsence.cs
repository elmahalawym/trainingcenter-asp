﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TrainingCenter.Models
{
    public class TeacherAbsence
    {
        [Key]
        public int ID { get; set; }

        [ForeignKey("ApplicationUser")]
        public string TeacherID { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public DateTime AbsenceDate { get; set; }
    }
}