﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.FriendlyUrls;
using TrainingCenter.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

namespace TrainingCenter.Admins.Students
{
    public partial class Exams : System.Web.UI.Page
    {
        private ApplicationDbContext _db = new ApplicationDbContext();
        private string _studentId;

        protected void Page_Load(object sender, EventArgs e)
        {
            // redirect to default page if user is was not given
            if(FriendlyUrl.Segments.Count != 1)
            {
                Response.Redirect("~/Admins/Students/Default");
                return;
            }
            _studentId = FriendlyUrl.Segments[0];
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_db));
            var userManager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            string studentRoleId = RoleManager.FindByName("student").Id;

            var student = userManager.Users.Where(u => u.Id == _studentId &&
                u.Roles.Any(r => r.RoleId == studentRoleId)).FirstOrDefault();
            if(student == null)
            {
                Response.Redirect("~/Admins/Students/Default");
                return;
            }
            
            // bind student grades
            List<StudentGrade> studentGrades = _db.StudentGrades.Where(g => g.StudentID == _studentId).Include(
                    g => g.Exam).Include(g => g.Exam.Course).ToList();
            
            
        }
    }
}