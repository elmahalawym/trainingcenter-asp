﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using Microsoft.AspNet.FriendlyUrls.ModelBinding;
using TrainingCenter.Models;

namespace TrainingCenter.SchoolYears
{
    public partial class Delete : System.Web.UI.Page
    {
		protected TrainingCenter.Models.ApplicationDbContext _db = new TrainingCenter.Models.ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        // This is the Delete methd to delete the selected SchoolYear item
        // USAGE: <asp:FormView DeleteMethod="DeleteItem">
        public void DeleteItem(int SchoolYearID)
        {
            using (_db)
            {
                var item = _db.SchoolYears.Find(SchoolYearID);

                if (item != null)
                {
                    _db.SchoolYears.Remove(item);
                    _db.SaveChanges();
                }
            }

            // update prices
            Helpers.UpdatePrices(Server.MapPath("~/App_Data/Prices.xml"));

            Response.Redirect("../Default");
        }

        // This is the Select methd to selects a single SchoolYear item with the id
        // USAGE: <asp:FormView SelectMethod="GetItem">
        public TrainingCenter.Models.SchoolYear GetItem([FriendlyUrlSegmentsAttribute(0)]int? SchoolYearID)
        {
            if (SchoolYearID == null)
            {
                return null;
            }

            using (_db)
            {
	            return _db.SchoolYears.Where(m => m.SchoolYearID == SchoolYearID).FirstOrDefault();
            }
        }

        protected void ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Cancel", StringComparison.OrdinalIgnoreCase))
            {
                Response.Redirect("../Default");
            }
        }
    }
}

