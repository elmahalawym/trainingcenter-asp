namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Invoices : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Invoices", "StudentID", "dbo.AspNetUsers");
            DropIndex("dbo.Invoices", new[] { "StudentID" });
            DropPrimaryKey("dbo.Invoices");
            AddColumn("dbo.Invoices", "Year", c => c.Int(nullable: false));
            AddColumn("dbo.Invoices", "Month", c => c.Int(nullable: false));
            AlterColumn("dbo.Invoices", "StudentID", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Invoices", new[] { "StudentID", "Year", "Month" });
            CreateIndex("dbo.Invoices", "StudentID");
            AddForeignKey("dbo.Invoices", "StudentID", "dbo.AspNetUsers", "Id", cascadeDelete: true);
            DropColumn("dbo.Invoices", "InvoiceID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Invoices", "InvoiceID", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.Invoices", "StudentID", "dbo.AspNetUsers");
            DropIndex("dbo.Invoices", new[] { "StudentID" });
            DropPrimaryKey("dbo.Invoices");
            AlterColumn("dbo.Invoices", "StudentID", c => c.String(maxLength: 128));
            DropColumn("dbo.Invoices", "Month");
            DropColumn("dbo.Invoices", "Year");
            AddPrimaryKey("dbo.Invoices", "InvoiceID");
            CreateIndex("dbo.Invoices", "StudentID");
            AddForeignKey("dbo.Invoices", "StudentID", "dbo.AspNetUsers", "Id");
        }
    }
}
