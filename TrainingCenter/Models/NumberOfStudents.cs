﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrainingCenter.Models
{
    public class NumberOfStudents
    {
        public int SchoolYearId { get; set; }
        public string SchoolYearName { get; set; }
        public string CourseName { get; set; }
        public double NumberOfStudents_General { get; set; }
        public double NumberOfStudents_Special { get; set; }

    }

}