﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admins/Admins.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TrainingCenter.Admins.Default" %>
<%@ MasterType virtualpath="~/Admins/Admins.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <div class="right-align">
        <h1></h1>
        <p><a href="/Admins/Invoices/default">المصروفات</a> </p>
        <p><a href="/Admins/salaries/default">المرتبات</a> </p>
        <p><a href="/Admins/Discounts/default">الخصومات</a> </p>
        <p><a href="/Admins/Schoolyears/default">الأسعار</a> </p>
        <p><a href="/Admins/Students/default">الطلاب</a> </p>
        <p><a href="/Admins/Teachers/Default">المدرسين</a> </p>
        <p><a href="/Admins/Exams/Default">الإمتحانات</a> </p>
        <p><a href="/Admins/Courses/Default">الكورسات</a> </p>
    </div>

</asp:Content>
