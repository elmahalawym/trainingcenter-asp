﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ExamCollection.aspx.cs" Inherits="TrainingCenter.Admins.Exams.ExamCollection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2>تحديد قائمة الإمتحانات</h2>
    <br />

    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="view1" runat="server">

            <div class="form-inline">
                <asp:Label runat="server" Text="السنة" AssociatedControlID="yearTxt"></asp:Label>

                <asp:TextBox runat="server" ID="yearTxt" CssClass="form-control" TextMode="Number"></asp:TextBox>

                <asp:Label runat="server" Text="الشهر" AssociatedControlID="monthDropDown"></asp:Label>
                <asp:DropDownList runat="server" ID="monthDropDown" CssClass="form-control">
                    <asp:ListItem Text="اختر الشهر" Value="-1" />
                    <asp:ListItem Text="يناير" Value="1" />
                    <asp:ListItem Text="فبراير" Value="2" />
                    <asp:ListItem Text="مارس" Value="3" />
                    <asp:ListItem Text="إبريل" Value="4" />
                    <asp:ListItem Text="مايو" Value="5" />
                    <asp:ListItem Text="يونيو" Value="6" />
                    <asp:ListItem Text="يوليو" Value="7" />
                    <asp:ListItem Text="أغسطس" Value="8" />
                    <asp:ListItem Text="سبتمبر" Value="9" />
                    <asp:ListItem Text="أكتوبر" Value="10" />
                    <asp:ListItem Text="نوفمبر" Value="11" />
                    <asp:ListItem Text="ديسمبر" Value="12" />
                </asp:DropDownList>

                <asp:Label runat="server" Text="النصف" AssociatedControlID="halfDropDown"></asp:Label>
                <asp:DropDownList runat="server" CssClass="form-control" ID="halfDropDown">
                    <asp:ListItem Text="اختر النصف" Value="-1" />
                    <asp:ListItem Text="أول" Value="1" />
                    <asp:ListItem Text="ثاني" Value="2" />
                </asp:DropDownList>

            </div>
            <br />

            <p class="text-danger">
                <asp:Literal ID="errorMessage" runat="server" />
            </p>

            <asp:Button ID="nextBtn" runat="server" Text="التالي" OnClick="nextBtn_Click"
                CssClass="btn btn-primary" />

        </asp:View>
        <asp:View ID="view2" runat="server">

            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="form-horizontal">

                        <div class="form-group">
                            <div class="col-md-10">
                                <asp:Label runat="server" CssClass="form-control-static" ID="yearLabel" />
                            </div>
                            <asp:Label runat="server" CssClass="col-md-2 control-label">السنة</asp:Label>
                        </div>


                        <div class="form-group">
                            <div class="col-md-10">
                                <asp:Label runat="server" CssClass="form-control-static" ID="monthLabel" />
                            </div>
                            <asp:Label runat="server" CssClass="col-md-2 control-label">الشهر</asp:Label>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10">
                                <asp:Label runat="server" CssClass="form-control-static" ID="halfLabel" />
                            </div>
                            <asp:Label runat="server" CssClass="col-md-2 control-label">النصف</asp:Label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-11">
                        <input type="text" class="form-control fullMarkSetTxt" />
                    </div>
                    <asp:Label runat="server" CssClass="col-md-1 control-label">الدرجة النهائية</asp:Label>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body">
                    <asp:GridView ID="coursesGridView" runat="server" ItemType="TrainingCenter.Models.ExamViewModel"
                        SelectMethod="coursesListView_GetData" ItemPlaceholderID="itemPlaceholder"
                        DataKeyNames="CourseID" AutoGenerateColumns="false"
                        CssClass="table table-bordered table-hover">
                        <%--                        <LayoutTemplate>
                            <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>اسم المادة</th>
                                        <th>الدرجة النهائية</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <div id="itemPlaceholder" runat="server"></div>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="itemCB" runat="server" CssClass="form-control courseCheckBox" />
                                </td>
                                <td>
                                    <asp:Label runat="server" Text='<%# Item.CourseName + " " + Item.SchoolYear.Name %>' CssClass="form-control" />
                                </td>
                                <td>
                                    <asp:TextBox runat="server" CssClass="form-control examFullMark" ID="fullMarkText" />
                                </td>
                            </tr>
                        </ItemTemplate>--%>
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="itemCB" runat="server" CssClass="form-control-static courseCheckBox" 
                                        Checked='<%# Eval("IsActive") %>' />
                                    
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="اسم المادة">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Item.CourseName %>' 
                                        CssClass="form-control" />

                                    <asp:HiddenField ID="cid" runat="server" Value="<%# Item.CourseId %>"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="الدرجة النهائية">
                                <ItemTemplate>
                                    <asp:TextBox runat="server" CssClass="form-control examFullMark" ID="fullMarkText" TextMode="Number"
                                        Text="<%# Item.FullMark %>"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <asp:Button ID="saveBtn" runat="server" CssClass="btn btn-primary" OnClick="saveBtn_Click"
                        Text="حفظ"/>

                    <p class="text-danger">
                        <asp:Literal ID="saveErrorMessage" runat="server" />
                    </p>

                </div>
            </div>

        </asp:View>
    </asp:MultiView>


</asp:Content>
