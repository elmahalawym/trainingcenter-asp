﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using TrainingCenter.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web.ModelBinding;

namespace TrainingCenter.Admins.Discounts
{
    public partial class Default : System.Web.UI.Page
    {
		protected TrainingCenter.Models.ApplicationDbContext _db = new TrainingCenter.Models.ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            (Page.Master as Admins).SelectedMenuItem = "discounts";

            if (!IsPostBack)
            {
                // fill teachers drop down
                var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_db));
                string teachersRoleId = RoleManager.FindByName("teacher").Id;

                var teachers = _db.Users.Where(u => u.Roles.Any(r => r.RoleId == teachersRoleId)).ToList();
                teacherDropDown.DataSource = teachers;
                teacherDropDown.DataBind();
            }
        }

        // Model binding method to get List of TeacherDiscount entries
        // USAGE: <asp:ListView SelectMethod="GetData">
        public IQueryable<TrainingCenter.Models.TeacherDiscount> GetData([Control]int? yearTxt, [Control]int? monthDropDown
            , [Control] string teacherDropDown)
        {
            var query = _db.Discounts.Include(m => m.ApplicationUser);

            if (yearTxt != null)
                query = query.Where(d => d.DiscountDate.Year == yearTxt);

            if (monthDropDown != null && monthDropDown != -1)
                query = query.Where(d => d.DiscountDate.Month == monthDropDown);

            if (teacherDropDown != null && teacherDropDown != "-1")
                query = query.Where(d => d.TeacherID == teacherDropDown);

            return query;
        }
    }
}

