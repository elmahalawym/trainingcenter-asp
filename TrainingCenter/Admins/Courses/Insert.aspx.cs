﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using TrainingCenter.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TrainingCenter.Admins.Courses
{
    public partial class Insert : System.Web.UI.Page
    {
        protected ApplicationDbContext _db = new ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // fill schoolYearCheckBoxList with data
                var schoolYears = _db.SchoolYears.ToList();
                schoolYearList.DataSource = schoolYears;
                schoolYearList.DataBind();

                // get teacher role id
                var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_db));
                string teacherRoleId = RoleManager.FindByName("teacher").Id;

                // fill teachersDropDown with data
                var teachers = _db.Users.Where(u => u.Roles.Any(r => r.RoleId == teacherRoleId)).ToList();
                teachersDropDown.DataSource = teachers.Select(t => new { Name = t.FirstName + " " + t.LastName, ID = t.Id });
                teachersDropDown.DataBind();
            }
        }


        protected void CreateCourse_Click(object sender, EventArgs e)
        {
            if(Page.IsValid)
            {
                try
                {
                    var selectedYears = schoolYearList.Items.Cast<ListItem>().Where
                        (li => li.Selected).ToList();
                    foreach(ListItem schoolyear in selectedYears)
                    {
                        Course course = new Course();
                        course.CourseName = courseNameTxt.Text;
                        course.TeacherID = teachersDropDown.SelectedValue;
                        course.SchoolYearID = int.Parse(schoolyear.Value);
                        course.NumberOfSessionsPerWeek = int.Parse(nSessionsTxt.Text);

                        _db.Courses.Add(course);
                        
                    }              
                    _db.SaveChanges();
                    _db.Dispose();
                    // update prices
                    Helpers.UpdatePrices(Server.MapPath("~/App_Data/Prices.xml"));
                    Response.Redirect("~/Admins/Courses/Default");
                }
                catch(Exception)
                {
                    errorMessage.Text = "Invalid Data";
                }
            }
        }


        /*
        // This is the Insert method to insert the entered Course item
        // USAGE: <asp:FormView InsertMethod="InsertItem">
        public void InsertItem()
        {
            using (_db)
            {
                var item = new TrainingCenter.Models.Course();

                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    // Save changes
                    _db.Courses.Add(item);
                    _db.SaveChanges();

                    Response.Redirect("Default");
                }
            }
        }

        protected void ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Cancel", StringComparison.OrdinalIgnoreCase))
            {
                Response.Redirect("Default");
            }
        }*/
    }
}
