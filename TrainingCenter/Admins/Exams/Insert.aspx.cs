﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using TrainingCenter.Models;
using System.Data;

namespace TrainingCenter.Admins.Exams
{
    public partial class Insert : System.Web.UI.Page
    {
        protected TrainingCenter.Models.ApplicationDbContext _db = new TrainingCenter.Models.ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // set year textbox default value to current year
                yearTxt.Text = DateTime.Now.Year.ToString();

                // set default month
                monthDropDown.SelectedIndex = DateTime.Now.Month;

                // fill SchoolyearDropDown
                var schoolYears = _db.SchoolYears.ToList();
                SchoolyearDropDown.DataSource = schoolYears;
                SchoolyearDropDown.DataBind();
            }
        }

        public IQueryable<Course> GetCourses([System.Web.ModelBinding.Control]int? SchoolyearDropDown)
        {
            if (SchoolyearDropDown != null)
                return _db.Courses.Where(c => c.SchoolYearID == SchoolyearDropDown);
            return null;
        }

        protected void InsertButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    Exam exam = new Exam();
                    exam.CourseID = int.Parse(coursesDropDown.SelectedValue);
                    exam.Month = int.Parse(monthDropDown.SelectedValue);
                    exam.Half = int.Parse(halfDropDown.SelectedValue);
                    exam.Year = int.Parse(yearTxt.Text);
                    exam.FullMark = int.Parse(fullMarkTxt.Text);
                    if (calender.SelectedDate.Date != DateTime.MinValue)
                        exam.ExamDate = calender.SelectedDate;
                    _db.Exams.Add(exam);

                    // save new exam
                    _db.SaveChanges();

                    // add initial grades for the exam
                    var subscriptions = _db.StudentSubscriptions.Where(subscription =>
                        subscription.Course.Exams.Any(ex => ex.Half == exam.Half && ex.Month == exam.Month &&
                        ex.Year == exam.Year && ex.CourseID == exam.CourseID)).Include(subscription => subscription.ApplicationUser).ToList();

                    foreach (var subscription in subscriptions)
                    {
                        _db.StudentGrades.Add(new StudentGrade()
                        {
                            StudentID = subscription.ApplicationUser.Id,
                            Month = exam.Month,
                            Half = exam.Half,
                            Year = exam.Year,
                            CourseID = exam.CourseID,
                            Grade = -1 // initial grade (means ungraded)
                        });
                    }

                    // save new grades
                    _db.SaveChanges();
                    Response.Redirect("~/Admins/Exams/Default");
                }
                catch (Exception ex) { errorMessage.Text = "An error occured."; }
            }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admins/Exams/Default");
        }


        /*
       // This is the Insert method to insert the entered Exam item
       // USAGE: <asp:FormView InsertMethod="InsertItem">
       public void InsertItem()
       {
           using (_db)
           {
               var item = new TrainingCenter.Models.Exam();

               TryUpdateModel(item);

               if (ModelState.IsValid)
               {
                   // Save changes
                   _db.Exams.Add(item);
                   _db.SaveChanges();

                   Response.Redirect("Default");
               }
           }
       }*/
    }
}
