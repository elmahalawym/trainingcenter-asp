﻿<%@ Page Title="TeacherDiscountDelete" Language="C#" MasterPageFile="~/Site.Master" CodeBehind="Delete.aspx.cs" Inherits="TrainingCenter.Admins.Discounts.Delete" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div>
        <p>&nbsp;</p>
        <h3>Are you sure want to delete this TeacherDiscount?</h3>
        <asp:FormView runat="server"
            ItemType="TrainingCenter.Models.TeacherDiscount" DataKeyNames="ID"
            DeleteMethod="DeleteItem" SelectMethod="GetItem"
            OnItemCommand="ItemCommand" RenderOuterTable="false">
            <EmptyDataTemplate>
                Cannot find the TeacherDiscount with ID <%: Request.QueryString["ID"] %>
            </EmptyDataTemplate>
            <ItemTemplate>
                <fieldset class="form-horizontal">
                    <legend>حذف الخصم</legend>

                    <!-- Amount -->
                    <div class="form-group">
                        <div class="col-md-10">
                            <asp:TextBox ID="amountTxt" runat="server" TextMode="Number" CssClass="form-control"
                                Text='<%# Eval("Amount") %>' />
                            <asp:RequiredFieldValidator runat="server" ErrorMessage="يجب تحديد القيمة"
                                ControlToValidate="amountTxt" CssClass="text-danger" />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="amountTxt" CssClass="col-md-2 control-label">قيمة الخصم</asp:Label>
                    </div>

                    <!-- Notes -->
                    <div class="form-group">
                        <div class="col-md-10">
                            <asp:TextBox ID="notesTxt" runat="server" CssClass="form-control" TextMode="MultiLine"
                                Text='<%# Eval("Notes") %>' />
                        </div>
                        <asp:Label runat="server" AssociatedControlID="notesTxt" CssClass="col-md-2 control-label">ملاحظات</asp:Label>
                    </div>
                    <div class="row">
                        &nbsp;
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" CssClass="btn btn-danger" />
                            <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" CssClass="btn btn-default" />
                        </div>
                    </div>
                </fieldset>
            </ItemTemplate>
        </asp:FormView>
    </div>
</asp:Content>

