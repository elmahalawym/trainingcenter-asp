namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class StudentSubscriptionDateTimeNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.StudentSubscriptions", "SubscriptionDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.StudentSubscriptions", "SubscriptionDate", c => c.DateTime(nullable: false));
        }
    }
}
