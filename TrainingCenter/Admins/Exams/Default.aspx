﻿<%@ Page Title="ExamList" Language="C#" MasterPageFile="~/Admins/Admins.master" CodeBehind="Default.aspx.cs" Inherits="TrainingCenter.Admins.Exams.Default" %>

<%@ Register TagPrefix="FriendlyUrls" Namespace="Microsoft.AspNet.FriendlyUrls" %>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">


    <h2>الإمتحانات</h2>
    <p>
        <asp:HyperLink runat="server" NavigateUrl="Insert" Text="إضافة إمتحان" />
        |
        <asp:HyperLink runat="server" NavigateUrl="ExamCollection" Text="تحديد مجموعة إمتحانات" />
    </p>

    <div class="form-inline">
        <!-- Filter by year -->
        <div class="form-group">
            <asp:Label runat="server" Text="السنة" AssociatedControlID="yearTxt"></asp:Label>
            <asp:TextBox runat="server" ID="yearTxt" CssClass="form-control" AutoPostBack="true" TextMode="Number"></asp:TextBox>
        </div>

        <!-- Filter by month -->
        <div class="form-group">
            <asp:Label runat="server" Text="الشهر" AssociatedControlID="monthDropDown"></asp:Label>
            <asp:DropDownList runat="server" ID="monthDropDown" CssClass="form-control" AutoPostBack="true">
                <asp:ListItem Text="كل الشهور" Value="-1" />
                <asp:ListItem Text="يناير" Value="1" />
                <asp:ListItem Text="فبراير" Value="2" />
                <asp:ListItem Text="مارس" Value="3" />
                <asp:ListItem Text="إبريل" Value="4" />
                <asp:ListItem Text="مايو" Value="5" />
                <asp:ListItem Text="يونيو" Value="6" />
                <asp:ListItem Text="يوليو" Value="7" />
                <asp:ListItem Text="أغسطس" Value="8" />
                <asp:ListItem Text="سبتمبر" Value="9" />
                <asp:ListItem Text="أكتوبر" Value="10" />
                <asp:ListItem Text="نوفمبر" Value="11" />
                <asp:ListItem Text="ديسمبر" Value="12" />
            </asp:DropDownList>
        </div>

<p></p>
        <!-- Filter by half -->
        <div class="form-group">
            <asp:Label runat="server" Text="النصف" AssociatedControlID="halfDropDown"></asp:Label>
            <asp:DropDownList runat="server" CssClass="form-control" ID="halfDropDown" AutoPostBack="true">
                <asp:ListItem Text="الكل" Value="-1" />
                <asp:ListItem Text="أول" Value="1" />
                <asp:ListItem Text="ثاني" Value="2" />
            </asp:DropDownList>
        </div>

       
        <!-- Filter by school year -->
        <div class="form-group">
            <asp:Label runat="server" Text="السنة الدراسية" AssociatedControlID="schoolYearsDropDown" CssClass="control-label" />
            <asp:DropDownList runat="server" ID="schoolYearsDropDown" AppendDataBoundItems="true"
                DataValueField="SchoolYearID" DataTextField="Name" AutoPostBack="true"
                CssClass="form-control">
                <asp:ListItem Text="كل السنوات" Value="-1" />
            </asp:DropDownList>
        </div>

        <%--<asp:Button runat="server" Text="بحث" CssClass="btn btn-default"/>--%>
    </div>
    <br />

    <div>
        <asp:ListView ID="ListView1" runat="server"
            DataKeyNames="Month,Half"
            ItemType="TrainingCenter.Models.Exam"
            SelectMethod="GetData">
            <EmptyDataTemplate>
                لا توجد امتحانات
            </EmptyDataTemplate>
            <LayoutTemplate>
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>
                                <asp:Label Text="السنة" runat="server" />
                            </th>
                            <th>
                                <asp:Label Text="الشهر" runat="server" />
                            </th>
                            <th>
                                <asp:Label Text="النصف" runat="server" />
                            </th>
                            <th>
                                <asp:Label Text="الكورس" runat="Server" />
                            </th>
                            <th>
                                <asp:LinkButton Text="موعد الإمتحان" CommandName="Sort" CommandArgument="ExamDate" runat="Server" />
                            </th>
                            <th>
                                <asp:LinkButton Text="الدرجة النهائية" CommandName="Sort" CommandArgument="FullMark" runat="Server" />
                            </th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr runat="server" id="itemPlaceholder" />
                    </tbody>
                </table>
                <asp:DataPager PageSize="5" runat="server">
                    <Fields>
                        <asp:NextPreviousPagerField ShowLastPageButton="False" ShowNextPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                        <asp:NumericPagerField ButtonType="Button" NumericButtonCssClass="btn" CurrentPageLabelCssClass="btn disabled" NextPreviousButtonCssClass="btn" />
                        <asp:NextPreviousPagerField ShowFirstPageButton="False" ShowPreviousPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                    </Fields>
                </asp:DataPager>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:DynamicControl runat="server" DataField="Year" Mode="ReadOnly" ID="Year" />
                    </td>
                    <td>
                        <%--<asp:DynamicControl runat="server" DataField="Month" Mode="ReadOnly" ID="Month" />--%>
                        <%# Item.GetMonth() %>
                    </td>
                    <td>
                        <%--<asp:DynamicControl runat="server" DataField="Half" Mode="ReadOnly" ID="Half" />--%>
                        <%# Item.GetHalf() %>
                    </td>
                    <td>
                        <%#: Item.Course.CourseName + " - " + Item.Course.SchoolYear.Name %>
                    </td>
                    <td>
                        <%#: (Item.ExamDate != null) ? Item.ExamDate.Value.ToShortDateString() : "غير محدد"  %>
                    </td>
                    <td>
                        <asp:DynamicControl runat="server" DataField="FullMark" ID="FullMark" Mode="ReadOnly" />
                    </td>
                    <td>
                        <asp:HyperLink runat="server" NavigateUrl='<%# FriendlyUrl.Href("~/Admins/Exams/Edit", Item.Year, Item.Month, Item.Half, Item.CourseID) %>' Text="تعديل\حذف" />
                        |
                        <asp:HyperLink runat="server" NavigateUrl='<%# FriendlyUrl.Href("~/Admins/Exams/StudentGrades", Item.Year, Item.Month, Item.Half, Item.CourseID) %>' Text="الدرجات" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
</asp:Content>

