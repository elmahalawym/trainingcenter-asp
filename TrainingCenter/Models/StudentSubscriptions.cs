﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrainingCenter.Models
{
    public class StudentSubscription
    {
        [Key, Column(Order = 0)]
        [ForeignKey("ApplicationUser")]
        public string StudentID { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        [Key, Column(Order = 1)]
        [ForeignKey("Course")]
        public int CourseID { get; set; }

        public virtual Course Course { get; set; }

        public DateTime? SubscriptionDate { get; set; }
    }
}