namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Salaries : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Salaries", "Year", c => c.Int(nullable: false));
            AddColumn("dbo.Salaries", "Month", c => c.Int(nullable: false));
            AddColumn("dbo.Salaries", "PaymentDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Salaries", "PaymentDate");
            DropColumn("dbo.Salaries", "Month");
            DropColumn("dbo.Salaries", "Year");
        }
    }
}
