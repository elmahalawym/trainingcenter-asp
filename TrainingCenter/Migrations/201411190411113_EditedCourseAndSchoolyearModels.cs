namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EditedCourseAndSchoolyearModels : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SchoolYears", "GeneralPrice", c => c.Double(nullable: false));
            AddColumn("dbo.SchoolYears", "SpecialPrice", c => c.Double(nullable: false));
            DropColumn("dbo.Courses", "GeneralPrice");
            DropColumn("dbo.Courses", "SpecialPrice");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Courses", "SpecialPrice", c => c.Double(nullable: false));
            AddColumn("dbo.Courses", "GeneralPrice", c => c.Double(nullable: false));
            DropColumn("dbo.SchoolYears", "SpecialPrice");
            DropColumn("dbo.SchoolYears", "GeneralPrice");
        }
    }
}
