﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrainingCenter.Models
{
    public class Course
    {
        public Course()
        {
            Exams = new List<Exam>();
            StudentSubscriptions = new List<StudentSubscription>();
        }

        [Key]
        public int CourseID { get; set; }

        [ForeignKey("ApplicationUser")]
        public string TeacherID { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        [ForeignKey("SchoolYear")]
        public int SchoolYearID { get; set; }

        public virtual SchoolYear SchoolYear { get; set; }

        public string CourseName { get; set; }

        [Display(Name="عدد الحصص في الأسبوع")]
        public int NumberOfSessionsPerWeek { get; set; }


        public virtual ICollection<Exam> Exams { get; set; }

        public virtual ICollection<StudentSubscription> StudentSubscriptions { get; set; }

        public override string ToString()
        {
            return CourseName + " - " + this.SchoolYear.Name;
        }


    }
}