﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TrainingCenter.Models
{
    public class TeacherViewModel
    {
        [ScaffoldColumn(false)]
        [Key]
        public string ApplicationUserID { get; set; }

        [Required]
        [Display(Name="اسم المستخدم")]
        public string UserName { get; set; }

        public string Email { get; set; }


        [Display(Name = "الإسم الأول")]
        [Required]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "إسم العائلة")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "رقم التليفون")]
        public string Phone { get; set; }
    }
}