﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Microsoft.AspNet.FriendlyUrls;
using System.Web.UI.WebControls;

namespace TrainingCenter.Admins
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            (Page.Master as Admins).SelectedMenuItem = "default";
        }

    }
}