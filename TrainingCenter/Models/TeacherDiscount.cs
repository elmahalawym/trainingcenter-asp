﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TrainingCenter.Models
{
    public enum DiscountTypes
    {
        [Display(Name = "التأخير")]
        Late,
        [Display(Name = "غياب بدون عذر")]
        Absence,
        [Display(Name = "غياب بعذر")]
        AbsenceWithExcuse
    }

    public class TeacherDiscount
    {
        [Key]
        public int ID { get; set; }

        [ForeignKey("ApplicationUser")]
        public string TeacherID { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public DateTime DiscountDate { get; set; }

        public DiscountTypes DiscountType { get; set; }

        public double Amount { get; set; }

        public string  Notes { get; set; }

        public string GetDiscountTypeName()
        {
            switch (DiscountType)
            {
                case DiscountTypes.Absence:
                    return "غياب بدون عذر";
                case DiscountTypes.AbsenceWithExcuse:
                    return "غياب بعذر";
                case DiscountTypes.Late:
                    return "التأخير";
                default:
                    return "";
            }
        }
    }
}