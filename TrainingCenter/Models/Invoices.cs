﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TrainingCenter.Models
{
    public class Invoice
    {
        public Invoice()
        {
            Paid = false;
            PaidAmount = 0.0d;
        }

        [ForeignKey("ApplicationUser")]
        [Key, Column(Order = 0)]
        public string StudentID { get; set; }

        [Key, Column(Order = 1)]
        public int Year { get; set; }

        [Key, Column(Order = 2)]
        public int Month { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public double Price { get; set; }
        public bool Paid { get; set; }

        public double PaidAmount { get; set; }

    }
}