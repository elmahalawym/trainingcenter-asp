﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using Microsoft.AspNet.FriendlyUrls.ModelBinding;
using TrainingCenter.Models;
using Microsoft.AspNet.FriendlyUrls;
namespace TrainingCenter.Admins.StudentViewModels
{
    public partial class Edit : System.Web.UI.Page
    {
        protected ApplicationDbContext _db = new ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // fill school years drop down
                var schoolYears = _db.SchoolYears.ToList();
                schoolYearDropDown.DataSource = schoolYears;
                schoolYearDropDown.DataBind();

                if (FriendlyUrl.Segments.Count > 0)
                {
                    string userId = FriendlyUrl.Segments[0];

                    ApplicationUser user = _db.Users.Where(u => u.Id == userId).FirstOrDefault();

                    // populate from controls with user data
                    if (user != null)
                    {
                        // fill user data
                        usernameTxt.Text = user.UserName;
                        emailTxt.Text = user.Email;
                        schoolYearDropDown.SelectedValue = user.SchoolYearID.ToString();
                        firstNameTxt.Text = user.FirstName;
                        lastNameTxt.Text = user.LastName;
                        phoneTxt.Text = user.Phone;
                        generalTypeDropDown.SelectedValue = (bool)user.GeneralType ? "1" : "0";

                        schoolNameTxt.Text = user.SchoolName;
                        birthDateTxt.Text = (user.BirthDate == null) ? "" : user.BirthDate.ToString();
                        fatherPhoneTxt.Text = user.FatherPhoneNumber;
                        motherPhoneTxt.Text = user.MotherPhoneNumber;
                        mobile1Txt.Text = user.Mobile1;
                        mobile2Txt.Text = user.Mobile2;
                        notesTxt.Text = user.Notes;
                        isActiveCB.Checked = user.IsStudentActive ?? false;

                        // set current month state
                        List<string> hMonthList = Helpers.GetHMonthList(Server.MapPath(string.Format("~/App_Data/{0}", Helpers.GetHMonthFileName(DateTime.Now.Year, DateTime.Now.Month))));
                        curMonthSubType.SelectedIndex = hMonthList.Contains(user.Id) ? 1 : 0;

                        // fill courses list 
                        schoolYearDropDown_SelectedIndexChanged(this, null);

                        // set selected courses
                        var subscriptions = _db.StudentSubscriptions.Where(s => s.StudentID == userId).ToList();
                        foreach (var item in subscriptions)
                        {
                            ListItem selectItem = coursesList.Items.Cast<ListItem>().Where(
                                i => int.Parse(i.Value) == item.CourseID).FirstOrDefault();
                            selectItem.Selected = true;
                        }
                    }
                    else
                    {
                        ErrorMessage.Text = "عفوا بيانات الطالب غير موجودة";
                        editBtn.Enabled = false;
                    }
                }
                else
                {
                    Response.Redirect("~/Admins/Students/Default");
                }
            }
        }

        // change courses in coursesList when the user changes school year
        protected void schoolYearDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedSchoolyearId = -1;
            if (!int.TryParse(schoolYearDropDown.SelectedValue, out selectedSchoolyearId))
            {
                coursesList.DataSource = null;
                coursesList.Items.Clear();
                return;
            }

            var courses = _db.Courses.Where(c => c.SchoolYearID == selectedSchoolyearId).ToList();
            coursesList.DataSource = courses;
            coursesList.DataBind();
        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            if (FriendlyUrl.Segments.Count > 0)
            {
                string userId = FriendlyUrl.Segments[0];

                DateTime birthDate;
                DateTime? birthDateEntry = null;
                if (!string.IsNullOrEmpty(birthDateTxt.Text) && DateTime.TryParse(birthDateTxt.Text, out birthDate))
                    birthDateEntry = birthDate;


                bool generalType = (int.Parse(generalTypeDropDown.SelectedValue) == 1) ? true : false;
                ApplicationUser user = _db.Users.Where(u => u.Id == userId).FirstOrDefault();
                if (user != null)
                {
                    user.UserName = usernameTxt.Text;
                    user.Email = emailTxt.Text;
                    user.SchoolYearID = int.Parse(schoolYearDropDown.SelectedValue);
                    user.FirstName = firstNameTxt.Text;
                    user.LastName = lastNameTxt.Text;
                    user.Phone = phoneTxt.Text;
                    user.GeneralType = generalType;

                    user.SchoolName = schoolNameTxt.Text;
                    user.BirthDate = birthDateEntry;
                    user.FatherPhoneNumber = fatherPhoneTxt.Text;
                    user.MotherPhoneNumber = motherPhoneTxt.Text;
                    user.Mobile1 = mobile1Txt.Text;
                    user.Mobile2 = mobile2Txt.Text;
                    user.Notes = notesTxt.Text;
                    user.IsStudentActive = isActiveCB.Checked;

                    try
                    {
                        _db.SaveChanges();
                    }
                    catch (Exception)
                    {
                        ErrorMessage.Text = "an error occured while updating student's data";
                        return;
                    }

                    // set month state for the student
                    string hMonthFilePath = Server.MapPath(string.Format("~/App_Data/{0}", Helpers.GetHMonthFileName(DateTime.Now.Year, DateTime.Now.Month)));
                    List<string> hMonthList = Helpers.GetHMonthList(hMonthFilePath);
                    if(curMonthSubType.SelectedIndex == 1) // half month
                    {
                        // add student id to list if not exists
                        if (!hMonthList.Contains(user.Id))
                            hMonthList.Add(user.Id);
                    }
                    else // full month
                    {
                        // remove student id from the list if it exists
                        if (hMonthList.Contains(user.Id))
                            hMonthList.Remove(user.Id);
                    }
                    // save new list
                    Helpers.SetHMonthList(hMonthFilePath, hMonthList);

                    // delete student subscriptions
                    var subscriptions = _db.StudentSubscriptions.Where(s => s.StudentID == userId);
                    foreach (StudentSubscription item in subscriptions)
                        _db.StudentSubscriptions.Remove(item);

                    _db.SaveChanges();

                    // add new student subscriptions
                    var selectedItems = coursesList.Items.Cast<ListItem>().Where
                        (li => li.Selected).ToList();
                    foreach (ListItem item in selectedItems)
                    {
                        _db.StudentSubscriptions.Add(new StudentSubscription()
                        {
                            CourseID = int.Parse(item.Value),
                            StudentID = userId
                        });
                    }
                    _db.SaveChanges();

                    // update number of students
                    string absNumbersFilePath = Server.MapPath(string.Format("~/App_Data/{0}", Helpers.AbsoluteNumbersFileName));
                    Helpers.UpdateNumberOfStudents(Server.MapPath(string.Format("~/App_Data/{0}", Helpers.NumberOfStudentsFileName)), absNumbersFilePath, hMonthFilePath);
                }
            }
            Response.Redirect("~/Admins/Students/Default");
        }



        /*
        // This is the Update methd to update the selected StudentViewModel item
        // USAGE: <asp:FormView UpdateMethod="UpdateItem">
        public void UpdateItem(string  ApplicationUserID)
        {
            
            using (_db)
            {
                var item = _db.Users.Find(ApplicationUserID);

                if (item == null)
                {
                    // The item wasn't found
                    ModelState.AddModelError("", String.Format("Item with id {0} was not found", ApplicationUserID));
                    return;
                }

                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    // Save changes here
                    _db.SaveChanges();
                    Response.Redirect("../Default");
                }
            }
        }

        // This is the Select method to selects a single StudentViewModel item with the id
        // USAGE: <asp:FormView SelectMethod="GetItem">
        public TrainingCenter.Models.StudentViewModel GetItem([FriendlyUrlSegmentsAttribute(0)]string ApplicationUserID)
        {
            throw new NotImplementedException();
            //if (ApplicationUserID == null)
            //{
            //    return null;
            //}

            //using (_db)
            //{
            //    return _db.Users.Find(ApplicationUserID);
            //}
        }

        protected void ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Cancel", StringComparison.OrdinalIgnoreCase))
            {
                Response.Redirect("../Default");
            }
        }*/
    }
}
