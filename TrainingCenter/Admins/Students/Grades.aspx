﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Grades.aspx.cs" Inherits="TrainingCenter.Admins.Students.Grades" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script runat="server">
        //TrainingCenter.Models.ApplicationDbContext db = new TrainingCenter.Models.ApplicationDbContext();

        //List<TrainingCenter.Models.Course> courses = db.Courses.Where(c => c.StudentSubscriptions.Any(s => s.StudentID == studentId)).ToList();
        
    </script>

    <h3 class="dontprint">من</h3>
    <div class="form-inline dontprint">
        <asp:Label runat="server" AssociatedControlID="yearTxtFrom" Text="السنة" CssClass="control-label" />
        <asp:TextBox runat="server" ID="yearTxtFrom" TextMode="Number" CssClass="form-control" AutoPostBack="true" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="text-danger"
            ErrorMessage="يجب إدخال السنة" ControlToValidate="yearTxtFrom" />

        <asp:Label runat="server" AssociatedControlID="monthDropDownFrom" Text="الشهر" CssClass="control-label" />
        <asp:DropDownList runat="server" ID="monthDropDownFrom" CssClass="form-control" AutoPostBack="true">
            <asp:ListItem Text="اختر الشهر" Value="-1" />
            <asp:ListItem Text="يناير" Value="1" />
            <asp:ListItem Text="فبراير" Value="2" />
            <asp:ListItem Text="مارس" Value="3" />
            <asp:ListItem Text="إبريل" Value="4" />
            <asp:ListItem Text="مايو" Value="5" />
            <asp:ListItem Text="يونيو" Value="6" />
            <asp:ListItem Text="يوليو" Value="7" />
            <asp:ListItem Text="أغسطس" Value="8" />
            <asp:ListItem Text="سبتمبر" Value="9" />
            <asp:ListItem Text="أكتوبر" Value="10" />
            <asp:ListItem Text="نوفمبر" Value="11" />
            <asp:ListItem Text="ديسمبر" Value="12" />
        </asp:DropDownList>
        <asp:Label runat="server" Text="النصف" AssociatedControlID="halfDropDownFrom" CssClass="control-label" />
        <asp:DropDownList runat="server" ID="halfDropDownFrom" CssClass="form-control" AutoPostBack="true">
            <asp:ListItem Text="أول" Value="1" />
            <asp:ListItem Text="ثاني" Value="2" />
        </asp:DropDownList>
    </div>
    <h3 class="dontprint">إلي</h3>
    <div class="form-inline dontprint">
                <asp:Label runat="server" AssociatedControlID="yearTxtTo" Text="السنة" CssClass="control-label" />
        <asp:TextBox runat="server" ID="yearTxtTo" TextMode="Number" CssClass="form-control" AutoPostBack="true" />
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="text-danger"
            ErrorMessage="يجب إدخال السنة" ControlToValidate="yearTxtFrom" />

        <asp:Label runat="server" AssociatedControlID="monthDropDownTo" Text="الشهر" CssClass="control-label" />
        <asp:DropDownList runat="server" ID="monthDropDownTo" CssClass="form-control" AutoPostBack="true">
            <asp:ListItem Text="اختر الشهر" Value="-1" />
            <asp:ListItem Text="يناير" Value="1" />
            <asp:ListItem Text="فبراير" Value="2" />
            <asp:ListItem Text="مارس" Value="3" />
            <asp:ListItem Text="إبريل" Value="4" />
            <asp:ListItem Text="مايو" Value="5" />
            <asp:ListItem Text="يونيو" Value="6" />
            <asp:ListItem Text="يوليو" Value="7" />
            <asp:ListItem Text="أغسطس" Value="8" />
            <asp:ListItem Text="سبتمبر" Value="9" />
            <asp:ListItem Text="أكتوبر" Value="10" />
            <asp:ListItem Text="نوفمبر" Value="11" />
            <asp:ListItem Text="ديسمبر" Value="12" />
        </asp:DropDownList>
        <asp:Label runat="server" Text="النصف" AssociatedControlID="halfDropDownTo" CssClass="control-label" />
        <asp:DropDownList runat="server" ID="halfDropDownTo" CssClass="form-control" AutoPostBack="true">
            <asp:ListItem Text="أول" Value="1" />
            <asp:ListItem Text="ثاني" Value="2" />
        </asp:DropDownList>
    </div>
    <br />
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>السنة</th>
                <th>الشهر</th>
                <th>المرحلة</th>
                <%foreach (var course in courses)
                  { %>
                <th><%= course.CourseName %></th>
                <% } %>
            </tr>
        </thead>
        <tbody>
            <%foreach (var gradeViewModel in GradesList)
              { %>
            <tr>
                <td>
                    <%= gradeViewModel.Year %>
                </td>
                <td>
                    <%= TrainingCenter.Helpers.GetMonthText(gradeViewModel.Month) %>
                </td>
                <td><%= ((gradeViewModel.Half == 1) ? "أول" : "ثاني") %></td>

                <%foreach (double grade in gradeViewModel.Grades)
                  { %>
                <td><%= (grade >= 0 ) ?string.Format("{0:0.00}", grade) : "-" %></td>
                <% } %>
            </tr>
            <% } %>
        </tbody>
    </table>
    <%--    <asp:ListView ID="gradesList" runat="server"
        DataKeyNames="Year,Month,Half">
        <LayoutTemplate>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <%foreach (var course in courses)
                          { %>
                        <th><%= course.CourseName %></th>
                        <% } %>
                        <th><%= courses.Count %></th>
                    </tr>
                </thead>
                <tbody>
                    <tr id="itemPlaceHolder" runat="server" />
                </tbody>
            </table>
        </LayoutTemplate>
        <EmptyDataTemplate>
            <p><%= courses.Count %></p>
        </EmptyDataTemplate>
        <ItemTemplate>
        </ItemTemplate>
    </asp:ListView>--%>
</asp:Content>
