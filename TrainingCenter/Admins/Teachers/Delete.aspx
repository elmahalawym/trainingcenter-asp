﻿<%@ Page Title="TeacherViewModelDelete" Language="C#" MasterPageFile="~/Site.Master" CodeBehind="Delete.aspx.cs" Inherits="TrainingCenter.Admins.TeacherViewModels.Delete" %>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div>
		<p>&nbsp;</p>
        <h3>هل أنت متأكد تردي حذف بيانات المدرس؟</h3>

        <asp:Label runat="server" ID="userNameTxt"></asp:Label>

        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="Delete_Click" Text="حذف" CssClass="btn btn-primary" />
                <a href="<%= Microsoft.AspNet.FriendlyUrls.FriendlyUrl.Href("~/Admins/Teachers/Default") %>" class="btn btn-default">إلغاء</a>
            </div>
        </div>
    </div>
</asp:Content>

