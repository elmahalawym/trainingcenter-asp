﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TrainingCenter.Models
{
    public class Salary
    {
        //[Key]
        //public int SalaryID { get; set; }

        [Key, Column(Order=0)]
        [ForeignKey("ApplicationUser")]
        public string TeacherID { get; set; }
        
        [Key, Column(Order = 1)]
        public int Year { get; set; }

        [Key, Column(Order = 2)]
        public int Month { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public double Price { get; set; }

        public bool IsPaid { get; set; }

        public double PaidAmount { get; set; }

        public DateTime? PaymentDate { get; set; }

        public string Notes { get; set; }
    }
}