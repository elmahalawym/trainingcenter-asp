﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrainingCenter.Models;
using System.Data.Entity;

namespace TrainingCenter.Admins.Exams
{
    public partial class ExamCollection : System.Web.UI.Page
    {
        protected ApplicationDbContext _db = new ApplicationDbContext();
        int year, month, half;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                // set default active view
                MultiView1.ActiveViewIndex = 0;

                // set page default values
                yearTxt.Text = DateTime.Now.Year.ToString();
                monthDropDown.SelectedIndex = DateTime.Now.Month;
                halfDropDown.SelectedIndex = (DateTime.Now.Day < 15) ? 1 : 2;

                errorMessage.Text = "";

            }

            bool res = int.TryParse(yearTxt.Text, out year);
            month = monthDropDown.SelectedIndex;
            half = halfDropDown.SelectedIndex;

            if (!res)
                Response.Redirect("~/Admins/Exams/Default");
        }

        protected void nextBtn_Click(object sender, EventArgs e)
        {
            int y;
            if (!string.IsNullOrEmpty(yearTxt.Text) && int.TryParse(yearTxt.Text, out y) &&
                int.Parse(monthDropDown.SelectedValue) > 0 && int.Parse(halfDropDown.SelectedValue) > 0)
            {
                errorMessage.Text = "";
                yearLabel.Text = yearTxt.Text;
                monthLabel.Text = monthDropDown.SelectedItem.Text;
                halfLabel.Text = halfDropDown.SelectedItem.Text;

                MultiView1.ActiveViewIndex++;
            }
            else
                errorMessage.Text = "أدخل السنة و الشهر و النصف";
        }

        // The return type can be changed to IEnumerable, however to support
        // paging and sorting, the following parameters must be added:
        //     int maximumRows
        //     int startRowIndex
        //     out int totalRowCount
        //     string sortByExpression
        public IQueryable<TrainingCenter.Models.ExamViewModel> coursesListView_GetData()
        {
           var curCourses = _db.Exams.Where(e => e.Year == year && e.Half == half && e.Month == month).Include(e => e.Course)
                .Include(e => e.Course.SchoolYear).Select(
                e => new ExamViewModel(){
                    CourseId = e.CourseID,
                    CourseName = e.Course.CourseName + " " + e.Course.SchoolYear.Name,
                    FullMark = e.FullMark,
                    IsActive = true
                }).ToList();
            var allCourses = _db.Courses.OrderBy(c => c.SchoolYearID).Include(c => c.SchoolYear).Select( c => 
                new ExamViewModel()
                {
                    CourseId = c.CourseID,
                    CourseName = c.CourseName + " " + c.SchoolYear.Name,
                    FullMark = 0,
                    IsActive = false
                }
                ).ToList();

            List<ExamViewModel> examViewModels = new List<ExamViewModel>();
            foreach (var course in allCourses)
            {
                if(curCourses.Where(c => c.CourseId == course.CourseId).Count() > 0)
                    examViewModels.Add(curCourses.Where(c => c.CourseId == course.CourseId).First());
                else
                    examViewModels.Add(course);                
            }
            return examViewModels.AsQueryable();
        }

        protected void saveBtn_Click(object sender, EventArgs e)
        {
            // current exams
            var curExams = _db.Exams.Where(ex => ex.Year == year && ex.Half == half && ex.Month == month).ToList();

            foreach (GridViewRow row in coursesGridView.Rows)
            {
                ExamViewModel exam = new ExamViewModel();

                CheckBox cb = row.FindControl("itemCB") as CheckBox;
                exam.IsActive = cb.Checked;

                HiddenField cid = row.FindControl("cid") as HiddenField;
                TextBox fullMarkText = row.FindControl("fullMarkText") as TextBox;

                if (string.IsNullOrEmpty(fullMarkText.Text))
                    fullMarkText.Text = "0";

                int courseId, fullMark;
                
                if(!int.TryParse(cid.Value, out courseId) || !int.TryParse(fullMarkText.Text, out fullMark))
                {
                    saveErrorMessage.Text = "Invalid Data";
                    return;
                }
                exam.CourseId = courseId;
                exam.FullMark = fullMark;

                // process exam
                if(exam.IsActive)
                {
                    // if exam is found -> edit full mark
                    if(curExams.Where(ex => ex.CourseID == exam.CourseId).Count() > 0)
                    {
                        _db.Exams.Where(ex => ex.Year == year && ex.Half == half && ex.Month == month &&
                            ex.CourseID == exam.CourseId).First().FullMark = exam.FullMark;
                    }
                    else // else insert it
                    {
                        _db.Exams.Add(new Exam()
                        {
                            FullMark = exam.FullMark,
                            CourseID = exam.CourseId,
                            Year = year,
                            Month = month,
                            Half = half
                        });
                    }
                }
                else
                {
                    // if exam is found -> delete it
                    if(curExams.Where(ex => ex.CourseID == exam.CourseId).Count() > 0)
                    {
                        _db.Exams.Remove(_db.Exams.Where(ex => ex.Year == year && ex.Half == half && ex.Month == month &&
                             ex.CourseID == exam.CourseId).First());
                    }
                }
            }

            // save changes
            _db.SaveChanges();
            Response.Redirect("~/Admins/Exams/Default");
        }

    }//class
}//namespace