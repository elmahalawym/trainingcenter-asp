﻿<%@ Page Title="CourseInsert" Language="C#" MasterPageFile="~/Site.Master" CodeBehind="Insert.aspx.cs" Inherits="TrainingCenter.Admins.Courses.Insert" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div>
        <p>&nbsp;</p>
        <asp:ValidationSummary runat="server" CssClass="alert alert-danger" />
        <p class="text-danger">
            <asp:Literal runat="server" ID="errorMessage" />
        </p>

        <div class="form-horizontal">
            <h4>إضافة كورس</h4>
            <hr />
            <!-- Course Name -->
            <div class="form-group">
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="courseNameTxt" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="courseNameTxt"
                        CssClass="text-danger" ErrorMessage="يجب إدخال اسم الكورس" />
                </div>
                <asp:Label runat="server" AssociatedControlID="courseNameTxt" CssClass="col-md-2 control-label">اسم الكورس</asp:Label>
            </div>

            <!-- School Year -->
<%--            <div class="form-group">
                <div class="col-md-10">

                    <asp:DropDownList runat="server" ID="schoolYearDropDown"
                        DataValueField="SchoolYearID" DataTextField="Name"
                        ItemType="TrainingCenter.Models.SchoolYear"
                        CssClass="form-control" />

                </div>
                <asp:Label runat="server" AssociatedControlID="schoolYearDropDown" CssClass="col-md-2 control-label">السنة الدراسية</asp:Label>
            </div>--%>
            <div class="form-group">
                <div class="col-md-10">

                    <asp:CheckBoxList runat="server" ID="schoolYearList"
                        DataValueField="SchoolYearID" DataTextField="Name">

                    </asp:CheckBoxList>
                      

                </div>
                <asp:Label runat="server" AssociatedControlID="schoolYearList" CssClass="col-md-2 control-label">السنة الدراسية</asp:Label>
            </div>
            <!-- Teacher -->
            <div class="form-group">
                <div class="col-md-10">

                    <asp:DropDownList runat="server" ID="teachersDropDown"
                        DataValueField="ID" DataTextField="Name"
                        ItemType="TrainingCenter.Models.SchoolYear"
                        CssClass="form-control" />
                
                </div>
                
                <asp:Label runat="server" AssociatedControlID="teachersDropDown" CssClass="col-md-2 control-label">المدرس</asp:Label>
            </div>

            <!-- Number of sessions per week -->
            <div class="form-group">
                <div class="col-md-10">

                    <asp:TextBox ID="nSessionsTxt" runat="server" CssClass="form-control" TextMode="Number" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="nSessionsTxt" ErrorMessage="يجب إدخال عدد الحصص" />

                </div>
                <asp:Label runat="server" AssociatedControlID="nSessionsTxt" CssClass="col-md-2 control-label">عدد الحصص في الأسبوع</asp:Label>
            </div>



            <!-- From Buttons -->
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <asp:Button runat="server" OnClick="CreateCourse_Click" Text="تسجيل" CssClass="btn btn-primary" />
                    <a href="<%= Microsoft.AspNet.FriendlyUrls.FriendlyUrl.Href("~/Admins/Courses/Default") %>" class="btn btn-default">إلغاء</a>
                </div>
            </div>
        </div>

        <%--<asp:FormView runat="server"
            ItemType="TrainingCenter.Models.Course" DefaultMode="Insert"
            InsertItemPosition="FirstItem" InsertMethod="InsertItem"
            OnItemCommand="ItemCommand" RenderOuterTable="false">
            <InsertItemTemplate>
                <fieldset class="form-horizontal">
                    <legend>Insert Course</legend>
                    <asp:ValidationSummary runat="server" CssClass="alert alert-danger" />
                    <asp:DynamicControl Mode="Insert"
                        DataField="TeacherID"
                        DataTypeName="TrainingCenter.Models.ApplicationUser"
                        DataTextField="FirstName"
                        DataValueField="Id"
                        UIHint="ForeignKey" runat="server" />
                    <asp:DynamicControl Mode="Insert"
                        DataField="SchoolYearID"
                        DataTypeName="TrainingCenter.Models.SchoolYear"
                        DataTextField="Name"
                        DataValueField="SchoolYearID"
                        UIHint="ForeignKey" runat="server" />
                    <asp:DynamicControl Mode="Insert" DataField="CourseName" runat="server" />
                    <asp:DynamicControl Mode="Insert" DataField="GeneralPrice" runat="server" />
                    <asp:DynamicControl Mode="Insert" DataField="SpecialPrice" runat="server" />
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <asp:Button runat="server" ID="InsertButton" CommandName="Insert" Text="Insert" CssClass="btn btn-primary" />
                            <asp:Button runat="server" ID="CancelButton" CommandName="Cancel" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" />
                        </div>
                    </div>
                </fieldset>
            </InsertItemTemplate>
        </asp:FormView>--%>
    </div>
</asp:Content>
