﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrainingCenter.Models
{
    public class GradesViewModel
    {
        public GradesViewModel()
        {
            Grades = new List<double>();
        }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Half { get; set; }
        public List<double> Grades { get; set; }
    }
}