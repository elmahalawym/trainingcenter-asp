﻿<%@ Page Title="CourseEdit" Language="C#" MasterPageFile="~/Site.Master" CodeBehind="Edit.aspx.cs" Inherits="TrainingCenter.Admins.Courses.Edit" %>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <div>
		<p>&nbsp;</p>
        <asp:ValidationSummary runat="server" CssClass="alert alert-danger" />
        <p class="text-danger">
            <asp:Literal runat="server" ID="errorMessage" />
        </p>

        <div class="form-horizontal">
            <h4>إضافة كورس</h4>
            <hr />
            <!-- Course Name -->
            <div class="form-group">
                <div class="col-md-10">
                    <asp:TextBox runat="server" ID="courseNameTxt" CssClass="form-control" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="courseNameTxt"
                        CssClass="text-danger" ErrorMessage="يجب إدخال اسم الكورس" />
                </div>
                <asp:Label runat="server" AssociatedControlID="courseNameTxt" CssClass="col-md-2 control-label">اسم الكورس</asp:Label>
            </div>

            <!-- School Year -->
            <div class="form-group">
                <div class="col-md-10">

                    <asp:DropDownList runat="server" ID="schoolYearDropDown"
                        DataValueField="SchoolYearID" DataTextField="Name"
                        ItemType="TrainingCenter.Models.SchoolYear"
                        CssClass="form-control" />

                </div>
                <asp:Label runat="server" AssociatedControlID="schoolYearDropDown" CssClass="col-md-2 control-label">السنة الدراسية</asp:Label>
            </div>

            <!-- Teacher -->
            <div class="form-group">
                <div class="col-md-10">

                    <asp:DropDownList runat="server" ID="teachersDropDown"
                        DataValueField="ID" DataTextField="UserName"
                        CssClass="form-control" />

                </div>
                <asp:Label runat="server" AssociatedControlID="teachersDropDown" CssClass="col-md-2 control-label">المدرس</asp:Label>
            </div>
            
            <!-- Number of sessions per week -->
            <div class="form-group">
                <div class="col-md-10">

                    <asp:TextBox ID="nSessionsTxt" runat="server" CssClass="form-control" TextMode="Number" />
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="nSessionsTxt" ErrorMessage="يجب إدخال عدد الحصص" />

                </div>
                <asp:Label runat="server" AssociatedControlID="nSessionsTxt" CssClass="col-md-2 control-label">عدد الحصص في الأسبوع</asp:Label>
            </div>

            <!-- From Buttons -->
            <div class="form-group">
                <div class="col-md-offset-2 col-md-10">
                    <asp:Button runat="server" OnClick="Edit_Click" Text="تعديل" CssClass="btn btn-primary" />
                    <a href="<%= Microsoft.AspNet.FriendlyUrls.FriendlyUrl.Href("~/Admins/Courses/Default") %>" class="btn btn-default">إلغاء</a>
                </div>
            </div>
        </div>


        <%--<asp:FormView runat="server"
            ItemType="TrainingCenter.Models.Course" DefaultMode="Edit" DataKeyNames="CourseID"
            UpdateMethod="UpdateItem" SelectMethod="GetItem"
            OnItemCommand="ItemCommand" RenderOuterTable="false">
            <EmptyDataTemplate>
                Cannot find the Course with CourseID <%: Request.QueryString["CourseID"] %>
            </EmptyDataTemplate>
            <EditItemTemplate>
                <fieldset class="form-horizontal">
                    <legend>Edit Course</legend>
					<asp:ValidationSummary runat="server" CssClass="alert alert-danger"  />                 
							<asp:DynamicControl Mode="Edit" 
								DataField="TeacherID" 
								DataTypeName="TrainingCenter.Models.ApplicationUser" 
								DataTextField="FirstName" 
								DataValueField="Id" 
								UIHint="ForeignKey" runat="server" />
							<asp:DynamicControl Mode="Edit" 
								DataField="SchoolYearID" 
								DataTypeName="TrainingCenter.Models.SchoolYear" 
								DataTextField="Name" 
								DataValueField="SchoolYearID" 
								UIHint="ForeignKey" runat="server" />
						    <asp:DynamicControl Mode="Edit" DataField="CourseName" runat="server" />
						    <asp:DynamicControl Mode="Edit" DataField="GeneralPrice" runat="server" />
						    <asp:DynamicControl Mode="Edit" DataField="SpecialPrice" runat="server" />
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
							<asp:Button runat="server" ID="UpdateButton" CommandName="Update" Text="Update" CssClass="btn btn-primary" />
							<asp:Button runat="server" ID="CancelButton" CommandName="Cancel" Text="Cancel" CausesValidation="false" CssClass="btn btn-default" />
						</div>
					</div>
                </fieldset>
            </EditItemTemplate>
        </asp:FormView>--%>
    </div>
</asp:Content>

