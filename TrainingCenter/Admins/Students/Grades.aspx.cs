﻿using Microsoft.AspNet.FriendlyUrls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrainingCenter.Models;

namespace TrainingCenter.Admins.Students
{
    public partial class Grades : System.Web.UI.Page
    {
        ApplicationDbContext db = new ApplicationDbContext();
        string studentId;
        protected List<Course> courses { get; set; }
        public List<GradesViewModel> GradesList { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (FriendlyUrl.Segments.Count != 1)
            {
                Response.Redirect("~/Admins/Students/Default");
                return;
            }
            studentId = FriendlyUrl.Segments[0];
            ApplicationUser user = db.Users.Find(studentId);

            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
            string studentsRoleId = RoleManager.FindByName("student").Id;

            if (user == null || !user.Roles.Any(r => r.RoleId == studentsRoleId))
            {
                Response.Redirect("~/Admins/Students/Default");
                return;
            }

            GradesList = new List<GradesViewModel>();
            courses = new List<Course>();

            // add grid columns
            int fromYear, toYear;
            if (IsPostBack && int.TryParse(yearTxtFrom.Text, out fromYear) && int.TryParse(yearTxtTo.Text, out toYear) && fromYear >= DateTime.Now.Year - 1 && toYear <= DateTime.Now.Year && int.Parse(monthDropDownFrom.SelectedValue) != -1 && int.Parse(monthDropDownTo.SelectedValue) != -1)
            {
                int schoolYearId = (int)user.SchoolYearID;
                courses = db.Courses.Where(c => c.StudentSubscriptions.Any(s => s.StudentID == studentId)).ToList();

                var grades = db.StudentGrades.Where(g => g.StudentID == studentId).ToList();

                Stage from = new Stage(fromYear, monthDropDownFrom.SelectedIndex, halfDropDownFrom.SelectedIndex + 1);
                Stage to = new Stage(toYear, monthDropDownTo.SelectedIndex, halfDropDownTo.SelectedIndex + 1);
                //for (int year = fromYear; year <= toYear; year++)
                //{
                //    for (int month = monthDropDownFrom.SelectedIndex; month <= monthDropDownTo.SelectedIndex; month++) //year
                //    {
                //        for (int half = 1; half <= 2; half++)
                //        {
                //            GradesViewModel vm = new GradesViewModel();
                //            vm.Year = year;
                //            vm.Month = month;
                //            vm.Half = half;
                //            foreach (var course in courses)
                //            {
                //                var grade = grades.Where(g => g.Year == vm.Year && g.Month == month && g.Half == vm.Half && g.CourseID == course.CourseID).FirstOrDefault();
                //                vm.Grades.Add((grade != null) ? grade.Grade : -1);
                //            }
                //            GradesList.Add(vm);
                //        }
                //    }
                //}
                for (Stage s = from; s <= to; s++)
                {
                    GradesViewModel vm = new GradesViewModel();
                    vm.Year = s.Year;
                    vm.Month = s.Month;
                    vm.Half = s.Half;
                    foreach (var course in courses)
                    {
                        var grade = grades.Where(g => g.Year == vm.Year && g.Month == s.Half && g.Half == vm.Half && g.CourseID == course.CourseID).FirstOrDefault();
                        vm.Grades.Add((grade != null) ? grade.Grade : -1);
                    }
                    GradesList.Add(vm);
                }

            }


        }


    }


    public class Stage
    {
        public Stage(int year, int month, int half)
        {
            this.Year = year;
            this.Month = month;
            this.Half = half;
        }
        public int Year { get; set; }
        public int Month { get; set; }
        public int Half { get; set; }

        public static Stage operator ++(Stage stage)
        {
            stage.Half++;
            if (stage.Half > 2)
            {
                stage.Half = 1;
                stage.Month++;
                if (stage.Month > 12)
                {
                    stage.Month = 1;
                    stage.Year++;
                }
            }
            return stage;
        }

        public static bool operator <(Stage st1, Stage st2)
        {
            return (st1.Year < st2.Year) || ((st1.Year == st2.Year) && st1.Month < st2.Month) || ((st1.Month == st2.Month) && st1.Half < st2.Half);
        }

        public static bool operator >(Stage st1, Stage st2)
        {
            return !(st1 < st2) && !st1.Equals(st2);
        }

        public static bool operator <=(Stage st1, Stage st2)
        {
            return (st1 < st2) || st1.Equals(st2);
        }

        public static bool operator >=(Stage st1, Stage st2)
        {
            return !(st1 < st2);
        }

        public override bool Equals(object obj)
        {
            Stage stage = obj as Stage;
            return (stage != null) && (stage.Year == Year) && (stage.Month == Month) && (stage.Half) == Half;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}