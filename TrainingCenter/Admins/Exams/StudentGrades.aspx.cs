﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TrainingCenter.Models;
using Microsoft.AspNet.FriendlyUrls;
using System.Data.Entity;
using System.Web.ModelBinding;
using Microsoft.AspNet.FriendlyUrls.ModelBinding;

namespace TrainingCenter.Admins.Exams
{
    public partial class StudentGrades : System.Web.UI.Page
    {
        protected TrainingCenter.Models.ApplicationDbContext _db = new TrainingCenter.Models.ApplicationDbContext();
        int month, year, half;
        int courseId;
        Exam exam;

        protected void Page_Load(object sender, EventArgs e)
        {
            // redirect to default page if the required parameters are not provided
            if (FriendlyUrl.Segments.Count != 4)
            {
                Response.Redirect("~/Admins/Exams/Default");
                return;
            }

            if (!int.TryParse(FriendlyUrl.Segments[0], out year) || !int.TryParse(FriendlyUrl.Segments[1], out month)
                 || !int.TryParse(FriendlyUrl.Segments[2], out half) || !int.TryParse(FriendlyUrl.Segments[3], out courseId))
            {
                Response.Redirect("~/Admins/Exams/Default");
                return;
            }

            // redirect to default page if exam is not found
            exam = _db.Exams.Where(ee => ee.Year == year && ee.Month == month &&
                ee.Half == half && ee.CourseID == courseId).Include(ee => ee.Course)
                .Include(ee => ee.Course.SchoolYear).FirstOrDefault();
            if (exam == null)
            {
                Response.Redirect("~/Admins/Exams/Default");
                return;
            }

            // fill form controls with exam data
            if (!IsPostBack)
            {
                yearLabel.Text = exam.Year.ToString();
                monthLabel.Text = exam.GetMonth();
                halfLabel.Text = exam.GetHalf();
                courseLabel.Text = exam.Course.CourseName + " - " + exam.Course.SchoolYear.Name;
                fullMarkLabel.Text = exam.FullMark.ToString();

                //var subscriptions = _db.StudentSubscriptions.Where(subscription =>
                //    subscription.Course.Exams.Any(ex => ex.Half == exam.Half && ex.Month == exam.Month &&
                //    ex.Year == exam.Year)).Include(subscription => subscription.ApplicationUser);
            }
        }

        // The return type can be changed to IEnumerable, however to support
        // paging and sorting, the following parameters must be added:
        //     int maximumRows
        //     int startRowIndex
        //     out int totalRowCount
        //     string sortByExpression
        public IQueryable<TrainingCenter.Models.StudentGrade> gradesRepeater_GetData([FriendlyUrlSegments(0)]int? year,
            [FriendlyUrlSegments(1)]int? month, [FriendlyUrlSegments(2)]int? half, [FriendlyUrlSegments(3)]int? courseId)
        {
            return _db.StudentGrades.Where(g => g.Year == year && g.Month == month &&
                    g.CourseID == courseId && g.Half == half).Include(s => s.ApplicationUser);
        }

        // save the entered grades in the database
        protected void Save_Click(object sender, EventArgs e)
        {
            try
            {
                // iterate over GradesList rows
                for (int i = 0; i < GradesList.Rows.Count; i++)
                {
                    // extract info from the current row
                    string studentId = ((HiddenField)GradesList.Rows[i].FindControl("studentId")).Value;
                    int grade = int.Parse(((TextBox)GradesList.Rows[i].FindControl("gradeTxt")).Text);

                    // get current student grade
                    StudentGrade studentGrade = _db.StudentGrades.Where(g => g.StudentID == studentId && g.Year == year
                        && g.Month == month && g.Half == half && g.CourseID == courseId).First();
                    
                    // update student grade with the new value
                    studentGrade.Grade = grade;
                }

                // update database
                _db.SaveChanges();

                // navigate back to default page
                Response.Redirect("~/Admins/Exams/Default");
            }
            catch (Exception ex) { errorMessage.Text = "An error occured while updating grades!" +
                Environment.NewLine + ex.Message; }
        }

        protected void updateStudentsListBtn_Click(object sender, EventArgs e)
        {
            // current grades in the database
            var curGrades = _db.StudentGrades.Where(g => g.Year == year && g.Month == month &&
                    g.CourseID == courseId && g.Half == half).Include(s => s.ApplicationUser).ToList();

            // subscriptions for the current exam
            var subscriptions = _db.StudentSubscriptions.Where(subscription =>
                subscription.Course.Exams.Any(ex => ex.Half == exam.Half && ex.Month == exam.Month &&
                ex.Year == exam.Year && ex.CourseID == courseId)).Include(subscription => subscription.ApplicationUser).ToList();

            // add initial grades for the exam
            foreach (var subscription in subscriptions)
            {
                if (curGrades.Where(g => g.StudentID == subscription.StudentID).Count() == 0)
                {
                    _db.StudentGrades.Add(new StudentGrade()
                    {
                        StudentID = subscription.ApplicationUser.Id,
                        Month = exam.Month,
                        Half = exam.Half,
                        Year = exam.Year,
                        CourseID = exam.CourseID,
                        Grade = -1 // initial grade (means ungraded)
                    });
                }
            }

            // save new grades
            _db.SaveChanges();
        }

    }
}