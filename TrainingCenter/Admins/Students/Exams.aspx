﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Exams.aspx.cs" Inherits="TrainingCenter.Admins.Students.Exams" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <asp:Repeater runat="server" ID="Repeater">
        <ItemTemplate>
            <h4><asp:Label runat="server" Text='<%# Eval("CourseTitle") %>' /></h4>

            <asp:ListView runat="server" ID="GradesList">
                <LayoutTemplate>
                    <table>
                        <thead>
                            <tr>
                                <th>Exam</th>
                                <th>Grade</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr runat="server" id="itemPlaceholder" />
                        </tbody>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <td><asp:Label runat="server" Text='<%# Eval("ExamName") %>' /></td>
                    <td><asp:Label runat="server" Text='<%# Eval("ExamGrade") %>' /></td>
                </ItemTemplate>
            </asp:ListView>

        </ItemTemplate>
    </asp:Repeater>

</asp:Content>
