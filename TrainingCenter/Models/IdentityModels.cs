﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using TrainingCenter.Models;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections;
using System.Collections.Generic;

namespace TrainingCenter.Models
{
    // You can add User data for the user by adding more properties to your User class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            StudentGrades = new List<StudentGrade>();
        }

        public ClaimsIdentity GenerateUserIdentity(ApplicationUserManager manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = manager.CreateIdentity(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        public Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager manager)
        {
            return Task.FromResult(GenerateUserIdentity(manager));
        }

        [Display(Name="الإسم الأول")]
        [Required]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "إسم العائلة")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "رقم التليفون")]
        public string Phone { get; set; }

        [ScaffoldColumn(false)]
        [ForeignKey("SchoolYear")]
        public int? SchoolYearID { get; set; }   // this property is for students only.

        public virtual SchoolYear SchoolYear { get; set; }

        public bool? GeneralType { get; set; }

        public string SchoolName { get; set; }

        public DateTime? BirthDate { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string FatherPhoneNumber { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string MotherPhoneNumber { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Mobile1 { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string Mobile2 { get; set; }

        public string Notes { get; set; }

        public bool? IsStudentActive { get; set; }

        [ScaffoldColumn(false)]
        public string CustomRole { get; set; }


        public virtual ICollection<StudentGrade> StudentGrades { get; set; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("TrainingCenterLocalDb", throwIfV1Schema: false)
        {
            this.Database.CreateIfNotExists();
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<SchoolYear> SchoolYears { get; set; }

        public DbSet<Absence> Absences { get; set; }

        public DbSet<Course> Courses { get; set; }
        public DbSet<Exam> Exams { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Salary> Salaries { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<StudentGrade> StudentGrades { get; set; }
        public DbSet<StudentSubscription> StudentSubscriptions { get; set; }
        //public DbSet<TeacherAbsence> TeacherAbsences { get; set; }

        public DbSet<TeacherDiscount> Discounts { get; set; }
    }
}

#region Helpers
namespace TrainingCenter
{
    public static class IdentityHelper
    {
        // Used for XSRF when linking external logins
        public const string XsrfKey = "XsrfId";

        public static void SignIn(ApplicationUserManager manager, ApplicationUser user, bool isPersistent)
        {
            IAuthenticationManager authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = manager.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
            authenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        public const string ProviderNameKey = "providerName";
        public static string GetProviderNameFromRequest(HttpRequest request)
        {
            return request.QueryString[ProviderNameKey];
        }

        public const string CodeKey = "code";
        public static string GetCodeFromRequest(HttpRequest request)
        {
            return request.QueryString[CodeKey];
        }

        public const string UserIdKey = "userId";
        public static string GetUserIdFromRequest(HttpRequest request)
        {
            return HttpUtility.UrlDecode(request.QueryString[UserIdKey]);
        }

        public static string GetResetPasswordRedirectUrl(string code, HttpRequest request)
        {
            var absoluteUri = "/Account/ResetPassword?" + CodeKey + "=" + HttpUtility.UrlEncode(code);
            return new Uri(request.Url, absoluteUri).AbsoluteUri.ToString();
        }

        public static string GetUserConfirmationRedirectUrl(string code, string userId, HttpRequest request)
        {
            var absoluteUri = "/Account/Confirm?" + CodeKey + "=" + HttpUtility.UrlEncode(code) + "&" + UserIdKey + "=" + HttpUtility.UrlEncode(userId);
            return new Uri(request.Url, absoluteUri).AbsoluteUri.ToString();
        }

        private static bool IsLocalUrl(string url)
        {
            return !string.IsNullOrEmpty(url) && ((url[0] == '/' && (url.Length == 1 || (url[1] != '/' && url[1] != '\\'))) || (url.Length > 1 && url[0] == '~' && url[1] == '/'));
        }

        public static void RedirectToReturnUrl(string returnUrl, HttpResponse response)
        {
            if (!String.IsNullOrEmpty(returnUrl) && IsLocalUrl(returnUrl))
            {
                response.Redirect(returnUrl);
            }
            else
            {
                response.Redirect("~/");
            }
        }
    }
}
#endregion
