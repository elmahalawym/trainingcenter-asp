﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrainingCenter.Models
{
    public class StudentGrade
    {
        [Key, Column(Order = 0)]
        [ForeignKey("ApplicationUser")]
        public string StudentID { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }

        //[Key, Column(Order = 1)]
        //[ForeignKey("Exam")]
        //public int ExamID { get; set; }
        //public virtual Exam Exam { get; set; }

        [Range(1, 12)]
        [Key, Column(Order = 1)]
        [ForeignKey("Exam")]
        public int Month { get; set; }

        [Range(1, 2)]
        [Key, Column(Order = 2)]
        [ForeignKey("Exam")]
        public int Half { get; set; }

        [Key, Column(Order = 3)]
        [ForeignKey("Exam")]
        public int Year { get; set; }

        [Key, Column(Order = 4)]
        [ForeignKey("Exam")]
        public int CourseID { get; set; }

        public virtual Exam Exam { get; set; }

        public int Grade { get; set; }
    }
}