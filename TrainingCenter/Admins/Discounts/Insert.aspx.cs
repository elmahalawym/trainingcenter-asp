﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using TrainingCenter.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TrainingCenter.Admins.Discounts
{
    public partial class Insert : System.Web.UI.Page
    {
        protected TrainingCenter.Models.ApplicationDbContext _db = new TrainingCenter.Models.ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // fill teachers drop down
                var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_db));
                string teachersRoleId = RoleManager.FindByName("teacher").Id;

                var teachers = _db.Users.Where(u => u.Roles.Any(r => r.RoleId == teachersRoleId)).ToList();
                teachersDropDown.DataSource = teachers;
                teachersDropDown.DataBind();

                // bind discount type dropdown
                var discountTypes = Enum.GetValues(typeof(DiscountTypes));
                var type = typeof(DiscountTypes);
                foreach (var item in discountTypes)
                {
                    var displayInfo = type.GetMember(((DiscountTypes)item).ToString());
                    var attributes = displayInfo[0].GetCustomAttributes(typeof(DisplayAttribute), false);
                    var displayName = ((DisplayAttribute)attributes[0]).Name;
                    discountTypeDropDown.Items.Add(new ListItem()
                    {
                        Text = displayName,
                        Value = ((int)item).ToString()
                    });
                }
                discountTypeDropDown.DataBind();
            }

        }

        // This is the Insert method to insert the entered TeacherDiscount item
        // USAGE: <asp:FormView InsertMethod="InsertItem">
        public void InsertItem()
        {
            using (_db)
            {
                var item = new TrainingCenter.Models.TeacherDiscount();

                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    // Save changes
                    _db.Discounts.Add(item);
                    _db.SaveChanges();

                    Response.Redirect("Default");
                }
            }
        }

        protected void ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Cancel", StringComparison.OrdinalIgnoreCase))
            {
                Response.Redirect("Default");
            }
        }

        protected void InsertButton_Click(object sender, EventArgs e)
        {
            if (discountCalendar.SelectedDate == DateTime.MinValue)
            {
                errorMessage.Text = "please select date"; return;
            }

            if (Page.IsValid)
            {
                try
                {
                    // load form values
                    TeacherDiscount discount = new TeacherDiscount();
                    discount.TeacherID = teachersDropDown.SelectedValue;
                    discount.DiscountDate = discountCalendar.SelectedDate;

                    discount.DiscountType = (DiscountTypes)int.Parse(discountTypeDropDown.SelectedValue);
                    discount.Amount = double.Parse(amountTxt.Text);
                    discount.Notes = notesTxt.Text;

                    // add values to database
                    _db.Discounts.Add(discount);
                    _db.SaveChanges();

                    // redirect the user to the default page
                    Response.Redirect("~/Admins/Discounts/Default");

                }
                catch (Exception) { errorMessage.Text = "an error occured"; }
            }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admins/Discounts/Default");
        }

    }
}
