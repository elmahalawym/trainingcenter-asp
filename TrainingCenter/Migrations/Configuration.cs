﻿namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using TrainingCenter.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity.Owin;

    internal sealed class Configuration : DbMigrationsConfiguration<TrainingCenter.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(TrainingCenter.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            CreateAdminUser(context);
            AddSchoolYears(context);
        }

        private void CreateAdminUser(ApplicationDbContext context)
        {
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            // role names
            string adminRoleName = "admin";
            string studentsRoleName = "student";
            string teacherRoleName = "teacher";

            // create roles if not exists
            if (!RoleManager.RoleExists(adminRoleName))
                RoleManager.Create(new IdentityRole(adminRoleName));

            if (!RoleManager.RoleExists(studentsRoleName))
                RoleManager.Create(new IdentityRole(studentsRoleName));

            if (!RoleManager.RoleExists(teacherRoleName))
                RoleManager.Create(new IdentityRole(teacherRoleName));
            
            // edit role IDs ... INVALID
            //context.Roles.Where(r => r.Name == "student").First().Id = STUDENT_ROLE_ID;
            //context.Roles.Where(r => r.Name == "teacher").First().Id = TEACHER_ROLE_ID;
            //context.SaveChanges();

            string userName = "admin";
            string password = "fE93#8@1^9";

            if (UserManager.Users.Where(u => u.UserName == userName).Count() == 0)
            {
                // create default admin user
                ApplicationUser user = new ApplicationUser();
                user.UserName = userName;
                user.FirstName = "default";
                user.LastName = "admin";
                user.Phone = "111111111";
                user.Email = "a7mad.fouda@yahoo.com";

                // create admin user
                UserManager.Create(user, password);

                // add user to admin role
                UserManager.AddToRole(user.Id, adminRoleName);
            }
        }

        private void AddSchoolYears(ApplicationDbContext context)
        {
            if (context.SchoolYears.Count() == 0) // if no data exists
            {
                context.SchoolYears.AddOrUpdate(s => s.Name, new SchoolYear()
                {
                    Name = "الصف الأول الإبتدائي"
                });
                context.SchoolYears.AddOrUpdate(s => s.Name, new SchoolYear()
                {
                    Name = "الصف الثاني الإبتدائي",
                });
                context.SchoolYears.AddOrUpdate(s => s.Name, new SchoolYear()
                {
                    Name = "الصف الثالث الإبتدائي",
                });
                context.SchoolYears.AddOrUpdate(s => s.Name, new SchoolYear()
                {
                    Name = "الصف الرابع الإبتدائي",
                });
                context.SchoolYears.AddOrUpdate(s => s.Name, new SchoolYear()
                {
                    Name = "الصف الخامس الإبتدائي",
                });
                context.SchoolYears.AddOrUpdate(s => s.Name, new SchoolYear()
                {
                    Name = "الصف السادس الإبتدائي",
                });
                context.SchoolYears.AddOrUpdate(s => s.Name, new SchoolYear()
                {
                    Name = "الصف الأول الإعدادي",
                });
                context.SchoolYears.AddOrUpdate(s => s.Name, new SchoolYear()
                {
                    Name = "الصف الثاني الإعدادي",
                });
                context.SchoolYears.AddOrUpdate(s => s.Name, new SchoolYear()
                {
                    Name = "الصف الثالث الإعدادي",
                });
                context.SchoolYears.AddOrUpdate(s => s.Name, new SchoolYear()
                {
                    Name = "الصف الأول الثانوي",
                });
                context.SchoolYears.AddOrUpdate(s => s.Name, new SchoolYear()
                {
                    Name = "الصف الثاني الثانوي",
                });
                context.SchoolYears.AddOrUpdate(s => s.Name, new SchoolYear()
                {
                    Name = "الصف الثالث الثانوي",
                });
            }


        }

    }
}
