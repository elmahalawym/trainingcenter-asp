﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using TrainingCenter.Models;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TrainingCenter
{
    public static class Helpers
    {
        #region Properites

        public static string PricesFileName
        {
            get { return "Prices.xml"; }
        }

        public static string NumberOfStudentsFileName
        {
            get { return "NumberOfStudents.xml"; }
        }

        public static string AbsoluteNumbersFileName
        {
            get { return "AbsoluteNumbers.xml"; }
        }


        #endregion

        #region HMonthList

        public static string GetHMonthFileName(int year, int month)
        {
            return string.Format("HMonthIDs_{0}_{1}.xml", year, month);
        }

        public static List<string> GetHMonthList(string filePath)
        {
            try
            {
                if (!File.Exists(filePath))
                    return new List<string>();

                using (FileStream fs = new FileStream(filePath, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<string>));
                    return serializer.Deserialize(fs) as List<string>;
                }
            }
            catch (Exception) { return null; }
        }

        public static void SetHMonthList(string filePath, List<string> newIds)
        {
            // save file
            try
            {
                // delete file if exists
                if (File.Exists(filePath))
                    File.Delete(filePath);

                using (FileStream fs = new FileStream(filePath, FileMode.CreateNew))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<string>));
                    serializer.Serialize(fs, newIds);
                }
            }
            catch (Exception) { }
        }

        public static void AddIdToHMonthList(string filePath, string id)
        {
            try
            {
                // get current ids
                List<string> ids = GetHMonthList(filePath);

                // add new id if not already exists
                if (!ids.Contains(id))
                    ids.Add(id);

                // save new list
                SetHMonthList(filePath, ids);
            }
            catch (Exception) { }
        }

        #endregion



        #region Prices

        public static Prices GetPrices(string filePath)
        {
            try
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Prices));
                    return serializer.Deserialize(fs) as Prices;
                }
            }
            catch (Exception) { return null; }
        }

        public static bool UpdatePrices(string filePath)
        {
            // get percentage and expenses
            Prices p = GetPrices(filePath);
            if (p == null)
                return false;

            return UpdatePrices(filePath, p.Percentage, p.AdministrativeExpenses);
        }

        public static bool UpdatePrices(string filePath, double percentage, double expenses)
        {
            try
            {
                // delete file if exists
                if (File.Exists(filePath))
                    File.Delete(filePath);

                using (FileStream fs = new FileStream(filePath, FileMode.CreateNew))
                {
                    Prices prices = new Prices();
                    prices.Percentage = percentage;
                    prices.AdministrativeExpenses = expenses;

                    List<SchoolYear> schoolyears = null;
                    using (ApplicationDbContext db = new ApplicationDbContext())
                        schoolyears = db.SchoolYears.ToList();

                    foreach (var schoolYear in schoolyears)
                    {
                        SchoolYearStudentValue value = new SchoolYearStudentValue();
                        value.SchoolYearId = schoolYear.SchoolYearID;
                        value.SchoolYearName = schoolYear.Name;

                        // calculate number of sessions
                        int number_of_sessions = 0;
                        using (ApplicationDbContext db = new ApplicationDbContext())
                        {
                            var courses = db.Courses.Where(c => c.SchoolYearID == schoolYear.SchoolYearID);
                            foreach (var course in courses)
                            {
                                number_of_sessions += course.NumberOfSessionsPerWeek;
                            }
                        }

                        // general student value
                        value.StudentGeneralPrice = ((schoolYear.GeneralPrice - expenses) * (percentage / 100)) / number_of_sessions;
                        value.StudentSpecialPrice = ((schoolYear.SpecialPrice - expenses) * (percentage / 100));

                        prices.SchoolyearValues.Add(value);
                    }

                    XmlSerializer serializer = new XmlSerializer(typeof(Prices));
                    serializer.Serialize(fs, prices);
                    return true;
                }
            }
            catch (Exception) { return false; }
        }

        #endregion


        #region Number_Of_Students

        public static List<NumberOfStudents> GetNumberOfStudents(string filePath)
        {
            try
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<NumberOfStudents>));
                    return serializer.Deserialize(fs) as List<NumberOfStudents>;
                }
            }
            catch (Exception) { return null; }
        }

        public static bool UpdateNumberOfStudents(string filePath, string absoluteNumbersFilePath, string hMonthFilePath)
        {
            List<NumberOfStudents> absoluteNumbers = new List<NumberOfStudents>();
            try
            {
                // delete file if exists
                if (File.Exists(filePath))
                    File.Delete(filePath);

                List<string> hMonthList = GetHMonthList(hMonthFilePath);

                using (FileStream fs = new FileStream(filePath, FileMode.CreateNew))
                {
                    List<NumberOfStudents> numberOfStudents = new List<NumberOfStudents>();
                    List<Course> courses;
                    using (ApplicationDbContext db = new ApplicationDbContext())
                        courses = db.Courses.ToList();

                    foreach (var c in courses)
                    {
                        using (ApplicationDbContext db = new ApplicationDbContext())
                        {
                            var course = db.Courses.Where(cc => cc.CourseID == c.CourseID).First();
                            NumberOfStudents value = new NumberOfStudents();
                            value.SchoolYearId = course.SchoolYearID;
                            value.SchoolYearName = course.SchoolYear.Name;

                            value.NumberOfStudents_General = 0.0d;
                            value.NumberOfStudents_Special = 0.0d;

                            var generalSubscriptions = course.StudentSubscriptions.Where(subscription =>
                                (bool)subscription.ApplicationUser.IsStudentActive &&
                                (bool)subscription.ApplicationUser.GeneralType).ToList();
                            foreach (var subscription in generalSubscriptions)
                                value.NumberOfStudents_General += hMonthList.Contains(subscription.StudentID) ? 0.5d : 1.0d;

                            var specialSubscriptions = course.StudentSubscriptions.Where(subscription =>
                                (bool)subscription.ApplicationUser.IsStudentActive &&
                                (bool)!subscription.ApplicationUser.GeneralType).ToList();
                            foreach (var subscription in specialSubscriptions)
                                value.NumberOfStudents_Special += hMonthList.Contains(subscription.StudentID) ? 0.5d : 1.0d;

                            // Absolute number calculations (full numbers without half month calculations
                            NumberOfStudents absValue = new NumberOfStudents();
                            absValue.SchoolYearId = course.SchoolYearID;
                            absValue.SchoolYearName = course.SchoolYear.Name;
                            absValue.NumberOfStudents_General = (double)course.StudentSubscriptions.Where(subscription =>
                                (bool)subscription.ApplicationUser.IsStudentActive &&
                                (bool)subscription.ApplicationUser.GeneralType).Count();
                            absValue.NumberOfStudents_Special = (double)course.StudentSubscriptions.Where(subscription =>
                                (bool)subscription.ApplicationUser.IsStudentActive &&
                                (bool)!subscription.ApplicationUser.GeneralType).Count();

                            value.CourseName = course.CourseName;
                            numberOfStudents.Add(value);
                            absoluteNumbers.Add(absValue);
                        }
                    }

                    XmlSerializer serializer = new XmlSerializer(typeof(List<NumberOfStudents>));
                    serializer.Serialize(fs, numberOfStudents);
                }
            }
            catch (Exception) { return false; }

            // save absolute values
            if (File.Exists(absoluteNumbersFilePath))
                File.Delete(absoluteNumbersFilePath);

            try
            {
                using (FileStream fs = new FileStream(absoluteNumbersFilePath, FileMode.CreateNew))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<NumberOfStudents>));
                    serializer.Serialize(fs, absoluteNumbers);

                }
            }
            catch (Exception) { return false; }
            return true;
        }

        #endregion


        #region Salaries

        public static void CalculateSalaries(string pricesFilePath, string NumberOfStudentsFilePath, string filePath, int year, int month, double factor = 1.0)
        {
            List<SalaryDetails> Salaries = new List<SalaryDetails>();

            // get prices
            Prices prices = GetPrices(pricesFilePath);

            // get number of students
            List<NumberOfStudents> numberOfStudentsList = GetNumberOfStudents(NumberOfStudentsFilePath);

            // get teachers
            List<ApplicationUser> teachers = null;
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(ctx));
                string teachersRoleId = RoleManager.FindByName("teacher").Id;

                teachers = ctx.Users.Where(u => u.Roles.Any(r => r.RoleId == teachersRoleId)).ToList();

                ctx.Dispose();
            }

            foreach (ApplicationUser teacher in teachers)
            {
                // calculate salary details for current teacher
                SalaryDetails sDetails = new SalaryDetails();
                sDetails.TeacherId = teacher.Id;
                sDetails.TeacherFullName = string.Format("{0} {1}", teacher.FirstName, teacher.LastName);
                sDetails.TeacherUsername = teacher.UserName;
                sDetails.Total = 0.0d;
                sDetails.Factor = factor;

                // get courses for current teacher
                List<Course> courses = null;
                using (ApplicationDbContext db = new ApplicationDbContext())
                {
                    courses = db.Courses.Where(c => c.TeacherID == teacher.Id).Include(c => c.SchoolYear).ToList();
                }

                // calculate salary unit for each course
                foreach (Course course in courses)
                {
                    try
                    {
                        SalaryUnit sUnit = new SalaryUnit();
                        sUnit.CourseId = course.CourseID;
                        sUnit.CourseName = string.Format("{0} - {1}", course.CourseName, course.SchoolYear.Name);

                        //general price
                        double generalPrice = prices.SchoolyearValues.Where(p => p.SchoolYearId == course.SchoolYearID).First().StudentGeneralPrice;

                        // special price
                        double specialPrice = prices.SchoolyearValues.Where(p => p.SchoolYearId == course.SchoolYearID).First().StudentSpecialPrice;

                        NumberOfStudents nStudents = numberOfStudentsList.Where(l => l.SchoolYearId == course.SchoolYearID).First();
                        // number of general students
                        double nGeneralStudents = nStudents.NumberOfStudents_General;

                        // number of special students
                        double nSpecialStudents = nStudents.NumberOfStudents_Special;

                        // total
                        double sUnitTotal = (generalPrice * nGeneralStudents) + (specialPrice * nSpecialStudents);

                        // add details
                        sUnit.NStudentsGeneral = nGeneralStudents;
                        sUnit.NStudentsSpecial = nSpecialStudents;
                        sUnit.GeneralStudentPrice = generalPrice;
                        sUnit.SpecialStudentPrice = specialPrice;
                        sUnit.Total = sUnitTotal;
                        sDetails.SalaryUnits.Add(sUnit);
                        sDetails.Total += sUnitTotal;
                    }
                    catch (Exception) { }
                }

                // multiply total by factor
                sDetails.Total *= factor;

                // add salary details
                Salaries.Add(sDetails);
            }

            // Update Salaries in Database
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                foreach (var salaryDetails in Salaries)
                {
                    Salary salary = db.Salaries.Where(s => s.Year == year && s.Month == month && s.TeacherID == salaryDetails.TeacherId).FirstOrDefault();
                    if (salary != null) // update current values
                    {
                        salary.Price = salaryDetails.Total;
                    }
                    else
                    {
                        // add new salary
                        salary = new Salary();
                        salary.Price = salaryDetails.Total;
                        salary.Year = year;
                        salary.Month = month;
                        salary.TeacherID = salaryDetails.TeacherId;
                        salary.IsPaid = false;
                        salary.PaidAmount = 0.0d;
                        db.Salaries.Add(salary);
                    }
                }

                // save changes to database
                db.SaveChanges();
            }

            // save file
            try
            {
                // delete file if exists
                if (File.Exists(filePath))
                    File.Delete(filePath);

                using (FileStream fs = new FileStream(filePath, FileMode.CreateNew))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<SalaryDetails>));
                    serializer.Serialize(fs, Salaries);
                }
            }
            catch (Exception) { }

        }//function

        public static List<SalaryDetails> GetSalaries(string filePath)
        {
            try
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<SalaryDetails>));
                    return serializer.Deserialize(fs) as List<SalaryDetails>;
                }
            }
            catch (Exception) { return null; }
        }


        public static string GetSalariesFileName(int year, int month)
        {
            return string.Format("Salaries_{0}_{1}.xml", year, month);
        }

        #endregion


        #region Invoices

        public static List<InvoiceXmlModel> GetInvoices(string filePath)
        {
            try
            {
                using (FileStream fs = new FileStream(filePath, FileMode.Open))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<InvoiceXmlModel>));
                    return serializer.Deserialize(fs) as List<InvoiceXmlModel>;
                }
            }
            catch (Exception) { return null; }
        }

        public static void CalculateInvoices(string filePath)
        {
            // get students
            List<ApplicationUser> students;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
                string studentsRoleId = RoleManager.FindByName("student").Id;

                students = db.Users.Where(u => u.Roles.Any(r => r.RoleId == studentsRoleId) && u.IsStudentActive == true).Include(s => s.SchoolYear).Include(s => s.StudentGrades).ToList();

                db.Dispose();
            }

            List<InvoiceXmlModel> invoices = new List<InvoiceXmlModel>();

            foreach (ApplicationUser student in students)
            {
                InvoiceXmlModel invoice = new InvoiceXmlModel();
                invoice.StudentID = student.Id;
                invoice.StudentName = string.Format("{0} {1}", student.FirstName, student.LastName);

                if ((bool)student.GeneralType)
                {
                    invoice.Value = student.SchoolYear.GeneralPrice;
                }
                else
                {
                    int numberOfCourses = student.StudentGrades.Count;
                    invoice.Value = numberOfCourses * student.SchoolYear.SpecialPrice;
                }

                // add invoice
                invoices.Add(invoice);
            }

            // save invoices
            try
            {
                // delete file if exists (overwrite)
                if (File.Exists(filePath))
                    File.Delete(filePath);

                using (FileStream fs = new FileStream(filePath, FileMode.CreateNew))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(List<InvoiceXmlModel>));
                    serializer.Serialize(fs, invoices);
                }
            }
            catch (Exception) { }
        }

        public static void CalculateInvoices(int year, int month, string hMonthFilePath, bool halfMonth = false)
        {
            List<string> hMonthIds = GetHMonthList(hMonthFilePath);
            // get students
            List<ApplicationUser> students;
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
                string studentsRoleId = RoleManager.FindByName("student").Id;

                students = db.Users.Where(u => u.Roles.Any(r => r.RoleId == studentsRoleId) && u.IsStudentActive == true).Include(s => s.SchoolYear).Include(s => s.StudentGrades).ToList();

                db.Dispose();
            }

            List<Invoice> newInvoices = new List<Invoice>();
            List<Invoice> invoicesInDb;

            foreach (ApplicationUser student in students)
            {
                Invoice invoice = new Invoice();
                invoice.StudentID = student.Id;
                invoice.Month = month;
                invoice.Year = year;
                invoice.Paid = false;

                if ((bool)student.GeneralType)
                {
                    invoice.Price = student.SchoolYear.GeneralPrice;
                }
                else
                {
                    int numberOfCourses = student.StudentGrades.Count;
                    invoice.Price = numberOfCourses * student.SchoolYear.SpecialPrice;
                }

                // divide price by 2 if invoices are calculated for half month or if the user in registered in the half month list
                if (halfMonth || hMonthIds.Contains(student.Id))
                    invoice.Price /= 2;

                // add invoice
                newInvoices.Add(invoice);
            }

            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                invoicesInDb = db.Invoices.Where(i => i.Year == year && i.Month == month).ToList();
            }

            // add invoices to DB
            using (ApplicationDbContext db = new ApplicationDbContext())
            {
                foreach (Invoice invoice in newInvoices)
                {
                    if (invoicesInDb.Where(i => i.StudentID == invoice.StudentID).Count() > 0) // invoice already exists
                    {
                        // update existing invoice
                        db.Invoices.Where(i => i.StudentID == invoice.StudentID).First().Price = invoice.Price;
                    }
                    else
                        db.Invoices.Add(invoice);
                }
                db.SaveChanges();
            }
        }

        public static string GetInvoicesFileName(int year, int month)
        {
            return string.Format("Invoices_{0}_{1}.xml", year, month);
        }

        #endregion

        public static string GetMonthText(int idx)
        {
            switch(idx)
            {
                case 1:
                    return "يناير";
                case 2:
                    return "فبراير";
                case 3:
                    return "مارس";
                case 4:
                    return "إبريل";
                case 5:
                    return "مايو";
                case 6:
                    return "يونيو";
                case 7:
                    return "يوليو";
                case 8:
                    return "أغسطس";
                case 9:
                    return "سبتمبر";
                case 10:
                    return "أكتوبر";
                case 11:
                    return "نوفمبر";
                case 12:
                    return "ديسمبر";
                default:
                    return "";
            }
        }

    }//class
}//namespace