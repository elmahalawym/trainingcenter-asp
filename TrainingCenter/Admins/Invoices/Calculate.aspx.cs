﻿/*
 * Invoices Calculation Page
 * 
 * 
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace TrainingCenter.Admins.Invoices
{
    public partial class Calculate : System.Web.UI.Page
    {
        //double factor;
        protected void Page_Load(object sender, EventArgs e)
        {
            //set default values
            if (!IsPostBack)
            {
                yearTxt.Text = DateTime.Now.Year.ToString();
                monthDropDown.SelectedIndex = DateTime.Now.Month;
            }
            //else
            //{
            //    if (!double.TryParse(factorTxt.Text, out factor) || factor < 0.0 || factor > 1.0)
            //    {
            //        factorError.Text = "factor must be between 0.0 and 1.0";
            //        return;
            //    }
            //}
        }

        protected void calculateBtn_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                int year = int.Parse(yearTxt.Text);

                string hMonthFilePath = Server.MapPath("~/App_Data/" + Helpers.GetHMonthFileName(year, monthDropDown.SelectedIndex));

                // calculate invoices
                if(curMonthSubType.SelectedIndex == 0) // full month
                {
                    Helpers.CalculateInvoices(year, monthDropDown.SelectedIndex, hMonthFilePath);
                }
                else // half month
                {
                    Helpers.CalculateInvoices(year, monthDropDown.SelectedIndex, hMonthFilePath, true);
                }
                

                // redirect to default page
                Response.Redirect("~/admins/invoices/default");
            }
        }

    }
}