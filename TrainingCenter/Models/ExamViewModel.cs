﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrainingCenter.Models
{
    public class ExamViewModel
    {
        public int CourseId { get; set; }

        public string CourseName { get; set; }
        
        public bool IsActive { get; set; }

        public int FullMark { get; set; }

    }
}