﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using Microsoft.AspNet.FriendlyUrls.ModelBinding;
using TrainingCenter.Models;
using Microsoft.AspNet.FriendlyUrls;
namespace TrainingCenter.Admins.TeacherViewModels
{
    public partial class Edit : System.Web.UI.Page
    {
        private ApplicationDbContext _db = new ApplicationDbContext();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (FriendlyUrl.Segments.Count > 0)
                {
                    string userId = FriendlyUrl.Segments[0];

                    ApplicationUser user = _db.Users.Where(u => u.Id == userId).FirstOrDefault();

                    // populate from controls with user data
                    if (user != null)
                    {
                        // fill user data
                        usernameTxt.Text = user.UserName;
                        emailTxt.Text = user.Email;
                        firstNameTxt.Text = user.FirstName;
                        lastNameTxt.Text = user.LastName;
                        phoneTxt.Text = user.Phone;
                    }
                    else
                    {
                        ErrorMessage.Text = "عفوا بيانات المدرس غير موجودة";
                        editBtn.Enabled = false;
                    }
                }
                else
                {
                    Response.Redirect("~/Admins/Teachers/Default");
                }
            }
        }

        protected void editBtn_Click(object sender, EventArgs e)
        {
            if (FriendlyUrl.Segments.Count > 0)
            {
                string userId = FriendlyUrl.Segments[0];

                ApplicationUser user = _db.Users.Where(u => u.Id == userId).FirstOrDefault();
                if (user != null)
                {
                    user.UserName = usernameTxt.Text;
                    user.Email = emailTxt.Text;
                    user.FirstName = firstNameTxt.Text;
                    user.LastName = lastNameTxt.Text;
                    user.Phone = phoneTxt.Text;
                    try
                    {
                        _db.SaveChanges();
                    }catch(Exception ex)
                    {
                        ErrorMessage.Text = "an error occured white updating teacher's data";
                        return;
                    }
                    
                }
            }
            Response.Redirect("~/Admins/Teachers/Default");
        }
    }
}
