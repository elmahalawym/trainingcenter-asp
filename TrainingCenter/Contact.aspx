﻿<%@ Page Title="اتصل بنا" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="TrainingCenter.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="right-align">
        <h2><%: Title %>.</h2>
        <h3>Your contact page.</h3>
        <address>
            العنوان<br />
            شششش<br />
            <abbr title="Phone">P:</abbr>
            01212121212
        </address>

        <address>
            <strong>Support:</strong>   <a href="mailto:Support@example.com">Support@example.com</a><br />
            <strong>Marketing:</strong> <a href="mailto:Marketing@example.com">Marketing@example.com</a>
        </address>

    </div>
    
    <div>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3427.279777722784!2d31.00807950000001!3d30.794787500000005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14f7c9748f3542dd%3A0x3149d061a8a3db4f!2sRoushdy%2C+Tanta+Qism+2%2C+Tanta%2C+Gharbia!5e0!3m2!1sen!2seg!4v1415913786331" width="400" height="300" frameborder="0" style="border:0"></iframe>
    </div>
</asp:Content>
