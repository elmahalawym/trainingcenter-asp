﻿<%@ Page Title="CourseList" Language="C#" MasterPageFile="~/Admins/Admins.master" CodeBehind="Default.aspx.cs" Inherits="TrainingCenter.Admins.Courses.Default" %>

<%@ Register TagPrefix="FriendlyUrls" Namespace="Microsoft.AspNet.FriendlyUrls" %>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">

<%--    <div class="container">
        <ul class="nav nav-tabs navbar-right" role="tablist">
            <li role="presentation"><a href="/Admins/Invoices/default">المصروفات</a></li>
            <li role="presentation"><a href="/Admins/salaries/default">المرتبات</a></li>
            <li role="presentation"><a href="/Admins/Discounts/default">الخصومات</a></li>
            <li role="presentation"><a href="/Admins/Schoolyears/default">الأسعار</a></li>
            <li role="presentation"><a href="/Admins/Students/default">الطلاب</a></li>
            <li role="presentation"><a href="/Admins/Teachers/Default">المدرسين</a></li>
            <li role="presentation"><a href="/Admins/Exams/Default">الإمتحانات</a></li>
            <li role="presentation" class="active"><a href="/Admins/Courses/Default">الكورسات</a></li>
            <li role="presentation"><a href="/Admins/Default">الرئيسية</a></li>
        </ul>
    </div>--%>

    <h2>قائمة الكورسات</h2>
    <p>
        <asp:HyperLink runat="server" NavigateUrl="Insert" Text="إضافة كورس" />
    </p>
    <div class="form-inline">
        <asp:Label runat="server" AssociatedControlID="schoolYearDropdown" Text="السنة الدراسية" CssClass="control-label" />
        <asp:DropDownList runat="server" ID="schoolYearDropdown" CssClass="form-control" AutoPostBack="true"
            DataValueField="SchoolYearID" DataTextField="Name" AppendDataBoundItems="true">
            <%--<asp:ListItem Text="اختر السنة الدراسية" Value="-1" />--%>
            <asp:ListItem Text="الكل" Value="0" />
        </asp:DropDownList>

    </div>
    <br />

    <div>
        <asp:ListView ID="ListView1" runat="server"
            DataKeyNames="CourseID"
            ItemType="TrainingCenter.Models.Course"
            SelectMethod="GetData">
            <EmptyDataTemplate>
                لا توجد كورسات
            </EmptyDataTemplate>
            <LayoutTemplate>
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>
                                <asp:LinkButton Text="اسم الكورس" CommandName="Sort" CommandArgument="CourseName" runat="Server" />
                            </th>
                            <th>
                                <asp:LinkButton Text="المدرس" CommandName="Sort" CommandArgument="TeacherID" runat="Server" />
                            </th>
                            <th>
                                <asp:LinkButton Text="السنة الدراسية" CommandName="Sort" CommandArgument="SchoolYearID" runat="Server" />
                            </th>
                            <th>
                                عدد الحصص في الأسبوع
                            </th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr runat="server" id="itemPlaceholder" />
                    </tbody>
                </table>
                <asp:DataPager PageSize="10" runat="server">
                    <Fields>
                        <asp:NextPreviousPagerField ShowLastPageButton="False" ShowNextPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                        <asp:NumericPagerField ButtonType="Button" NumericButtonCssClass="btn" CurrentPageLabelCssClass="btn disabled" NextPreviousButtonCssClass="btn" />
                        <asp:NextPreviousPagerField ShowFirstPageButton="False" ShowPreviousPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                    </Fields>
                </asp:DataPager>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:DynamicControl runat="server" DataField="CourseName" ID="CourseName" Mode="ReadOnly" />
                    </td>
                    <td>
                        <a href="<%#: Item.ApplicationUser != null ? Microsoft.AspNet.FriendlyUrls.FriendlyUrl.Href("~/Admins/Teachers/Details/" + Item.ApplicationUser.Id) : "#" %>">
                            <%#: Item.ApplicationUser != null ? Item.ApplicationUser.FirstName + " " + Item.ApplicationUser.LastName : "" %>
                        </a>
                    </td>
                    <td>
                        <%#: Item.SchoolYear != null ? Item.SchoolYear.Name : "" %>
                    </td>
                    <td><%# Item.NumberOfSessionsPerWeek %></td>
                    <td>
                        <asp:HyperLink runat="server" NavigateUrl='<%# FriendlyUrl.Href("~/Admins/Courses/Details", Item.CourseID) %>' Text="Details" />
                        | 
					    <asp:HyperLink runat="server" NavigateUrl='<%# FriendlyUrl.Href("~/Admins/Courses/Edit", Item.CourseID) %>' Text="تعديل" />
                        | 
                        <asp:HyperLink runat="server" NavigateUrl='<%# FriendlyUrl.Href("~/Admins/Courses/Delete", Item.CourseID) %>' Text="حذف" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>
    </div>
</asp:Content>

