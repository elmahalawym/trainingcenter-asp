﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet;
using Microsoft.AspNet.FriendlyUrls;
using TrainingCenter.Models;
using System.Data.Entity;

namespace TrainingCenter.Admins.Invoices
{
    public partial class Edit : System.Web.UI.Page
    {
        ApplicationDbContext db = new ApplicationDbContext();
        int year, month;
        string studentId;
        Invoice invoice;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(FriendlyUrl.Segments.Count != 3)
            {
                Response.Redirect("~/Admins/Invoices/Default");
                return;
            }

            studentId = FriendlyUrl.Segments[2];
            if(!int.TryParse(FriendlyUrl.Segments[0], out year) || !int.TryParse(FriendlyUrl.Segments[1], out month))
            {
                Response.Redirect("~/Admins/Invoices/Default");
                return;
            }
            invoice = db.Invoices.Where(i => i.Year == year && i.Month == month && i.StudentID == studentId).Include(i => i.ApplicationUser).FirstOrDefault();
            if (invoice == null)
            {
                Response.Redirect("~/Admins/Invoices/Default");
                return;
            }

            if(!IsPostBack)
            {
                // populate form controls with default values
                studentNameLabel.Text = invoice.ApplicationUser.FirstName + " " + invoice.ApplicationUser.LastName;
                expensesLabel.Text = string.Format("{0:0.00}", invoice.Price);
                paidAmountTxt.Text = string.Format("{0:0.00}", invoice.PaidAmount);
            }

        }

        protected void UpdateButton_Click(object sender, EventArgs e)
        {
            try
            {
                double entry = double.Parse(paidAmountTxt.Text);
                invoice.PaidAmount = entry;
                if (entry < 1)
                    invoice.Paid = false;
                else
                    invoice.Paid = true;
                db.SaveChanges();

                Response.Redirect("~/Admins/Invoices/Default");
            }
            catch (Exception) { ErrorMessage.Text = "An error occured"; }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admins/Invoices/Default");
        }
    }
}