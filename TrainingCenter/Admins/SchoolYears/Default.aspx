﻿<%@ Page Title="SchoolYearList" Language="C#" MasterPageFile="~/Admins/Admins.master" CodeBehind="Default.aspx.cs" Inherits="TrainingCenter.SchoolYears.Default" %>

<%@ Register TagPrefix="FriendlyUrls" Namespace="Microsoft.AspNet.FriendlyUrls" %>
<asp:Content runat="server" ContentPlaceHolderID="MainContent">

    <h2>الأسعار</h2>
    <p>
        <asp:HyperLink runat="server" NavigateUrl="Insert" Text="إضافة" />
    </p>
    <div>
        <asp:ListView ID="ListView1" runat="server"
            DataKeyNames="SchoolYearID"
            ItemType="TrainingCenter.Models.SchoolYear"
            SelectMethod="GetData">
            <EmptyDataTemplate>
            لا يوجد سنوات دراسية مضافة
            </EmptyDataTemplate>
            <LayoutTemplate>
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <%--                            <th>
                                <asp:LinkButton Text="SchoolYearID" CommandName="Sort" CommandArgument="SchoolYearID" runat="Server" />
                            </th>--%>
                            <th>
                                <asp:LinkButton Text="إسم السنة الدراسية" CommandName="Sort" CommandArgument="Name" runat="Server" />
                            </th>
                            <th>
                                <asp:Label Text="السعر العام" runat="Server" />
                            </th>
                            <th>
                                <asp:Label Text="السعر الخاص" runat="Server" />
                            </th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr runat="server" id="itemPlaceholder" />
                    </tbody>
                </table>
                <asp:DataPager PageSize="3" runat="server">
                    <Fields>
                        <asp:NextPreviousPagerField FirstPageText="الأول" LastPageText="الأخير" NextPageText="التالي" PreviousPageText="السابق" ShowLastPageButton="False" ShowNextPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                        <asp:NumericPagerField ButtonType="Button" NumericButtonCssClass="btn" CurrentPageLabelCssClass="btn disabled" NextPreviousButtonCssClass="btn" />
                        <asp:NextPreviousPagerField FirstPageText="الأول" LastPageText="الأخير" NextPageText="التالي" PreviousPageText="السابق" ShowFirstPageButton="False" ShowPreviousPageButton="False" ButtonType="Button" ButtonCssClass="btn" />
                    </Fields>
                </asp:DataPager>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <%--							<td>
								<asp:DynamicControl runat="server" DataField="SchoolYearID" ID="SchoolYearID" Mode="ReadOnly" />
							</td>--%>
                    <td>
                        <asp:DynamicControl runat="server" DataField="Name" ID="Name" Mode="ReadOnly" />
                    </td>
                    <td>
                        <asp:DynamicControl runat="server" DataField="GeneralPrice" ID="GeneralPrice" Mode="ReadOnly" />
                    </td>
                    <td>
                        <asp:DynamicControl runat="server" DataField="SpecialPrice" ID="SpecialPrice" Mode="ReadOnly" />
                    </td>


                    <td>
                        <%--<asp:HyperLink runat="server" NavigateUrl='<%# FriendlyUrl.Href("~/Admins/SchoolYears/Details", Item.SchoolYearID) %>' Text="Details" />--%>

                        <asp:HyperLink runat="server" NavigateUrl='<%# FriendlyUrl.Href("~/Admins/SchoolYears/Edit", Item.SchoolYearID) %>' Text="تعديل" />
                        | 
                        <asp:HyperLink runat="server" NavigateUrl='<%# FriendlyUrl.Href("~/Admins/SchoolYears/Delete", Item.SchoolYearID) %>' Text="حذف" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:ListView>

        <br />
        <br />

        <!-- panel displaying current prices -->
        <div class="panel panel-default">
            <div class="panel-body">

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>النسبة</th>
                            <th>المصاريف الإدارية</th>
                        </tr>
                    </thead>
                    <tr>
                        <td>
                            <asp:Literal runat="server" ID="percentageTxt" /></td>
                        <td>
                            <asp:Literal runat="server" ID="expensesTxt" /></td>
                    </tr>
                </table>

                <asp:Button runat="server" ID="tempBtn" OnClick="editPrices_Click"
                    CssClass="btn btn-primary" Text="تعديل الإسعار" />
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>سعر الطالب الواحد</h3>
            </div>
            <div class="panel-body">
                <asp:ListView runat="server" ItemType="TrainingCenter.Models.SchoolYearStudentValue"
                    SelectMethod="Unnamed_GetData">
                    <LayoutTemplate>
                        <table class="table table-bordered table-hover">
                            <thead>
                                <th>السنة الدراسية</th>
                                <th>السعر العام (كل المواد)</th>
                                <th>سعر حصتين</th>
                                <th>سعر 3 حصص</th>
                                <th>السعر الخاص (المادة الواحدة)</th>
                            </thead>
                            <tbody>
                                <div id="itemPlaceHolder" runat="server"></div>
                            </tbody>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%# Item.SchoolYearName %></td>
                            <td><%# Item.StudentGeneralPrice %></td>
                            <td><%# Item.StudentGeneralPrice * 2 %></td>
                            <td><%# Item.StudentGeneralPrice * 3 %></td>
                            <td><%# Item.StudentSpecialPrice %></td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </div>



    </div>
</asp:Content>

