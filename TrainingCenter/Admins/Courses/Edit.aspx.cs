﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using Microsoft.AspNet.FriendlyUrls.ModelBinding;
using TrainingCenter.Models;
using Microsoft.AspNet.FriendlyUrls;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TrainingCenter.Admins.Courses
{
    public partial class Edit : System.Web.UI.Page
    {
        protected ApplicationDbContext _db = new ApplicationDbContext();

        int courseId = -1;
        protected void Page_Load(object sender, EventArgs e)
        {
            // redirect to default page if course id was not provided
            if (FriendlyUrl.Segments.Count == 0 || !int.TryParse(FriendlyUrl.Segments[0], out courseId))
                Response.Redirect("~/Admins/Courses/Default");

            // get selected course and navigate to default page if an invalid course id was given
            Course course = _db.Courses.Where(c => c.CourseID == courseId).FirstOrDefault();
            if (course == null)
                Response.Redirect("~/Admins/Courses/Default");

            if (!IsPostBack)
            {
                // fill form controls with course data
                courseNameTxt.Text = course.CourseName;
                schoolYearDropDown.SelectedValue = course.SchoolYearID.ToString();
                teachersDropDown.SelectedValue = course.TeacherID.ToString();
                nSessionsTxt.Text = course.NumberOfSessionsPerWeek.ToString();

                // fill schoolYearDropDown with data
                var schoolYears = _db.SchoolYears.ToList();
                schoolYearDropDown.DataSource = schoolYears;
                schoolYearDropDown.DataBind();

                // fill teachersDropDown with data
                // get teacher role Id
                var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_db));
                string teacherRoleId = RoleManager.FindByName("teacher").Id;

                var teachers = _db.Users.Where(u => u.Roles.Any(r => r.RoleId == teacherRoleId)).ToList();
                teachersDropDown.DataSource = teachers;
                teachersDropDown.DataBind();
            }
        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                try
                {
                    // get selected course
                    Course course = _db.Courses.Where(c => c.CourseID == courseId).FirstOrDefault();

                    course.CourseName = courseNameTxt.Text;
                    course.TeacherID = teachersDropDown.SelectedValue;
                    course.SchoolYearID = int.Parse(schoolYearDropDown.SelectedValue);
                    course.NumberOfSessionsPerWeek = int.Parse(nSessionsTxt.Text);

                    // save changes
                    _db.SaveChanges();
                    _db.Dispose();

                    // update prices
                    Helpers.UpdatePrices(Server.MapPath("~/App_Data/Prices.xml"));

                    Response.Redirect("~/Admins/Courses/Default");
                }
                catch (Exception)
                {
                    errorMessage.Text = "Invalid Data";
                }
            }
        }




        /*
        // This is the Update methd to update the selected Course item
        // USAGE: <asp:FormView UpdateMethod="UpdateItem">
        public void UpdateItem(int  CourseID)
        {
            using (_db)
            {
                var item = _db.Courses.Find(CourseID);

                if (item == null)
                {
                    // The item wasn't found
                    ModelState.AddModelError("", String.Format("Item with id {0} was not found", CourseID));
                    return;
                }

                TryUpdateModel(item);

                if (ModelState.IsValid)
                {
                    // Save changes here
                    _db.SaveChanges();
                    Response.Redirect("../Default");
                }
            }
        }

        // This is the Select method to selects a single Course item with the id
        // USAGE: <asp:FormView SelectMethod="GetItem">
        public TrainingCenter.Models.Course GetItem([FriendlyUrlSegmentsAttribute(0)]int? CourseID)
        {
            if (CourseID == null)
            {
                return null;
            }

            using (_db)
            {
                return _db.Courses.Find(CourseID);
            }
        }

        protected void ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Cancel", StringComparison.OrdinalIgnoreCase))
            {
                Response.Redirect("../Default");
            }
        }*/
    }
}
