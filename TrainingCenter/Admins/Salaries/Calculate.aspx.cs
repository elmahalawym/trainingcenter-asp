﻿/*
 * Salaries Calculation Page
 * 
 * 
 * */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TrainingCenter.Admins.Salaries
{
    public partial class Calculate : System.Web.UI.Page
    {
        //double factor;
        protected void Page_Load(object sender, EventArgs e)
        {
            //set default values
            if(!IsPostBack)
            {
                yearTxt.Text = DateTime.Now.Year.ToString();
                monthDropDown.SelectedIndex = DateTime.Now.Month;
            }
            //else
            //{
            //    if (!double.TryParse(factorTxt.Text, out factor) || factor < 0.0 || factor > 1.0)
            //    {
            //        factorError.Text = "factor must be between 0.0 and 1.0";
            //        return;
            //    }
            //}
        }

        protected void calculateBtn_Click(object sender, EventArgs e)
        {
            if(Page.IsValid)
            {
                int year = int.Parse(yearTxt.Text);

                string pricesFilePath = Server.MapPath("~/App_Data/Prices.xml");
                string numberOfStudentsFilePath = Server.MapPath("~/App_Data/NumberOfStudents.xml");
                string absoluteNumbersFilePath = Server.MapPath(string.Format("~/App_Data/{0}", Helpers.AbsoluteNumbersFileName));
                string salariesFilePath = Server.MapPath(string.Format("~/App_Data/{0}", Helpers.GetSalariesFileName(
                    year, monthDropDown.SelectedIndex)));

                // calculate salaries
                if (curMonthSubType.SelectedIndex == 0) // full month -> calculate salaries with the numbers registered
                {
                    Helpers.CalculateSalaries(pricesFilePath, numberOfStudentsFilePath, salariesFilePath, year, monthDropDown.SelectedIndex);
                }
                else // half month -> calculate salaries with absolute numbers and factor = 0.5
                {
                    Helpers.CalculateSalaries(pricesFilePath, absoluteNumbersFilePath, salariesFilePath, year, monthDropDown.SelectedIndex, 0.5);
                }


                // redirect to default page
                Response.Redirect("~/admins/salaries/default");
            }
        }

        protected void cancelBtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Admins/Salaries/Default");
        }

    }
}