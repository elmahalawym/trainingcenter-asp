namespace TrainingCenter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Absences",
                c => new
                    {
                        StudentID = c.String(nullable: false, maxLength: 128),
                        SessionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.StudentID, t.SessionID })
                .ForeignKey("dbo.AspNetUsers", t => t.StudentID, cascadeDelete: true)
                .ForeignKey("dbo.Sessions", t => t.SessionID, cascadeDelete: true)
                .Index(t => t.StudentID)
                .Index(t => t.SessionID);
            
            CreateTable(
                "dbo.Sessions",
                c => new
                    {
                        SessionID = c.Int(nullable: false, identity: true),
                        CourseID = c.Int(nullable: false),
                        StartDateTime = c.DateTime(nullable: false),
                        EndDateTime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.SessionID)
                .ForeignKey("dbo.Courses", t => t.CourseID, cascadeDelete: true)
                .Index(t => t.CourseID);
            
            CreateTable(
                "dbo.Courses",
                c => new
                    {
                        CourseID = c.Int(nullable: false, identity: true),
                        TeacherID = c.String(maxLength: 128),
                        SchoolYearID = c.Int(nullable: false),
                        CourseName = c.String(),
                        GeneralPrice = c.Double(nullable: false),
                        SpecialPrice = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.CourseID)
                .ForeignKey("dbo.AspNetUsers", t => t.TeacherID)
                .ForeignKey("dbo.SchoolYears", t => t.SchoolYearID, cascadeDelete: true)
                .Index(t => t.TeacherID)
                .Index(t => t.SchoolYearID);
            
            CreateTable(
                "dbo.Exams",
                c => new
                    {
                        ExamID = c.Int(nullable: false, identity: true),
                        CourseID = c.Int(nullable: false),
                        ExamDate = c.DateTime(nullable: false),
                        FullMark = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ExamID)
                .ForeignKey("dbo.Courses", t => t.CourseID, cascadeDelete: true)
                .Index(t => t.CourseID);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        InvoiceID = c.Int(nullable: false, identity: true),
                        StudentID = c.String(maxLength: 128),
                        Price = c.Double(nullable: false),
                        Paid = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.InvoiceID)
                .ForeignKey("dbo.AspNetUsers", t => t.StudentID)
                .Index(t => t.StudentID);
            
            CreateTable(
                "dbo.Salaries",
                c => new
                    {
                        SalaryID = c.Int(nullable: false, identity: true),
                        TeacherID = c.String(maxLength: 128),
                        Price = c.Double(nullable: false),
                        IsPaid = c.Boolean(nullable: false),
                        PaidAmount = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.SalaryID)
                .ForeignKey("dbo.AspNetUsers", t => t.TeacherID)
                .Index(t => t.TeacherID);
            
            CreateTable(
                "dbo.StudentGrades",
                c => new
                    {
                        StudentID = c.String(nullable: false, maxLength: 128),
                        ExamID = c.Int(nullable: false),
                        Grade = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.StudentID, t.ExamID })
                .ForeignKey("dbo.AspNetUsers", t => t.StudentID, cascadeDelete: true)
                .ForeignKey("dbo.Exams", t => t.ExamID, cascadeDelete: true)
                .Index(t => t.StudentID)
                .Index(t => t.ExamID);
            
            CreateTable(
                "dbo.StudentSubscriptions",
                c => new
                    {
                        StudentID = c.String(nullable: false, maxLength: 128),
                        CourseID = c.Int(nullable: false),
                        SubscriptionDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.StudentID, t.CourseID })
                .ForeignKey("dbo.AspNetUsers", t => t.StudentID, cascadeDelete: true)
                .ForeignKey("dbo.Courses", t => t.CourseID, cascadeDelete: true)
                .Index(t => t.StudentID)
                .Index(t => t.CourseID);
            
            CreateTable(
                "dbo.TeacherAbsences",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TeacherID = c.String(maxLength: 128),
                        AbsenceDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.AspNetUsers", t => t.TeacherID)
                .Index(t => t.TeacherID);
            
            AddColumn("dbo.AspNetUsers", "CustomRole", c => c.String());
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TeacherAbsences", "TeacherID", "dbo.AspNetUsers");
            DropForeignKey("dbo.StudentSubscriptions", "CourseID", "dbo.Courses");
            DropForeignKey("dbo.StudentSubscriptions", "StudentID", "dbo.AspNetUsers");
            DropForeignKey("dbo.StudentGrades", "ExamID", "dbo.Exams");
            DropForeignKey("dbo.StudentGrades", "StudentID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Salaries", "TeacherID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Invoices", "StudentID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Exams", "CourseID", "dbo.Courses");
            DropForeignKey("dbo.Absences", "SessionID", "dbo.Sessions");
            DropForeignKey("dbo.Sessions", "CourseID", "dbo.Courses");
            DropForeignKey("dbo.Courses", "SchoolYearID", "dbo.SchoolYears");
            DropForeignKey("dbo.Courses", "TeacherID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Absences", "StudentID", "dbo.AspNetUsers");
            DropIndex("dbo.TeacherAbsences", new[] { "TeacherID" });
            DropIndex("dbo.StudentSubscriptions", new[] { "CourseID" });
            DropIndex("dbo.StudentSubscriptions", new[] { "StudentID" });
            DropIndex("dbo.StudentGrades", new[] { "ExamID" });
            DropIndex("dbo.StudentGrades", new[] { "StudentID" });
            DropIndex("dbo.Salaries", new[] { "TeacherID" });
            DropIndex("dbo.Invoices", new[] { "StudentID" });
            DropIndex("dbo.Exams", new[] { "CourseID" });
            DropIndex("dbo.Courses", new[] { "SchoolYearID" });
            DropIndex("dbo.Courses", new[] { "TeacherID" });
            DropIndex("dbo.Sessions", new[] { "CourseID" });
            DropIndex("dbo.Absences", new[] { "SessionID" });
            DropIndex("dbo.Absences", new[] { "StudentID" });
            DropColumn("dbo.AspNetUsers", "CustomRole");
            DropTable("dbo.TeacherAbsences");
            DropTable("dbo.StudentSubscriptions");
            DropTable("dbo.StudentGrades");
            DropTable("dbo.Salaries");
            DropTable("dbo.Invoices");
            DropTable("dbo.Exams");
            DropTable("dbo.Courses");
            DropTable("dbo.Sessions");
            DropTable("dbo.Absences");
        }
    }
}
