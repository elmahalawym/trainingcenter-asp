﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TrainingCenter.Models
{
    public class InvoiceXmlModel
    {
        public string StudentID { get; set; }

        public string StudentName { get; set; }

        public double Value { get; set; }
    }
}